document.addEventListener("DOMContentLoaded", function () {
    // Verificar si hay un formulario de inicio de sesión en la página actual
    const formCrearSocio = document.getElementById("formCrearSocio");
    if (formCrearSocio) {
        formCrearSocio.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnCrearSocio") {
                e.preventDefault();
                const currentDate = new Date();
                const formattedDate = `${currentDate.getFullYear()}-${(currentDate.getMonth() + 1).toString().padStart(2, '0')}-${currentDate.getDate().toString().padStart(2, '0')}`;
                const data = {
                    NombreSocio: document.getElementById("inputNombreSocio").value,
                    ApellidoPartenoSocio: document.getElementById("inputApellidoPartenoSocio").value,
                    ApellidoMaternoSocio: document.getElementById("inputApellidoMaternoSocio").value,
                    SexoSocio: document.getElementById("inputSexoSocio").value,
                    CorreoElectronicoSocio: document.getElementById("inputCorreoElectronicoSocio").value,
                    TelefonoSocio: document.getElementById("inputTelefonoSocio").value,
                    FechaNacimientoSocio: document.getElementById("inputFechaNacimientoSocio").value,
                    FotoPerfilSocio: document.getElementById("inputFotoPerfilSocio").value,
                    DireccionCalleSocio: document.getElementById("inputDireccionCalleSocio").value,
                    DireccionColoniaSocio: document.getElementById("inputDireccionColoniaSocio").value,
                    DireccionEstadoSocio: document.getElementById("inputDireccionEstadoSocio").value,
                    CodigoPostalSocio: document.getElementById("inputCodigoPostalSocio").value,
                    PlanGimnasioID: document.getElementById("inputPlanGimnasioID").value,
                    InicioPlanGimnasio: formattedDate,
                    PeriodoPlan: document.getElementById("inputPeriodoPlan").value,
                };
                CrearSocio(data);
            }
        });
    }
    const formModificarSocio = document.getElementById("formModificarSocio");
    if (formModificarSocio) {
        formModificarSocio.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnModificarSocio") {
                e.preventDefault();
                const data = {
                    SocioID: document.getElementById("inputSocio_ID").value,
                    NombreSocio: document.getElementById("inputNombreSocio").value,
                    ApellidoPartenoSocio: document.getElementById("inputApellidoPartenoSocio").value,
                    ApellidoMaternoSocio: document.getElementById("inputApellidoMaternoSocio").value,
                    SexoSocio: document.getElementById("inputSexoSocio").value,
                    CorreoElectronicoSocio: document.getElementById("inputCorreoElectronicoSocio").value,
                    TelefonoSocio: document.getElementById("inputTelefonoSocio").value,
                    FechaNacimientoSocio: document.getElementById("inputFechaNacimientoSocio").value,
                    FotoPerfilSocio: document.getElementById("inputFotoPerfilSocio").value,
                    DireccionCalleSocio: document.getElementById("inputDireccionCalleSocio").value,
                    DireccionColoniaSocio: document.getElementById("inputDireccionColoniaSocio").value,
                    DireccionEstadoSocio: document.getElementById("inputDireccionEstadoSocio").value,
                    CodigoPostalSocio: document.getElementById("inputCodigoPostalSocio").value,
                    PlanGimnasioID: document.getElementById("inputPlanGimnasioID").value,
                    InicioPlanGimnasio: document.getElementById("inputInicioPlanGimnasio").value,
                    PeriodoPlan: document.getElementById("inputPeriodoPlan").value,
                };
                ModificarSocio(data);
            }
        });
    }
    const formBuscarSocio = document.getElementById('formBuscarSocio');
    if(formBuscarSocio){
        formBuscarSocio.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnBuscarSocio") {
                e.preventDefault();
                const data = {
                    SocioID: document.getElementById("inputSocioID").value
                };
                BuscarSocio(data);
            }
        });
    }
    const formEliminarSocio = document.getElementById('formEliminarSocio');
    if(formEliminarSocio){
        formEliminarSocio.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnEliminarSocio") {
                e.preventDefault();
                const data = {
                    SocioID: document.getElementById("inputSocioID").value
                };
                EliminarSocio(data);
            }
        });
    }
    ListadoPlanGimnasio();
    ListadoSocios();
});

function CrearSocio(data) {
    fetch('./../../Controller/SocioController.php?action=CrearSocio', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Registro de socio exitosamente.') {
            alert('Registro de socio exitosamente.');
            window.location.href = '/src/View/Socio/BuscarSocio.html';
        } else if (result.message === 'Socio ya se encuentra Registrado.') {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error API No encontrada.', error));
}
function ModificarSocio(data){
    fetch('./../../Controller/SocioController.php?action=ModificarSocio', {
        method: 'PATCH',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Se modifico exitosamente el socio.') {
            alert(result.message);
            window.location.href = '/src/View/Socio/BuscarSocio.html';
        } else if (result.message === 'Error al modificar el socio.') {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error API No encontrada.', error));
}
function BuscarSocio(data){
    fetch('./../../Controller/SocioController.php?action=BuscarSocio', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result) {
            alert(result.message);
            // Actualizar el formulario con los datos del socio modificado
            actualizarFormulario(result.data);
        } else if (result.message === 'No existe el socio.') {
            alert(result.message);
            document.getElementById("inputSocioID").value = "";
        }
    })
    .catch(error => console.error('Error API No encontrada.', error));
}
function EliminarSocio(data){
    fetch('./../../Controller/SocioController.php?action=EliminarSocio', {
        method: 'DELETE',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Elimino al socio.') {
            alert(result.message);
            window.location.href = '/src/View/Socio/BuscarSocio.html';
        } else if (result.message === 'Error al eliminar al socio.') {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error API No encontrada.', error));
}
function ListadoSocios(){
    fetch('./../../Controller/SocioController.php?action=ListadoSocios', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.data && result.data.length > 0) {
            listaSocios = result.data;
            console.log('Lista de socios:', listaSocios);

            const tbListaSocio = document.getElementById('tbListaSocio').getElementsByTagName('tbody')[0];
            tbListaSocio.innerHTML = ''; // Limpiar la tabla antes de agregar nuevos registros

            listaSocios.forEach(socio => {
                const newRow = tbListaSocio.insertRow();
                newRow.innerHTML = `
                    <td>${socio.SocioID}</td>
                    <td>${socio.NombreSocio}</td>
                    <td>${socio.ApellidoPartenoSocio}</td>
                    <td>${socio.ApellidoMaternoSocio}</td>
                    <td>${socio.SexoSocio}</td>
                    <td>${socio.CorreoElectronicoSocio}</td>
                    <td>${socio.TelefonoSocio}</td>
                    <td>${socio.FechaNacimientoSocio}</td>
                    <td>${socio.PlanGimnasioID}</td>
                    <td>${socio.InicioPlanGimnasio}</td>
                    <td>${socio.ProximoPagoPlan}</td>
                    <td>${socio.PeriodoPlan}</td>
                    <th>
                        <button type="submit" class="btn btn-info">
                            <a href="../PagoGimnasio/CrearPagoGimnasio.html?id=${socio.SocioID}&proximoPago=${socio.ProximoPagoPlan}" style="color: aliceblue;">Realizar Pago</a>
                        </button>
                    </th>
                `;
            });

        } else {
            console.error('No se encontraron datos válidos en la respuesta.');
        }
    })
    .catch(error => console.error('Error API No encontrada.', error));
}
function ListadoPlanGimnasio() {
    fetch('./../../Controller/PlanGimnasioController.php?action=ListadoPlanGimnasio', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.data && result.data.length > 0) {
            listadoPlanGimnasio = result.data;
            console.log('Lista de Planes Gimnasio:', listadoPlanGimnasio);

            const selectPlan = document.getElementById('inputPlanGimnasioID');
            selectPlan.innerHTML = '';

            listadoPlanGimnasio.forEach(planGimnasio => {
                const option = document.createElement('option');
                option.value = planGimnasio.PlanGimnasioID; // Ajustar según tus necesidades
                option.textContent = `${planGimnasio.NombrePlanGimnasio} - ${planGimnasio.CostoPlanGimnasio}`;
                selectPlan.appendChild(option);
                document.getElementById
            });
        } else {
            console.error('No se encontraron datos válidos en la respuesta.');
        }
    })
    .catch(error => console.error('Error API No encontrada.', error));
}
