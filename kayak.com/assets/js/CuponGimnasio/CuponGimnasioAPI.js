document.addEventListener("DOMContentLoaded", function () {
    // Verificar si hay un formulario de inicio de sesión en la página actual
    const formCrearCuponGimnasio = document.getElementById("formCrearCuponGimnasio");
    if (formCrearCuponGimnasio) {
        formCrearCuponGimnasio.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnCrearCuponGimnasio") {
                e.preventDefault();
                const data = {
                    NombreCupon: document.getElementById("inputNombreCupon").value,
                    TipoDescuento: document.getElementById("inputTipoDescuento").value,
                    ValorDescuento: document.getElementById("inputCostoValorDescuenton").value,
                    FechaExpiracion: document.getElementById("inputFechaExpiracion").value,
                    DescripcionCupon: document.getElementById("inputDescripcionCupon").value
                };
                CrearCuponGimnasio(data);
            }
        });
    }
    const formModificarCuponGimnasio = document.getElementById("formModificarCuponGimnasio");
    if (formModificarCuponGimnasio) {
        formModificarCuponGimnasio.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnModificarCuponGimnasio") {
                e.preventDefault();
                const data = {
                    CuponID: document.getElementById("inputCupon_ID").value,
                    NombreCupon: document.getElementById("inputNombreCupon").value,
                    TipoDescuento: document.getElementById("inputTipoDescuento").value,
                    ValorDescuento: document.getElementById("inputCostoValorDescuenton").value,
                    FechaExpiracion: document.getElementById("inputFechaExpiracion").value,
                    DescripcionCupon: document.getElementById("inputDescripcionCupon").value
                };
                ModificarCuponGimnasio(data);
            }
        });
    }
    const formBuscarCuponGimnasio = document.getElementById('formBuscarCuponGimnasio');
    if(formBuscarCuponGimnasio){
        formBuscarCuponGimnasio.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnBuscarCuponGimnasio") {
                e.preventDefault();
                const data = {
                    CuponID: document.getElementById("inputCuponID").value
                };
                BuscarCuponGimnasio(data);
            }
        });
    }
    const formEliminarCuponGimnasio = document.getElementById('formEliminarCuponGimnasio');
    if(formEliminarCuponGimnasio){
        formEliminarCuponGimnasio.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnEliminarCuponGimnasio") {
                e.preventDefault();
                const data = {
                    CuponID: document.getElementById("inputCuponID").value
                };
                EliminarCuponGimnasio(data);
            }
        });
    }
    ListadoCuponGimnasio();
});

function CrearCuponGimnasio(data) {
    fetch('./../../Controller/CuponGimnasioController.php?action=registrarCuponGimnasio', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Se registro cupon.') {
            alert(result.message);
            window.location.href = '/src/View/Socio/BuscarSocio.html';
        } else if (result.message === 'Error al registrar cupon.') {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error API No encontrada.', error));
}
function ModificarCuponGimnasio(data){
    fetch('./../../Controller/CuponGimnasioController.php?action=modificarCuponGimnasio', {
        method: 'PATCH',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Se modifico cupon.') {
            alert(result.message);
            window.location.href = '/src/View/Socio/BuscarSocio.html';
        } else if (result.message === 'Error al modificar cupon.') {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error API No encontrada.', error));
}
function BuscarCuponGimnasio(data){
    fetch('./../../Controller/CuponGimnasioController.php?action=buscarCuponGimnasio', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Se encontro cupon.') {
            alert(result.message);
            // Actualizar el formulario con los datos del socio modificado
            actualizarFormulario(result.data);
        } else if (result.message === 'No existe cupon.') {
            alert(result.message);
            document.getElementById("inputSocioID").value = "";
        }
    })
    .catch(error => console.error('Error API No encontrada.', error));
}
function EliminarCuponGimnasio(data){
    fetch('./../../Controller/CuponGimnasioController.php?action=eliminarCuponGimnasio', {
        method: 'DELETE',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Se elimino cupon.') {
            alert(result.message);
            window.location.href = '/src/View/Socio/BuscarSocio.html';
        } else if (result.message === 'Error al eliminar el Plan Gimnasio.') {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error al cupon.', error));
}
function ListadoCuponGimnasio() {
    fetch('./../../Controller/CuponGimnasioController.php?action=listarCuponGimnasio', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.data && result.data.length > 0) {
            
            listadoCuponGimnasio = result.data;
            console.log('Lista de Cupones Gimnasio:', listadoCuponGimnasio);

            const listaCuponesContainer = document.getElementById('listaCuponContainer');
            listaCuponesContainer.innerHTML = ''; // Limpiar el contenedor antes de agregar nuevos elementos

            listadoCuponGimnasio.forEach(cuponGimnasio => {
                const planElement = document.createElement('div');
                planElement.className = 'col-md-4';
                planElement.innerHTML = `
                    <div class='single-service'>
                        <h3>
                        <p>Código Cupón: <a href='#' id="inputNombreCupon">${cuponGimnasio.NombreCupon}</a></p>
                        </h3>
                        <img src="./../../../assets/img/event/event1.jpg" alt="">
                        <p id="inputTipoDescuento">Tipo de descuento: ${cuponGimnasio.TipoDescuento}</p>
                        <p id="inputValorDescuento">Valor del descuento: ${cuponGimnasio.ValorDescuento}</p>
                        <p id="inputValorDescuento">Fecha de expiración del cupón: ${cuponGimnasio.FechaExpiracion}</p> <br>
                        <p id="inputValorDescripcionCupon">Descipcion del cupón: $${cuponGimnasio.DescripcionCupon}</p>
                    </div>
                `;
                listaCuponesContainer.appendChild(planElement);
            });
        } else {
            console.error('No se encontraron datos válidos en la respuesta.');
        }
    })
    .catch(error => console.error('Error API No encontrada.', error));
}
