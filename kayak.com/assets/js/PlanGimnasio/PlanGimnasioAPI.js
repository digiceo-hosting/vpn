document.addEventListener("DOMContentLoaded", function () {
    // Verificar si hay un formulario de inicio de sesión en la página actual
    const formCrearPlanGimnasio = document.getElementById("formCrearPlanGimnasio");
    if (formCrearPlanGimnasio) {
        formCrearPlanGimnasio.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnCrearPlanGimnasio") {
                e.preventDefault();
                const data = {
                    PlanGimnasioID: document.getElementById("inputPlanGimnasioID").value,
                    NombrePlanGimnasio: document.getElementById("inputNombrePlanGimnasio").value,
                    DescripcionPlanGimnasio: document.getElementById("inputDescripcionPlanGimnasio").value,
                    CostoPlanGimnasio: document.getElementById("inputCostoPlanGimnasio").value
                };
                CrearPlanGimnasio(data);
            }
        });
    }
    const formModificarPlanGimnasio = document.getElementById("formModificarPlanGimnasio");
    if (formModificarPlanGimnasio) {
        formModificarPlanGimnasio.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnModificarPlanGimnasio") {
                e.preventDefault();
                const data = {
                    PlanGimnasioID: document.getElementById("inputPlanGimnasioID").value,
                    NombrePlanGimnasio: document.getElementById("inputNombrePlanGimnasio").value,
                    DescripcionPlanGimnasio: document.getElementById("inputDescripcionPlanGimnasio").value,
                    CostoPlanGimnasio: document.getElementById("inputCostoPlanGimnasio").value
                };
                ModificarPlanGimnasio(data);
            }
        });
    }
    const formBuscarPlanGimnasio = document.getElementById('formBuscarPlanGimnasio');
    if(formBuscarPlanGimnasio){
        formBuscarPlanGimnasio.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnBuscarPlanGimnasio") {
                e.preventDefault();
                const data = {
                    PlanGimnasioID: document.getElementById("inputPlanGimnasioID").value
                };
                BuscarPlanGimnasio(data);
            }
        });
    }
    const formEliminarPlanGimnasio = document.getElementById('formEliminarPlanGimnasio');
    if(formEliminarPlanGimnasio){
        formEliminarPlanGimnasio.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnEliminarPlanGimnasio") {
                e.preventDefault();
                const data = {
                    PlanGimnasioID: document.getElementById("inputPlanGimnasioID").value
                };
                EliminarPlanGimnasio(data);
            }
        });
    }
    ListadoPlanGimnasio();
});

function CrearPlanGimnasio(data) {
    fetch('./../../Controller/PlanGimnasioController.php?action=CrearPlanGimnasio', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Registro de plan exitosamente.') {
            alert(result.message);
		window.location.href = '/src/View/Socio/BuscarSocio.html';
        } else if (result.message === 'Socio ya se encuentra Registrado.') {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error API No encontrada.', error));
}
function ModificarPlanGimnasio(data){
    fetch('./../../Controller/PlanGimnasioController.php?action=ModificarPlanGimnasio', {
        method: 'PATCH',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Se modifico exitosamente el Plan.') {
            alert(result.message);
		window.location.href = '/src/View/Socio/BuscarSocio.html';
        } else if (result.message === 'Error al modificar el Plan. Usuario no encontrado.') {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error API No encontrada.', error));
}
function BuscarPlanGimnasio(data){
    fetch('./../../Controller/PlanGimnasioController.php?action=BuscarPlanGimnasio', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Se encontro el Plan Gimnasio.') {
            alert(result.message);
            // Actualizar el formulario con los datos del socio modificado
            actualizarFormulario(result.data);
        } else if (result.message === 'No existe el Plan Gimnasio.') {
            alert(result.message);
            document.getElementById("inputSocioID").value = "";
        }
    })
    .catch(error => console.error('Error API No encontrada.', error));
}
function EliminarPlanGimnasio(data){
    fetch('./../../Controller/PlanGimnasioController.php?action=EliminarPlanGimnasio', {
        method: 'DELETE',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Elimino el Plan Gimnasio.') {
            alert(result.message);
		window.location.href = '/src/View/Socio/BuscarSocio.html';
        } else if (result.message === 'Error al eliminar el Plan Gimnasio.') {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error API No encontrada.', error));
}
function ListadoPlanGimnasio() {
    fetch('./../../Controller/PlanGimnasioController.php?action=ListadoPlanGimnasio', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.data && result.data.length > 0) {
            listadoPlanGimnasio = result.data;
            console.log('Lista de Planes Gimnasio:', listadoPlanGimnasio);

            const listaPlanesContainer = document.getElementById('listaPlanesContainer');
            listaPlanesContainer.innerHTML = ''; // Limpiar el contenedor antes de agregar nuevos elementos

            listadoPlanGimnasio.forEach(planGimnasio => {
                const planElement = document.createElement('div');
                planElement.className = 'col-md-4';
                planElement.innerHTML = `
                    <div class='single-service'>
                        <h3><a href='#' id="PlanGimnasioID">${planGimnasio.PlanGimnasioID}</a></h3>
                        <h3><a href='#' id="NombrePlanGimnasio">${planGimnasio.NombrePlanGimnasio}</a></h3>
                        <img src="./../../../assets/img/event/event1.jpg" alt="">
                        <p id="DescripcionPlanGimnasio">${planGimnasio.DescripcionPlanGimnasio}</p>
                        <p id="CostoPlanGimnasio">$${planGimnasio.CostoPlanGimnasio}</p> <br>
                    </div>
                `;
                listaPlanesContainer.appendChild(planElement);
            });
        } else {
            console.error('No se encontraron datos válidos en la respuesta.');
        }
    })
    .catch(error => console.error('Error API No encontrada.', error));
}
