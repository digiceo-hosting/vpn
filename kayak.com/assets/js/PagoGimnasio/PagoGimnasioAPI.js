document.addEventListener("DOMContentLoaded", function () {
    // Verificar si hay un formulario de inicio de sesión en la página actual
    const formCrearPagoGimnasio = document.getElementById("formCrearPagoGimnasio");
    if (formCrearPagoGimnasio) {
        formCrearPagoGimnasio.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnCrearPagoGimnasio") {
                e.preventDefault();
                const data = {
                    SocioID: document.getElementById("inputSocioID").value,
                    Monto: document.getElementById("inputMonto").value,
                    FechaPago: document.getElementById("inputFechaPago").value,
                    EstadoPago: document.getElementById("inputEstadoPago").value,
                    Cupon: document.getElementById("inputCupon").value
                };
                CrearPagoGimnasio(data);
            }
        });
    }
    const formModificarPagoGimnasio = document.getElementById("formModificarPagoGimnasio");
    if (formModificarPagoGimnasio) {
        formModificarPagoGimnasio.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnModificarPagoGimnasio") {
                e.preventDefault();
                const data = {
                    PagoID: document.getElementById("inputPagoID").value,
                    SocioID: document.getElementById("inputSocioID").value,
                    Monto: document.getElementById("inputMonto").value,
                    FechaPago: document.getElementById("inputFechaPago").value,
                    EstadoPago: document.getElementById("inputEstadoPago").value
                };
                ModificarPagoGimnasio(data);
            }
        });
    }
    const formBuscarPagoGimnasio = document.getElementById('formBuscarPagoGimnasio');
    if(formBuscarPagoGimnasio){
        formBuscarPagoGimnasio.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnBuscarPagoGimnasio") {
                e.preventDefault();
                const data = {
                    PagoID: document.getElementById("inputPago_ID").value,
                };
                BuscarPagoGimnasio(data);
            }
        });
    }
    const formEliminarPagoGimnasio = document.getElementById('formEliminarPagoGimnasio');
    if(formEliminarPagoGimnasio){
        formEliminarPagoGimnasio.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnEliminarPagoGimnasio") {
                e.preventDefault();
                const data = {
                    PagoID: document.getElementById("inputPagoID").value,
                };
                EliminarPagoGimnasio(data);
            }
        });
    }
    ListadoPagoGimnasio();
});

function CrearPagoGimnasio(data) {
    fetch('./../../Controller/PagosGimnasioController.php?action=registrarPago', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Se guardo pago del socio.') {
            alert(result.message);
		window.location.href = '/src/View/Socio/BuscarSocio.html';
        } else if (result.message === 'Error al guardar pago del socio.') {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error API No encontrada.', error));
}
function ModificarPagoGimnasio(data){
    fetch('./../../Controller/PagosGimnasioController.php?action=modificarPagoGimnasio', {
        method: 'PATCH',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Se modifico pago del socio.') {
            alert(result.message);
		window.location.href = '/src/View/Socio/BuscarSocio.html';
        } else if (result.message === 'Error al modificar pago del socio.') {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error API No encontrada.', error));
}
function BuscarPagoGimnasio(data){
    fetch('./../../Controller/PagosGimnasioController.php?action=buscarPagoGimnasio', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.data && result.data.length > 0) {
            listaPagos = result.data;
            console.log('Lista pagos:', listaPagos);
            alert(result.message);

            const tbListaPagos = document.getElementById('tbListaPagosSocio').getElementsByTagName('tbody')[0];
            tbListaPagos.innerHTML = ''; // Limpiar la tabla antes de agregar nuevos registros

            listaPagos.forEach(pago => {
                const newRow = tbListaPagos.insertRow();
                newRow.innerHTML = `
                    <td>${pago.PagoID}</td>
                    <td>${pago.SocioID}</td>
                    <td>${pago.Monto}</td>
                    <td>${pago.FechaPago}</td>
                    <td>${pago.EstadoPago}</td>
                `;
            });
            actualizarFormulario(result.data);
        } else {
            console.error('No se encontraron datos válidos en la respuesta.');
        }
    })
    .catch(error => console.error('Error API No encontrada.', error));
}
function EliminarPagoGimnasio(data){
    fetch('./../../Controller/PagosGimnasioController.php?action=eliminarPagoGimnasio', {
        method: 'DELETE',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Se elimino pago del socio.') {
            alert(result.message);
		window.location.href = '/src/View/Socio/BuscarSocio.html';
        } else if (result.message === 'Error al eliminar pago del socio.') {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error al cupon.', error));
}
function ListadoPagoGimnasio() {
    fetch('./../../Controller/PagosGimnasioController.php?action=listarPagoGimnasio', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.data && result.data.length > 0) {
            listaPagos = result.data;
            console.log('Lista pagos:', listaPagos);

            const tbListaPagos = document.getElementById('tbListaPagos').getElementsByTagName('tbody')[0];
            tbListaPagos.innerHTML = ''; // Limpiar la tabla antes de agregar nuevos registros

            listaPagos.forEach(pago => {
                const newRow = tbListaPagos.insertRow();
                newRow.innerHTML = `
                    <td>${pago.PagoID}</td>
                    <td>${pago.SocioID}</td>
                    <td>${pago.Monto}</td>
                    <td>${pago.FechaPago}</td>
                    <td>${pago.EstadoPago}</td>
                `;
            });

        } else {
            console.error('No se encontraron datos válidos en la respuesta.');
        }
    })
    .catch(error => console.error('Error API No encontrada.', error));
}
