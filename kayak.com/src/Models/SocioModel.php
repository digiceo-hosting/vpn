<?php
    class SocioModel{
        private $conn;
        private $table_bd = "Socios_Gimnasio";

        //DATOS DEL SOCIO 
        public $SocioID; //10021001
        public $NombreSocio; //Agustin
        public $ApellidoPartenoSocio; //Arenas
        public $ApellidoMaternoSocio; //Martinez
        public $SexoSocio; //Hombre
        public $CorreoElectronicoSocio; //austintv52@gmail.com
        public $TelefonoSocio; //2211276404
        public $FechaNacimientoSocio; //03-11-1990
        public $FotoPerfilSocio; // image.png
        public $CURPSocio; //AEMA901103HPLRRG00
        public $DireccionCalleSocio; //113 A OTE 1807
        public $DireccionColoniaSocio; //Los Heroes Puebla
        public $DireccionEstadoSocio; //Puebla, Pue.
        public $CodigoPostalSocio; //72590
        public $PlanGimnasioID; // 01
        public $InicioPlanGimnasio; //Enero de 2023
        public $ProximoPagoPlan; //Febrero de 2024
        public $PeriodoPlan; 

        public function __construct($db)
        {
            $this->conn = $db;
        }
        public function registrarSocio() {
            try{
                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this-> NombreSocio = htmlspecialchars(strip_tags($this->NombreSocio));
                $this-> ApellidoPartenoSocio = htmlspecialchars(strip_tags($this->ApellidoPartenoSocio));
                $this-> ApellidoMaternoSocio = htmlspecialchars(strip_tags($this->ApellidoMaternoSocio));
                $this-> SexoSocio = htmlspecialchars(strip_tags($this->SexoSocio));
                $this-> CorreoElectronicoSocio = htmlspecialchars(strip_tags($this->CorreoElectronicoSocio));
                $this-> TelefonoSocio = htmlspecialchars(strip_tags($this->TelefonoSocio));
                $this-> FechaNacimientoSocio = htmlspecialchars(strip_tags($this->FechaNacimientoSocio));
                $this-> FotoPerfilSocio = htmlspecialchars(strip_tags($this->FotoPerfilSocio));
                $this-> PlanGimnasioID = htmlspecialchars(strip_tags($this->PlanGimnasioID));
                $this-> DireccionCalleSocio = htmlspecialchars(strip_tags($this->DireccionCalleSocio));
                $this-> DireccionColoniaSocio = htmlspecialchars(strip_tags($this->DireccionColoniaSocio));
                $this-> DireccionEstadoSocio = htmlspecialchars(strip_tags($this->DireccionEstadoSocio));
                $this-> CodigoPostalSocio = htmlspecialchars(strip_tags($this->CodigoPostalSocio));
                $this-> InicioPlanGimnasio = htmlspecialchars(strip_tags($this->InicioPlanGimnasio));
                $this-> ProximoPagoPlan = htmlspecialchars(strip_tags($this->ProximoPagoPlan));
                $this-> PeriodoPlan = htmlspecialchars(strip_tags($this->PeriodoPlan));

                // Verificar si ya existe un socio con el mismo correo o telefono
                $queryVerificarCorreo = "SELECT CorreoElectronicoSocio, TelefonoSocio FROM $this->table_bd WHERE CorreoElectronicoSocio = :CorreoElectronicoSocio OR TelefonoSocio = :TelefonoSocio";
                $stmtVerificaSocio = $this->conn->prepare($queryVerificarCorreo);
                // Vincular parámetros
                $stmtVerificaSocio->bindParam(":CorreoElectronicoSocio", $this->CorreoElectronicoSocio);
                $stmtVerificaSocio->bindParam(":TelefonoSocio", $this->TelefonoSocio);
                $stmtVerificaSocio->execute();
                //VALIDA LA CONSULTA Y DETERMINA EXISTENCIA
                if ($stmtVerificaSocio->rowCount() > 0) {
                    // El correo ó el telefono ya está registrado, retornar un mensaje de error
                    return null;
                }

                // Si no existe el correo ó telefono proceder con el registro
                $queryRegistroSocio = "INSERT INTO $this->table_bd (NombreSocio,ApellidoPartenoSocio,ApellidoMaternoSocio,SexoSocio,CorreoElectronicoSocio,TelefonoSocio,FechaNacimientoSocio,FotoPerfilSocio,PlanGimnasioID,DireccionCalleSocio,DireccionColoniaSocio,DireccionEstadoSocio,CodigoPostalSocio, InicioPlanGimnasio,ProximoPagoPlan, PeriodoPlan) VALUES (:NombreSocio,:ApellidoPartenoSocio,:ApellidoMaternoSocio,:SexoSocio,:CorreoElectronicoSocio,:TelefonoSocio,:FechaNacimientoSocio,:FotoPerfilSocio,:PlanGimnasioID,:DireccionCalleSocio,:DireccionColoniaSocio,:DireccionEstadoSocio,:CodigoPostalSocio,:InicioPlanGimnasio,:ProximoPagoPlan, :PeriodoPlan)";
                // Preparar la declaración SQL
                $stmt = $this->conn->prepare($queryRegistroSocio);
                // Vincular parámetros
                $stmt->bindParam(":NombreSocio", $this->NombreSocio);
                $stmt->bindParam(":ApellidoPartenoSocio", $this->ApellidoPartenoSocio);
                $stmt->bindParam(":ApellidoMaternoSocio", $this->ApellidoMaternoSocio);
                $stmt->bindParam(":SexoSocio", $this->SexoSocio);
                $stmt->bindParam(":CorreoElectronicoSocio", $this->CorreoElectronicoSocio);
                $stmt->bindParam(":TelefonoSocio", $this->TelefonoSocio);
                $stmt->bindParam(":FechaNacimientoSocio", $this->FechaNacimientoSocio);
                $stmt->bindParam(":FotoPerfilSocio", $this->FotoPerfilSocio);
                $stmt->bindParam(":PlanGimnasioID", $this->PlanGimnasioID);
                $stmt->bindParam(":DireccionCalleSocio", $this->DireccionCalleSocio);
                $stmt->bindParam(":DireccionColoniaSocio", $this->DireccionColoniaSocio);
                $stmt->bindParam(":DireccionEstadoSocio", $this->DireccionEstadoSocio);
                $stmt->bindParam(":CodigoPostalSocio", $this->CodigoPostalSocio);
                $stmt->bindParam(":InicioPlanGimnasio", $this->InicioPlanGimnasio);
                $stmt->bindParam(":ProximoPagoPlan", $this->ProximoPagoPlan);
                $stmt->bindParam(":PeriodoPlan", $this->PeriodoPlan);
                // Ejecutar la consulta
                if ($stmt->execute()) {
                    // Devolver el SocioID generado
                    return $this->conn->lastInsertId();
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            }catch(PDOException $e){
                throw new PDOException(
                    "Error en la ejecución de la consulta: " . $e->getMessage()
                );
            }
        }
        public function modificarSocio() {
            try {
                $querymodificarSocio = "UPDATE $this->table_bd SET NombreSocio=:NombreSocio,ApellidoPartenoSocio=:ApellidoPartenoSocio,ApellidoMaternoSocio=:ApellidoMaternoSocio,SexoSocio=:SexoSocio,CorreoElectronicoSocio=:CorreoElectronicoSocio,TelefonoSocio=:TelefonoSocio,FechaNacimientoSocio=:FechaNacimientoSocio,FotoPerfilSocio=:FotoPerfilSocio,PlanGimnasioID=:PlanGimnasioID,DireccionCalleSocio=:DireccionCalleSocio,DireccionColoniaSocio=:DireccionColoniaSocio,DireccionEstadoSocio=:DireccionEstadoSocio,CodigoPostalSocio=:CodigoPostalSocio, InicioPlanGimnasio=:InicioPlanGimnasio, PeriodoPlan=:PeriodoPlan WHERE SocioID = :SocioID";
                // Preparar la declaración SQL
                $stmt = $this->conn->prepare($querymodificarSocio);
                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this-> SocioID = htmlspecialchars(strip_tags($this->SocioID));
                $this-> NombreSocio = htmlspecialchars(strip_tags($this->NombreSocio));
                $this-> ApellidoPartenoSocio = htmlspecialchars(strip_tags($this->ApellidoPartenoSocio));
                $this-> ApellidoMaternoSocio = htmlspecialchars(strip_tags($this->ApellidoMaternoSocio));
                $this-> SexoSocio = htmlspecialchars(strip_tags($this->SexoSocio));
                $this-> CorreoElectronicoSocio = htmlspecialchars(strip_tags($this->CorreoElectronicoSocio));
                $this-> TelefonoSocio = htmlspecialchars(strip_tags($this->TelefonoSocio));
                $this-> FechaNacimientoSocio = htmlspecialchars(strip_tags($this->FechaNacimientoSocio));
                $this-> FotoPerfilSocio = htmlspecialchars(strip_tags($this->FotoPerfilSocio));
                $this-> PlanGimnasioID = htmlspecialchars(strip_tags($this->PlanGimnasioID));
                $this-> DireccionCalleSocio = htmlspecialchars(strip_tags($this->DireccionCalleSocio));
                $this-> DireccionColoniaSocio = htmlspecialchars(strip_tags($this->DireccionColoniaSocio));
                $this-> DireccionEstadoSocio = htmlspecialchars(strip_tags($this->DireccionEstadoSocio));
                $this-> CodigoPostalSocio = htmlspecialchars(strip_tags($this->CodigoPostalSocio));
                $this-> InicioPlanGimnasio = htmlspecialchars(strip_tags($this->InicioPlanGimnasio));
                $this-> PeriodoPlan = htmlspecialchars(strip_tags($this->PeriodoPlan));
                // Vincular parámetros
                $stmt->bindParam(":SocioID", $this->SocioID);
                $stmt->bindParam(":NombreSocio", $this->NombreSocio);
                $stmt->bindParam(":ApellidoPartenoSocio", $this->ApellidoPartenoSocio);
                $stmt->bindParam(":ApellidoMaternoSocio", $this->ApellidoMaternoSocio);
                $stmt->bindParam(":SexoSocio", $this->SexoSocio);
                $stmt->bindParam(":CorreoElectronicoSocio", $this->CorreoElectronicoSocio);
                $stmt->bindParam(":TelefonoSocio", $this->TelefonoSocio);
                $stmt->bindParam(":FechaNacimientoSocio", $this->FechaNacimientoSocio);
                $stmt->bindParam(":FotoPerfilSocio", $this->FotoPerfilSocio);
                $stmt->bindParam(":PlanGimnasioID", $this->PlanGimnasioID);
                $stmt->bindParam(":DireccionCalleSocio", $this->DireccionCalleSocio);
                $stmt->bindParam(":DireccionColoniaSocio", $this->DireccionColoniaSocio);
                $stmt->bindParam(":DireccionEstadoSocio", $this->DireccionEstadoSocio);
                $stmt->bindParam(":CodigoPostalSocio", $this->CodigoPostalSocio);
                $stmt->bindParam(":InicioPlanGimnasio", $this->InicioPlanGimnasio);
                $stmt->bindParam(":PeriodoPlan", $this->PeriodoPlan);
                // Ejecutar la consulta
                if ($stmt->execute()) {
                    // Verificar cuántas filas fueron afectadas
                    $filasAfectadas = $stmt->rowCount();
                    // Devolver el resultado
                    return $filasAfectadas > 0;
                    if($filasAfectadas > 0){
                        return $filasAfectadas;
                    }
                    return null;
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            } catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta: " . $e->getMessage());
            }

        }
        public function eliminarSocio() {
            try {
                $queryeliminarSocio = "DELETE FROM $this->table_bd WHERE SocioID = :SocioID";
        
                // Preparar la declaración SQL
                $stmt = $this->conn->prepare($queryeliminarSocio);
        
                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this->SocioID = htmlspecialchars(strip_tags($this->SocioID));
        
                // Vincular parámetros
                $stmt->bindParam(":SocioID", $this->SocioID);
        
                // Ejecutar la consulta
                if ($stmt->execute()) {
                    // Verificar cuántas filas fueron afectadas
                    $filasAfectadas = $stmt->rowCount();
                    // Devolver el resultado
                    return $filasAfectadas > 0;
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            } catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta: " . $e->getMessage());
            }
        }
        public function buscarSocio() {
            $queryBuscarSocio = "SELECT SocioID,NombreSocio,ApellidoPartenoSocio,ApellidoMaternoSocio,SexoSocio,CorreoElectronicoSocio,TelefonoSocio,FechaNacimientoSocio,FotoPerfilSocio,PlanGimnasioID,DireccionCalleSocio,DireccionColoniaSocio,DireccionEstadoSocio,CodigoPostalSocio, InicioPlanGimnasio, ProximoPagoPlan, PeriodoPlan FROM $this->table_bd WHERE SocioID = :SocioID";
            $stmt = $this->conn->prepare($queryBuscarSocio);
            // Limpia y filtra los datos antes de insertarlos en la base de datos
            $this->SocioID = htmlspecialchars(strip_tags($this->SocioID));
            $stmt->bindParam(":SocioID", $this->SocioID);
            // Ejecuta la consulta
            if ($stmt->execute()) {
                // Devuelve el resultado (puedes ajustar esto según tus necesidades)
                return $stmt->fetch(PDO::FETCH_ASSOC);
            } else {
                // Lanzar una excepción en caso de error
                throw new PDOException("Error en la ejecución de la consulta.");
            }
        }
        public function listarSocios(){
            $queryListarSocios = "SELECT SocioID,NombreSocio,ApellidoPartenoSocio,ApellidoMaternoSocio,SexoSocio,CorreoElectronicoSocio,TelefonoSocio,FechaNacimientoSocio,FotoPerfilSocio,PlanGimnasioID,DireccionCalleSocio,DireccionColoniaSocio,DireccionEstadoSocio,CodigoPostalSocio, InicioPlanGimnasio, ProximoPagoPlan, PeriodoPlan FROM $this->table_bd";
            $stmt = $this->conn->prepare($queryListarSocios);
            // Ejecuta la consulta
            if ($stmt->execute()) {
                // Devuelve todos los resultados como un arreglo asociativo
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            } else {
                // Lanzar una excepción en caso de error
                throw new PDOException("Error en la ejecución de la consulta.");
            }
        }
    }
?>