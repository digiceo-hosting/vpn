<?php
    class PagosGimnasioModel{
        private $conn;
        private $table_bd = "Pagos_Gimnasio";

        //DATOS DE LOS PAGOS DEL GIMNASIO
        public $PagoID;
        public $SocioID;
        public $Monto;
        public $FechaPago;
        public $EstadoPago;
        public $Cupon;

        public function __construct($db)
        {
            $this->conn = $db;
        }
        public function obtenerProximoPago($fechaInicioSuscripcion, $duracionPlan) {
            $fechaInicio = new DateTime($fechaInicioSuscripcion);
            $proximoPago = clone $fechaInicio;
            $proximoPago->modify("+$duracionPlan months");
            return $proximoPago->format('Y-m-d');
        }
        public function validarEstadoPago($proximoPago, $socioID) {

            // $fechaActual = new DateTime(); // Obtener la fecha actual
            $fechaActual = new DateTime(); // Obtener la fecha actual
            // Convertir $proximoPago a objeto DateTime
            $proximoPago = new DateTime($proximoPago);
            // Obtener la fecha actual en el formato 'Y-m-d' (año-mes-día)
            $variable1 = $fechaActual->format('Y-m-d 00:00:00');

            // Obtener la fecha del próximo pago en el mismo formato
            $variable2 = $proximoPago->format('Y-m-d 00:00:00');
            
            $Socio = $socioID;
            // Verificar si ya se ha realizado un pago para el próximo mes
            $mesProximoPago = $proximoPago->format('m'); // Obtener el mes del próximo pago
            $anioProximoPago = $proximoPago->format('Y'); // Obtener el año del próximo pago
        
            // Consulta SQL para verificar si existe un pago para el próximo mes del socio
            $sql = "SELECT COUNT(*) AS total_pagos FROM $this->table_bd WHERE SocioID = :Socio AND MONTH(FechaPago) = :mes AND YEAR(FechaPago) = :anio";
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':Socio', $Socio);
            $stmt->bindParam(':mes', $mesProximoPago);
            $stmt->bindParam(':anio', $anioProximoPago);
            $stmt->execute();
            $resultado = $stmt->fetch(PDO::FETCH_ASSOC);


            if($variable1 < $variable2){
                if ($resultado['total_pagos'] > 0) {
                    return "Con acceso. ";
                } else {
                    return "<Socio con acceso : Regularizar Pago. ";
                }
            }elseif($variable1 = $variable2){
                if ($resultado['total_pagos'] > 0) {
                    return "Con acceso. ";
                } else {
                    return "=Socio con acceso : Regularizar Pago. ";
                }
            }elseif($variable1 > $variable2){
                if ($resultado['total_pagos'] > 0) {
                    // return "Socio con Acceso 5>1";
                    return "Con acceso.";
                } else {
                    return "Sin acceso : Falta de pago.";
                }
            }
            
            
        }
        public function registrarPago($socioID, $monto, $ProximoPagoPlan, $cupon) {
            try {
                if ($cupon > 0) {
                    // Consultar la tabla de cupones para obtener el cupón correspondiente
                    $sql = "SELECT * FROM Cupones_Gimnasio WHERE NombreCupon = :cuponID";
                    $stmt = $this->conn->prepare($sql);
                    $stmt->bindParam(':cuponID', $cupon);
                    $stmt->execute();
                    $cuponData = $stmt->fetch(PDO::FETCH_ASSOC);
        
                    // Verificar si se encontró el cupón en la base de datos
                    if ($cuponData) {
                        // Obtener el tipo de descuento y el valor del cupón
                        $tipoDescuento = $cuponData['TipoDescuento']; // Puede ser 'Porcentaje' o 'Cantidad'
                        $valorDescuento = $cuponData['ValorDescuento'];
        
                        // Aplicar el descuento al monto total según el tipo de descuento
                        if ($tipoDescuento == 'Porcentaje') {
                            // Calcular el nuevo monto aplicando el descuento porcentual
                            $montoDescuento = ($monto * $valorDescuento) / 100;
                            $monto -= $montoDescuento;
                        } elseif ($tipoDescuento == 'Cantidad') {
                            // Restar la cantidad del cupón al monto total
                            $monto -= $valorDescuento;
                        } else {
                            // Manejar un tipo de descuento no reconocido (opcional)
                            throw new Exception("Tipo de descuento no válido para el cupón.");
                        }
                    } else {
                        // Manejar el caso en el que el cupón no se encontró en la base de datos
                        throw new Exception("El cupón especificado no existe.");
                    }
                }
        
                // Preparar la declaración SQL
                $sqlInsertPago = "INSERT INTO  $this->table_bd  (SocioID, Monto, FechaPago, EstadoPago) VALUES (:socioID, :monto, :proximoPago, 'Pagado')";
                $stmt = $this->conn->prepare($sqlInsertPago);
                $stmt->bindParam(':socioID', $socioID);
                $stmt->bindParam(':monto', $monto);
                $stmt->bindParam(':proximoPago', $ProximoPagoPlan);
        
                // Ejecutar la consulta
                if ($stmt->execute()) {
                    $proximoPago = $this->obtenerProximoPago($ProximoPagoPlan, 1); // Asumiendo que la duración del plan es mensual
                    $sqlUpdateSocio = "UPDATE Socios_Gimnasio SET ProximoPagoPlan = :proximoPago WHERE SocioID = :socioID";
                    $stmt = $this->conn->prepare($sqlUpdateSocio);
                    $stmt->bindParam(':proximoPago', $proximoPago);
                    $stmt->bindParam(':socioID', $socioID);
                    $stmt->execute();
        
                    // Devolver el PlanGimnasioID generado
                    return true;
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            } catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta: " . $e->getMessage());
            }
        }
        //CRUD API PAGO
        public function CrearPagoGimnasio() {
            try {
                $cupon = $this->Cupon;
                $monto = $this->Monto;
                $ProximoPagoPlan = $this->FechaPago;
                $socioID = $this->SocioID;

                if ($cupon > 0) {
                    // Consultar la tabla de cupones para obtener el cupón correspondiente
                    $sql = "SELECT * FROM Cupones_Gimnasio WHERE CuponID = :cuponID";
                    $stmt = $this->conn->prepare($sql);
                    $stmt->bindParam(':cuponID', $cupon);
                    $stmt->execute();
                    $cuponData = $stmt->fetch(PDO::FETCH_ASSOC);
        
                    // Verificar si se encontró el cupón en la base de datos
                    if ($cuponData) {
                        // Obtener el tipo de descuento y el valor del cupón
                        $tipoDescuento = $cuponData['TipoDescuento']; // Puede ser 'Porcentaje' o 'Cantidad'
                        $valorDescuento = $cuponData['ValorDescuento'];
        
                        // Aplicar el descuento al monto total según el tipo de descuento
                        if ($tipoDescuento == 'Porcentaje') {
                            // Calcular el nuevo monto aplicando el descuento porcentual
                            $montoDescuento = ($monto * $valorDescuento) / 100;
                            $monto -= $montoDescuento;
                        } elseif ($tipoDescuento == 'Cantidad') {
                            // Restar la cantidad del cupón al monto total
                            $monto -= $valorDescuento;
                        } else {
                            // Manejar un tipo de descuento no reconocido (opcional)
                            throw new Exception("Tipo de descuento no válido para el cupón.");
                        }
                    } else {
                        // Manejar el caso en el que el cupón no se encontró en la base de datos
                        throw new Exception("El cupón especificado no existe.");
                    }
                }

                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this-> SocioID = htmlspecialchars(strip_tags($this->SocioID));
                $this-> Monto = htmlspecialchars(strip_tags($this->Monto));
                $this-> FechaPago = htmlspecialchars(strip_tags($this->FechaPago));
                $this-> EstadoPago = htmlspecialchars(strip_tags($this->EstadoPago));

                //Proceso de registro de los pagos del gimnasio
                $queryRegistroPagoGimnasio = "INSERT INTO $this->table_bd (SocioID, Monto, FechaPago, EstadoPago) VALUES (:SocioID, :Monto, :FechaPago, :EstadoPago)";
                // Preparar la declaración SQL
                $stmt = $this->conn->prepare($queryRegistroPagoGimnasio);
                // Vincular parámetros
                $stmt->bindParam(":SocioID", $this->SocioID);
                $stmt->bindParam(":Monto", $this->Monto);
                $stmt->bindParam(":FechaPago", $this->FechaPago);
                $stmt->bindParam(":EstadoPago", $this->EstadoPago);

                // Ejecutar la consulta
                if ($stmt->execute()) {
                    $proximoPago = $this->obtenerProximoPago($ProximoPagoPlan, 1); // Asumiendo que la duración del plan es mensual
                    $sqlUpdateSocio = "UPDATE Socios_Gimnasio SET ProximoPagoPlan = :proximoPago WHERE SocioID = :socioID";
                    $stmt = $this->conn->prepare($sqlUpdateSocio);
                    $stmt->bindParam(':proximoPago', $proximoPago);
                    $stmt->bindParam(':socioID', $socioID);
                    $stmt->execute();
        
                    // Devolver el PlanGimnasioID generado
                    return true;
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            } catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta: " . $e->getMessage());
            }
        }
        public function modificarPagoGimnasio() {
            try{
                $querymodificarPagoGimnasio = "UPDATE $this->table_bd SET 
                SocioID=:SocioID, Monto=:Monto, FechaPago=:FechaPago, EstadoPago=:EstadoPago WHERE PagoID = :PagoID";
                // Preparar la declaración SQL
                $stmt = $this->conn->prepare($querymodificarPagoGimnasio);
                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this-> PagoID = htmlspecialchars(strip_tags($this->PagoID));
                $this-> SocioID = htmlspecialchars(strip_tags($this->SocioID));
                $this-> Monto = htmlspecialchars(strip_tags($this->Monto));
                $this-> FechaPago = htmlspecialchars(strip_tags($this->FechaPago));
                $this-> EstadoPago = htmlspecialchars(strip_tags($this->EstadoPago));
                // Vincular parámetros
                $stmt->bindParam(":PagoID", $this->PagoID);
                $stmt->bindParam(":SocioID", $this->SocioID);
                $stmt->bindParam(":Monto", $this->Monto);
                $stmt->bindParam(":FechaPago", $this->FechaPago);
                $stmt->bindParam(":EstadoPago", $this->EstadoPago);
                // Ejecutar la consulta
                if ($stmt->execute()) {
                    // Verificar cuántas filas fueron afectadas
                    $filasAfectadas = $stmt->rowCount();
                    // Devolver el resultado
                    return $filasAfectadas > 0;
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            }catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta: " . $e->getMessage());
            }
        }
        public function eliminarPagoGimnasio() {
            try{
                $queryeliminarPagoGimnasio = "DELETE FROM $this->table_bd WHERE PagoID = :PagoID";
        
                // Preparar la declaración SQL
                $stmt = $this->conn->prepare($queryeliminarPagoGimnasio);
        
                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this->PagoID = htmlspecialchars(strip_tags($this->PagoID));
        
                // Vincular parámetros
                $stmt->bindParam(":PagoID", $this->PagoID);
        
                // Ejecutar la consulta
                if ($stmt->execute()) {
                    // Verificar cuántas filas fueron afectadas
                    $filasAfectadas = $stmt->rowCount();
                    // Devolver el resultado
                    return $filasAfectadas > 0;
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            }catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta: " . $e->getMessage());
            }
        }
        public function buscarPagoGimnasio() {
            try{
                $queryBuscarPagoGimnasio = "SELECT PagoID, SocioID, Monto, FechaPago, EstadoPago FROM $this->table_bd WHERE SocioID = :PagoID";
                $stmt = $this->conn->prepare($queryBuscarPagoGimnasio);
                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this->PagoID = htmlspecialchars(strip_tags($this->PagoID));
                $stmt->bindParam(":PagoID", $this->PagoID);
                // Ejecuta la consulta
                if ($stmt->execute()) {
                    // Devuelve el resultado (puedes ajustar esto según tus necesidades)
                    return $stmt->fetchAll(PDO::FETCH_ASSOC);
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            }catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta: " . $e->getMessage());
            }
        }
        public function listarPagoGimnasio() {
            try{
                $queryListarPagoGimnasio = "SELECT PagoID, SocioID, Monto, FechaPago, EstadoPago FROM $this->table_bd";
                $stmt = $this->conn->prepare($queryListarPagoGimnasio);
                // Ejecuta la consulta
                if ($stmt->execute()) {
                    // Devuelve todos los resultados como un arreglo asociativo
                    return $stmt->fetchAll(PDO::FETCH_ASSOC);
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            }catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta: " . $e->getMessage());
            }
        }
    }
?>