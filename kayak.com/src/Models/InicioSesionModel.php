<?php 
    class InicioSesionModel{
        private $conn;
        private $table_bd = "InicioSesion";

        public $InicioSesionID;
        public $NickUser;
        public $NombreUser;
        public $ApellidoUser;
        public $CorreoUser;
        public $ClaveUser;
        public $FechaCreacion;
        public $RolUser;
        public $OficinaUser;

        public function __construct($db) {
            $this->conn = $db;
        }
        //Funcion valida el inicio de sesion de las cuentas : Retorna data de la cuenta ? Retorna false
        public function inicioSesionCuenta(){
            try {
                $queryInicioSesion = "SELECT InicioSesionID, NickUser, NombreUser, ApellidoUser, CorreoUser, ClaveUser, FechaCreacion, RolUser, OficinaUser FROM $this->table_bd WHERE CorreoUser = :CorreoUser AND ClaveUser = :ClaveUser";
                $stmt = $this->conn->prepare($queryInicioSesion);

                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this->CorreoUser = htmlspecialchars(strip_tags($this->CorreoUser));
                $this->ClaveUser = htmlspecialchars(strip_tags($this->ClaveUser));
                
                $stmt->bindParam(":CorreoUser", $this->CorreoUser);
                $stmt->bindParam(":ClaveUser", $this->ClaveUser);

                // Ejecuta la consulta
                if ($stmt->execute()) {
                    // Devuelve el resultado (puedes ajustar esto según tus necesidades)
                    return $stmt->fetch(PDO::FETCH_ASSOC);
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            }catch (PDOException $e) {
                // http_response_code(500);
                throw new PDOException("Error en la ejecución de la consulta: " . $e->getMessage());
            }
        }
        // Función para registrar cuentas (Cliente o Administrativo) : Retorna InicioSesionID ? Retorna false
        public function registrarCuenta() {
            try {
                // Verificar si ya existe un registro con el mismo correo
                $queryVerificarCorreo = "SELECT CorreoUser FROM $this->table_bd WHERE CorreoUser = :CorreoUser";
                $stmtVerificarCorreo = $this->conn->prepare($queryVerificarCorreo);$this->CorreoUser = htmlspecialchars(strip_tags($this->CorreoUser));
                $stmtVerificarCorreo->bindParam(":CorreoUser", $this->CorreoUser);
                $stmtVerificarCorreo->execute();
                if ($stmtVerificarCorreo->rowCount() > 0) {
                    // El correo ya está registrado, retornar un mensaje de error
                    return null;
                }

                // Si no existe el correo, proceder con el registro
                $queryRegistro = "INSERT INTO $this->table_bd (NickUser, NombreUser, ApellidoUser, CorreoUser, ClaveUser, RolUser, OficinaUser) VALUES (:NickUser, :NombreUser, :ApellidoUser, :CorreoUser, :ClaveUser, :RolUser, :OficinaUser)";

                // Preparar la declaración SQL
                $stmt = $this->conn->prepare($queryRegistro);

                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this->NickUser = htmlspecialchars(strip_tags($this->NickUser));
                $this->NombreUser = htmlspecialchars(strip_tags($this->NombreUser));
                $this->ApellidoUser = htmlspecialchars(strip_tags($this->ApellidoUser));
                $this->CorreoUser = htmlspecialchars(strip_tags($this->CorreoUser));
                $this->ClaveUser = htmlspecialchars(strip_tags($this->ClaveUser));
                $this->RolUser = htmlspecialchars(strip_tags($this->RolUser));
                $this->OficinaUser = htmlspecialchars(strip_tags($this->OficinaUser));

                // Vincular parámetros
                $stmt->bindParam(":NickUser", $this->NickUser);
                $stmt->bindParam(":NombreUser", $this->NombreUser);
                $stmt->bindParam(":ApellidoUser", $this->ApellidoUser);
                $stmt->bindParam(":CorreoUser", $this->CorreoUser);
                $stmt->bindParam(":ClaveUser", $this->ClaveUser);
                $stmt->bindParam(":RolUser", $this->RolUser);
                $stmt->bindParam(":OficinaUser", $this->OficinaUser);

                // Ejecutar la consulta
                if ($stmt->execute()) {
                    // Devolver el InicioSesionID generado o algún otro indicador de éxito
                    return $this->conn->lastInsertId();
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            } catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta: " . $e->getMessage());
            }
        }
        //Funcion para recuperar clave de la cuenta (Cliente o Administrativo) : Retorna data de la cuenta ? Retorna false
        public function recuperacionCuenta(){
            try {
                $queryRecuperacionClaveUser = "SELECT InicioSesionID, NickUser, NombreUser, ApellidoUser, CorreoUser, ClaveUser, FechaCreacion, RolUser, OficinaUser FROM $this->table_bd WHERE CorreoUser = :CorreoUser";
                // Preparar la declaración SQL
                $stmt = $this->conn->prepare($queryRecuperacionClaveUser);

                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this->CorreoUser = htmlspecialchars(strip_tags($this->CorreoUser));
                // Vincular parámetros
                $stmt->bindParam(":CorreoUser", $this->CorreoUser);

                // Ejecutar la consulta
                if ($stmt->execute()) {
                    // Devolver el resultado como un array asociativo
                    return $stmt->fetch(PDO::FETCH_ASSOC);
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            }catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta: " . $e->getMessage());
            }
        }
        //Funcion para modificar cuentas (Cliente o Administrativo) : Retorna true ? Retorna false
        public function modificarCuenta(){
            try {
                $querymodificarCuenta = "UPDATE $this->table_bd SET NickUser = :NickUser, NombreUser = :NombreUser,  ApellidoUser = :ApellidoUser, CorreoUser = :CorreoUser, ClaveUser = :ClaveUser, RolUser = :RolUser, OficinaUser = :OficinaUser WHERE InicioSesionID = :InicioSesionID";
        
                // Preparar la declaración SQL
                $stmt = $this->conn->prepare($querymodificarCuenta);
        
                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this->InicioSesionID = htmlspecialchars(strip_tags($this->InicioSesionID));
                $this->NickUser = htmlspecialchars(strip_tags($this->NickUser));
                $this->NombreUser = htmlspecialchars(strip_tags($this->NombreUser));
                $this->ApellidoUser = htmlspecialchars(strip_tags($this->ApellidoUser));
                $this->CorreoUser = htmlspecialchars(strip_tags($this->CorreoUser));
                $this->ClaveUser = htmlspecialchars(strip_tags($this->ClaveUser));
                $this->RolUser = htmlspecialchars(strip_tags($this->RolUser));
                $this->OficinaUser = htmlspecialchars(strip_tags($this->OficinaUser));
        
                // Vincular parámetros
                $stmt->bindParam(":InicioSesionID", $this->InicioSesionID);
                $stmt->bindParam(":NickUser", $this->NickUser);
                $stmt->bindParam(":NombreUser", $this->NombreUser);
                $stmt->bindParam(":ApellidoUser", $this->ApellidoUser);
                $stmt->bindParam(":CorreoUser", $this->CorreoUser);
                $stmt->bindParam(":ClaveUser", $this->ClaveUser);
                $stmt->bindParam(":RolUser", $this->RolUser);
                $stmt->bindParam(":OficinaUser", $this->OficinaUser);
        
                // Ejecutar la consulta
                if ($stmt->execute()) {
                    // Verificar cuántas filas fueron afectadas
                    $filasAfectadas = $stmt->rowCount();

                    // Devolver el resultado
                    return $filasAfectadas > 0;
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta de modificar cuenta.");
                }
            } catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta de modificar cuenta: " . $e->getMessage());
            }
        }
        //Funcion para eliminar cuentas (Cliente o Administrativo) : Retorna true ? Retorna false
        public function eliminarCuenta(){
            try {
                $queryeliminarCuenta = "DELETE FROM $this->table_bd WHERE InicioSesionID = :InicioSesionID";
        
                // Preparar la declaración SQL
                $stmt = $this->conn->prepare($queryeliminarCuenta);
        
                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this->InicioSesionID = htmlspecialchars(strip_tags($this->InicioSesionID));
        
                // Vincular parámetros
                $stmt->bindParam(":InicioSesionID", $this->InicioSesionID);
        
                // Ejecutar la consulta
                if ($stmt->execute()) {
                    // Verificar cuántas filas fueron afectadas
                    $filasAfectadas = $stmt->rowCount();

                    // Devolver el resultado
                    return $filasAfectadas > 0;
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta de eliminar cuenta.");
                }
            } catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta de eliminar cuenta: " . $e->getMessage());
            }
        }
    }
?>