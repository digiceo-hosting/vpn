<?php
    class PlanGimnasioModel{
        private $conn;
        private $table_bd = "Plan_Gimnasio";

        //DATOS DE LOS PLANES DEL GIMNASIO
        public $PlanGimnasioID; //01
        public $NombrePlanGimnasio; //Black
        public $DescripcionPlanGimnasio; //Incluye Gimnasio y Sauna
        public $CostoPlanGimnasio; //$520.00
        
        public function __construct($db)
        {
            $this->conn = $db;
        }
        public function registrarPlanGimnasio() {
            try{
                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this-> NombrePlanGimnasio = htmlspecialchars(strip_tags($this->NombrePlanGimnasio));
                $this-> DescripcionPlanGimnasio = htmlspecialchars(strip_tags($this->DescripcionPlanGimnasio));
                $this-> CostoPlanGimnasio = htmlspecialchars(strip_tags($this->CostoPlanGimnasio));

                //Proceso de registro de los planes del gimnasio
                $queryRegistroPlanGimnasio = "INSERT INTO $this->table_bd (NombrePlanGimnasio,DescripcionPlanGimnasio,CostoPlanGimnasio) VALUES (:NombrePlanGimnasio,:DescripcionPlanGimnasio,:CostoPlanGimnasio)";
                // Preparar la declaración SQL
                $stmt = $this->conn->prepare($queryRegistroPlanGimnasio);
                // Vincular parámetros
                $stmt->bindParam(":NombrePlanGimnasio", $this->NombrePlanGimnasio);
                $stmt->bindParam(":DescripcionPlanGimnasio", $this->DescripcionPlanGimnasio);
                $stmt->bindParam(":CostoPlanGimnasio", $this->CostoPlanGimnasio);
                // Ejecutar la consulta
                if ($stmt->execute()) {
                    // Devolver el PlanGimnasioID generado
                    return $this->conn->lastInsertId();
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            }catch(PDOException $e){
                throw new PDOException(
                    "Error en la ejecución de la consulta: " . $e->getMessage()
                );
            }
        }
        public function modificarPlanGimnasio() {
            try{
                $querymodificarPlanGimnasio = "UPDATE $this->table_bd SET 
                NombrePlanGimnasio=:NombrePlanGimnasio,DescripcionPlanGimnasio=:DescripcionPlanGimnasio,CostoPlanGimnasio=:CostoPlanGimnasio WHERE PlanGimnasioID = :PlanGimnasioID";
                // Preparar la declaración SQL
                $stmt = $this->conn->prepare($querymodificarPlanGimnasio);
                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this-> PlanGimnasioID = htmlspecialchars(strip_tags($this->PlanGimnasioID));
                $this-> NombrePlanGimnasio = htmlspecialchars(strip_tags($this->NombrePlanGimnasio));
                $this-> DescripcionPlanGimnasio = htmlspecialchars(strip_tags($this->DescripcionPlanGimnasio));
                $this-> CostoPlanGimnasio = htmlspecialchars(strip_tags($this->CostoPlanGimnasio));
                // Vincular parámetros
                $stmt->bindParam(":PlanGimnasioID", $this->PlanGimnasioID);
                $stmt->bindParam(":NombrePlanGimnasio", $this->NombrePlanGimnasio);
                $stmt->bindParam(":DescripcionPlanGimnasio", $this->DescripcionPlanGimnasio);
                $stmt->bindParam(":CostoPlanGimnasio", $this->CostoPlanGimnasio);
                // Ejecutar la consulta
                if ($stmt->execute()) {
                    // Verificar cuántas filas fueron afectadas
                    $filasAfectadas = $stmt->rowCount();
                    // Devolver el resultado
                    return $filasAfectadas > 0;
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            }catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta: " . $e->getMessage());
            }
        }
        public function eliminarPlanGimnasio() {
            try{
                $queryeliminarPlanGimnasio = "DELETE FROM $this->table_bd WHERE PlanGimnasioID = :PlanGimnasioID";
        
                // Preparar la declaración SQL
                $stmt = $this->conn->prepare($queryeliminarPlanGimnasio);
        
                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this->PlanGimnasioID = htmlspecialchars(strip_tags($this->PlanGimnasioID));
        
                // Vincular parámetros
                $stmt->bindParam(":PlanGimnasioID", $this->PlanGimnasioID);
        
                // Ejecutar la consulta
                if ($stmt->execute()) {
                    // Verificar cuántas filas fueron afectadas
                    $filasAfectadas = $stmt->rowCount();
                    // Devolver el resultado
                    return $filasAfectadas > 0;
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            }catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta: " . $e->getMessage());
            }
        }
        public function buscarPlanGimnasio() {
            try{
                $queryBuscarPlanGimnasio = "SELECT PlanGimnasioID, NombrePlanGimnasio,DescripcionPlanGimnasio,CostoPlanGimnasio FROM $this->table_bd WHERE PlanGimnasioID = :PlanGimnasioID";
                $stmt = $this->conn->prepare($queryBuscarPlanGimnasio);
                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this->PlanGimnasioID = htmlspecialchars(strip_tags($this->PlanGimnasioID));
                $stmt->bindParam(":PlanGimnasioID", $this->PlanGimnasioID);
                // Ejecuta la consulta
                if ($stmt->execute()) {
                    // Devuelve el resultado (puedes ajustar esto según tus necesidades)
                    return $stmt->fetch(PDO::FETCH_ASSOC);
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            }catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta: " . $e->getMessage());
            }
        }
        public function listarPlanGimnasio() {
            try{
                $queryListarPlanGimnasio = "SELECT PlanGimnasioID,NombrePlanGimnasio,DescripcionPlanGimnasio,CostoPlanGimnasio FROM $this->table_bd";
                $stmt = $this->conn->prepare($queryListarPlanGimnasio);
                // Ejecuta la consulta
                if ($stmt->execute()) {
                    // Devuelve todos los resultados como un arreglo asociativo
                    return $stmt->fetchAll(PDO::FETCH_ASSOC);
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            }catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta: " . $e->getMessage());
            }
        }
    }
?>