<?php
    class CuponGimnasioModel{
        private $conn;
        private $table_bd = "Cupones_Gimnasio";

        //DATOS DE LOS CUPONES DEL GIMNASIO
        public $CuponID; //01
        public $NombreCupon;
        public $TipoDescuento;
        public $ValorDescuento;
        public $FechaExpiracion;
        public $DescripcionCupon;
        
        public function __construct($db)
        {
            $this->conn = $db;
        }
        public function registrarCuponGimnasio() {
            try{
                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this-> NombreCupon = htmlspecialchars(strip_tags($this->NombreCupon));
                $this-> TipoDescuento = htmlspecialchars(strip_tags($this->TipoDescuento));
                $this-> ValorDescuento = htmlspecialchars(strip_tags($this->ValorDescuento));
                $this-> FechaExpiracion = htmlspecialchars(strip_tags($this->FechaExpiracion));
                $this-> DescripcionCupon = htmlspecialchars(strip_tags($this->DescripcionCupon));

                //Proceso de registro de los cupones del gimnasio
                $queryRegistroCuponGimnasio = "INSERT INTO $this->table_bd (NombreCupon,TipoDescuento,ValorDescuento,FechaExpiracion,DescripcionCupon) VALUES (:NombreCupon,:TipoDescuento,:ValorDescuento,:FechaExpiracion,:DescripcionCupon)";
                // Preparar la declaración SQL
                $stmt = $this->conn->prepare($queryRegistroCuponGimnasio);
                // Vincular parámetros
                $stmt->bindParam(":NombreCupon", $this->NombreCupon);
                $stmt->bindParam(":TipoDescuento", $this->TipoDescuento);
                $stmt->bindParam(":ValorDescuento", $this->ValorDescuento);
                $stmt->bindParam(":FechaExpiracion", $this->FechaExpiracion);
                $stmt->bindParam(":DescripcionCupon", $this->DescripcionCupon);
                // Ejecutar la consulta
                if ($stmt->execute()) {
                    // Devolver el PlanGimnasioID generado
                    return $this->conn->lastInsertId();
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            }catch(PDOException $e){
                throw new PDOException(
                    "Error en la ejecución de la consulta: " . $e->getMessage()
                );
            }
        }
        public function modificarCuponGimnasio() {
            try{
                $querymodificarCuponGimnasio = "UPDATE $this->table_bd SET 
                NombreCupon=:NombreCupon, TipoDescuento=:TipoDescuento, ValorDescuento=:ValorDescuento, FechaExpiracion=:FechaExpiracion, DescripcionCupon=:DescripcionCupon  WHERE CuponID = :CuponID";
                // Preparar la declaración SQL
                $stmt = $this->conn->prepare($querymodificarCuponGimnasio);
                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this-> CuponID = htmlspecialchars(strip_tags($this->CuponID));
                $this-> NombreCupon = htmlspecialchars(strip_tags($this->NombreCupon));
                $this-> TipoDescuento = htmlspecialchars(strip_tags($this->TipoDescuento));
                $this-> ValorDescuento = htmlspecialchars(strip_tags($this->ValorDescuento));
                $this-> FechaExpiracion = htmlspecialchars(strip_tags($this->FechaExpiracion));
                $this-> DescripcionCupon = htmlspecialchars(strip_tags($this->DescripcionCupon));
                // Vincular parámetros
                $stmt->bindParam(":CuponID", $this->CuponID);
                $stmt->bindParam(":NombreCupon", $this->NombreCupon);
                $stmt->bindParam(":TipoDescuento", $this->TipoDescuento);
                $stmt->bindParam(":ValorDescuento", $this->ValorDescuento);
                $stmt->bindParam(":FechaExpiracion", $this->FechaExpiracion);
                $stmt->bindParam(":DescripcionCupon", $this->DescripcionCupon);
                // Ejecutar la consulta
                if ($stmt->execute()) {
                    // Verificar cuántas filas fueron afectadas
                    $filasAfectadas = $stmt->rowCount();
                    // Devolver el resultado
                    return $filasAfectadas > 0;
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            }catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta: " . $e->getMessage());
            }
        }
        public function eliminarCuponGimnasio() {
            try{
                $queryeliminarCuponGimnasio = "DELETE FROM $this->table_bd WHERE CuponID = :CuponID";
        
                // Preparar la declaración SQL
                $stmt = $this->conn->prepare($queryeliminarCuponGimnasio);
        
                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this->CuponID = htmlspecialchars(strip_tags($this->CuponID));
        
                // Vincular parámetros
                $stmt->bindParam(":CuponID", $this->CuponID);
        
                // Ejecutar la consulta
                if ($stmt->execute()) {
                    // Verificar cuántas filas fueron afectadas
                    $filasAfectadas = $stmt->rowCount();
                    // Devolver el resultado
                    return $filasAfectadas > 0;
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            }catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta: " . $e->getMessage());
            }
        }
        public function buscarCuponGimnasio() {
            try{
                $queryBuscarCuponGimnasio = "SELECT CuponID,NombreCupon,TipoDescuento,ValorDescuento,FechaExpiracion,DescripcionCupon,FechaCreacion FROM $this->table_bd WHERE CuponID = :CuponID";
                $stmt = $this->conn->prepare($queryBuscarCuponGimnasio);
                // Limpia y filtra los datos antes de insertarlos en la base de datos
                $this->CuponID = htmlspecialchars(strip_tags($this->CuponID));
                $stmt->bindParam(":CuponID", $this->CuponID);
                // Ejecuta la consulta
                if ($stmt->execute()) {
                    // Devuelve el resultado (puedes ajustar esto según tus necesidades)
                    return $stmt->fetch(PDO::FETCH_ASSOC);
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            }catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta: " . $e->getMessage());
            }
        }
        public function listarCuponGimnasio() {
            try{
                $queryListarCuponGimnasio = "SELECT CuponID,NombreCupon,TipoDescuento,ValorDescuento,FechaExpiracion,DescripcionCupon,FechaCreacion  FROM $this->table_bd";
                $stmt = $this->conn->prepare($queryListarCuponGimnasio);
                // Ejecuta la consulta
                if ($stmt->execute()) {
                    // Devuelve todos los resultados como un arreglo asociativo
                    return $stmt->fetchAll(PDO::FETCH_ASSOC);
                } else {
                    // Lanzar una excepción en caso de error
                    throw new PDOException("Error en la ejecución de la consulta.");
                }
            }catch (PDOException $e) {
                throw new PDOException("Error en la ejecución de la consulta: " . $e->getMessage());
            }
        }
    }
?>