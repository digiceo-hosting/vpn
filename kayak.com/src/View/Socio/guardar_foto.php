<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_FILES['foto'])) {
        $fotoTemporal = $_FILES['foto']['tmp_name'];
        $nombreFoto = uniqid() . '.png';  // Genera un nombre único para la imagen
        $rutaDestino = './../../../assets/img/fotosSocios/' . $nombreFoto;  // Cambia esto según tu lógica de almacenamiento
        move_uploaded_file($fotoTemporal, $rutaDestino);

        // Devuelve el nombre de la imagen como respuesta JSON
        echo json_encode(['nombreFoto' => $nombreFoto]);
    } else {
        echo json_encode(['error' => 'No se recibió ninguna imagen.']);
    }
} else {
    echo json_encode(['error' => 'Solicitud no válida.']);
}
?>
