<?php
    require '../Models/PagosGimnasioModel.php';
    require '../Config/DataBase.php';

    // Realizar la conexión a la base de datos
    $database = new Database();
    $db = $database->conectar();

    //Inicializar el obejto PagosModel
    $PagosM = new PagosGimnasioModel($db);
    //Obtener el método de la solicitud HTTP
    $requestMethod = $_SERVER['REQUEST_METHOD'];

    // TRY: CONTROLA LOS METODOS [GET-POST-PATCH-DALETE] Y EXCEPTION A ERROR 500
    try {
        if (isset($_GET['action'])) {
            $action = $_GET['action'];
            $data = json_decode(file_get_contents("php://input"));
            // Ejecutar el manejo según el método de la solicitud HTTP
            switch ($requestMethod) {
                case 'GET':
                    handleGetRequest($action, $data, $PagosM);
                    break;
                case 'POST':
                    handlePostRequest($action, $data, $PagosM);
                    break;
                case 'PATCH':
                    handlePatchRequest($action, $data, $PagosM);
                    break;
                case 'DELETE':
                    handleDeleteRequest($action, $data, $PagosM);
                    break;
                default:
                    http_response_code(404);
                    echo json_encode([
                        'Message' => 'Solicitud no válida.'
                    ], JSON_UNESCAPED_UNICODE);
                    break;
            }
        } else {
            http_response_code(404);
            echo json_encode([
                'Message' => 'No hay acción. URL: /PagoGimnasioController/'
            ], JSON_UNESCAPED_UNICODE);
        }
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode([
            'Message' => 'Error interno del servidor. Detalles: ' . $e->getMessage() . ' en línea ' . $e->getLine()
        ], JSON_UNESCAPED_UNICODE);
    }

    // Función para manejar las solicitudes POST
    function handlePostRequest($action, $data, $PagosM)
    {
        switch ($action) {
            case 'registrarPago':
                $SocioID = $data->SocioID;
                $monto = $data->Monto;
                $ProximoPagoPlan = $data->FechaPago;
                $cupon = $data->Cupon;

                $ResultadoPagoGimnasioModel = $PagosM->registrarPago($SocioID, $monto ,$ProximoPagoPlan,$cupon);
                // Verificar el resultado del registro
                if ($ResultadoPagoGimnasioModel) {
                    // Registro exitoso
                    http_response_code(200);
                    echo json_encode(array('message' => 'Se guardo pago del socio.', 'data' => $ResultadoPagoGimnasioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    // Error en el registro
                    http_response_code(400);
                    echo json_encode(array('message' => 'Error al guardar pago del socio', 'data' => $ResultadoPagoGimnasioModel), JSON_UNESCAPED_UNICODE);
                }
                break;
            case 'CrearPagoGimnasio':
                $PagosM-> SocioID = $data-> SocioID;
                $PagosM-> Monto = $data-> Monto;
                $PagosM-> FechaPago = $data-> FechaPago;
                $PagosM-> EstadoPago = $data-> EstadoPago;
                $PagosM-> Cupon = $data-> Cupon;

                $ResultadoPagoGimnasioModel = $PagosM->CrearPagoGimnasio();

                // Verificar el resultado del registro
                if ($ResultadoPagoGimnasioModel) {
                    // Registro exitoso
                    http_response_code(200);
                    echo json_encode(array('message' => 'Se guardo pago del socio.', 'data' => $ResultadoPagoGimnasioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    // Error en el registro
                    http_response_code(400);
                    echo json_encode(array('message' => 'Error al guardar pago del socio.', 'data' => $ResultadoPagoGimnasioModel), JSON_UNESCAPED_UNICODE);
                }
                break;
            case 'buscarPagoGimnasio':
                $PagosM-> PagoID = $data-> PagoID;
                $ResultadoPagoGimnasioModel = $PagosM->buscarPagoGimnasio();
                // Verificar el resultado del registro
                if ($ResultadoPagoGimnasioModel) {
                    // Registro exitoso
                    http_response_code(200);
                    echo json_encode(array('message' => 'Se encontro pago del socio.', 'data' => $ResultadoPagoGimnasioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    // Error en el registro
                    http_response_code(400);
                    echo json_encode(array('message' => 'Error al buscar pago del socio.', 'data' => $ResultadoPagoGimnasioModel), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                echo json_encode(['Message' => 'Acción POST desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes GET
    function handleGetRequest($action, $data, $PagosM)
    {
        switch ($action) {
            case 'obtenerProximoPago':
                $PagosM-> fechaInicioSuscripcion = $data-> fechaInicioSuscripcion;
                $PagosM-> duracionPlan = $data-> duracionPlan;
                
                $ResultadoPagoGimnasioModel = $PagosM->obtenerProximoPago('2024-02-05',1);
                // Verificar el resultado del registro
                if ($ResultadoPagoGimnasioModel) {
                    // Registro exitoso
                    http_response_code(200);
                    echo json_encode(array('message' => 'ResultadoPagoGimnasioModel.', 'data' => $ResultadoPagoGimnasioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    // Error en el registro
                    http_response_code(400);
                    echo json_encode(array('message' => 'ResultadoPagoGimnasioModel.', 'data' => $ResultadoPagoGimnasioModel), JSON_UNESCAPED_UNICODE);
                }
                break;
            case 'validarEstadoPago':
                $ResultadoPagoGimnasioModel = $PagosM->validarEstadoPago('2024-02-05',10021003);
                // Verificar el resultado del registro
                if ($ResultadoPagoGimnasioModel) {
                    // Registro exitoso
                    http_response_code(200);
                    echo json_encode(array('message' => 'ResultadoPagoGimnasioModel.', 'data' => $ResultadoPagoGimnasioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    // Error en el registro
                    http_response_code(400);
                    echo json_encode(array('message' => 'ResultadoPagoGimnasioModel.', 'data' => $ResultadoPagoGimnasioModel), JSON_UNESCAPED_UNICODE);
                }
                break;
            case 'listarPagoGimnasio':
                $ResultadoPagoGimnasioModel = $PagosM->listarPagoGimnasio();

                // Verificar el resultado del registro
                if ($ResultadoPagoGimnasioModel) {
                    // Registro exitoso
                    http_response_code(200);
                    echo json_encode(array('message' => 'Lista de pagos de los socios.', 'data' => $ResultadoPagoGimnasioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    // Error en el registro
                    http_response_code(400);
                    echo json_encode(array('message' => 'Error al listar pago de los socios', 'data' => $ResultadoPagoGimnasioModel), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                http_response_code(404);
                echo json_encode(['Message' => 'Acción GET desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes PATCH
    function handlePatchRequest($action, $data, $PagosM)
    {
        switch ($action) {
            case 'modificarPagoGimnasio':
                $PagosM-> PagoID = $data-> PagoID;
                $PagosM-> SocioID = $data-> SocioID;
                $PagosM-> Monto = $data-> Monto;
                $PagosM-> FechaPago = $data-> FechaPago;
                $PagosM-> EstadoPago = $data-> EstadoPago;
                $PagosM-> Cupon = $data-> Cupon;

                $ResultadoPagoGimnasioModel = $PagosM->modificarPagoGimnasio();

                // Verificar el resultado del registro
                if ($ResultadoPagoGimnasioModel) {
                    // Registro exitoso
                    http_response_code(200);
                    echo json_encode(array('message' => 'Se modifico pago del socio.', 'data' => $ResultadoPagoGimnasioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    // Error en el registro
                    http_response_code(400);
                    echo json_encode(array('message' => 'Error al modificar pago del socio.', 'data' => $ResultadoPagoGimnasioModel), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                echo json_encode(['Message' => 'Acción PATCH desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes DELETE
    function handleDeleteRequest($action, $data, $PagosM)
    {
        switch ($action) {
            case 'eliminarPagoGimnasio':
                $PagosM-> PagoID = $data-> PagoID;
                $ResultadoPagoGimnasioModel = $PagosM->eliminarPagoGimnasio();
                // Verificar el resultado del registro
                if ($ResultadoPagoGimnasioModel) {
                    // Registro exitoso
                    http_response_code(200);
                    echo json_encode(array('message' => 'Se elimino pago del socio.', 'data' => $ResultadoPagoGimnasioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    // Error en el registro
                    http_response_code(400);
                    echo json_encode(array('message' => 'Error al eliminar pago del socio.', 'data' => $ResultadoPagoGimnasioModel), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                echo json_encode(['Message' => 'Acción DELETE desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }