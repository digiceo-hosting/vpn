<?php
    require '../Models/InicioSesionModel.php';
    require 'mailcontroller.php';
    require '../../vendor/autoload.php';
    require '../Config/DataBase.php';

    // Realizar la conexión a la base de datos
    $database = new Database();
    $db = $database->conectar();
    // Inicializar el objeto InicioSesionModel
    $InicioSesion = new InicioSesionModel($db);
    // Obtener el método de la solicitud HTTP
    $requestMethod = $_SERVER['REQUEST_METHOD'];

    try {
        // Manejar solicitudes AJAX
        if (isset($_GET['action'])) {
            $action = $_GET['action'];
            $data = json_decode(file_get_contents("php://input"));

            // echo "Contenido de \$data: ";
            // var_dump($data);
            // echo "Contenido de \$action: ";
            // var_dump($action);
            // echo "Contenido de \$requestMethod: ";
            // var_dump($requestMethod);

            // Ejecutar el manejo según el método de la solicitud HTTP
            switch ($requestMethod) {
                case 'GET':
                    handleGetRequest($action, $data, $InicioSesion);
                    break;
                case 'POST':
                    handlePostRequest($action, $data, $InicioSesion);
                    break;
                case 'PATCH':
                    handlePatchRequest($action, $data, $InicioSesion);
                    break;
                case 'DELETE':
                    handleDeleteRequest($action, $data, $InicioSesion);
                    break;
                default:
                    http_response_code(404);
                    echo json_encode(['Message' => 'Solicitud no válida.'], JSON_UNESCAPED_UNICODE);
                    break;
            }
        } else {
            http_response_code(404);
            echo json_encode(['Message' => 'No hay acción. URL: /InicioSesionController/'], JSON_UNESCAPED_UNICODE);
        }
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['Message' => 'Error interno del servidor. Detalles: ' . $e->getMessage() . ' en línea ' . $e->getLine()], JSON_UNESCAPED_UNICODE);
    }
    // Función para manejar las solicitudes POST
    function handlePostRequest($action, $data, $InicioSesion)
    {
        switch ($action) {
            case 'inicioSesion':
                $InicioSesion->CorreoUser = $data->CorreoUser;
                $InicioSesion->ClaveUser = $data->ClaveUser;

                $result = $InicioSesion->inicioSesionCuenta();

                if ($result) {
                    http_response_code(200);
                    echo json_encode(array('message' => 'Inicio de sesion exitosamente.', 'data' => $result), JSON_UNESCAPED_UNICODE);
                } else {
                    http_response_code(404);
                    echo json_encode(array('message' => 'Error al inicio de sesion. Usuario no encontrado.'), JSON_UNESCAPED_UNICODE);
                }
                break;
            case 'crearCuenta':
                $InicioSesion->NickUser = $data->NickUser;
                $InicioSesion->NombreUser = $data->NombreUser;
                $InicioSesion->ApellidoUser = $data->ApellidoUser;
                $InicioSesion->CorreoUser = $data->CorreoUser;
                $InicioSesion->ClaveUser = $data->ClaveUser;
                $InicioSesion->RolUser = $data->RolUser;
                $InicioSesion->OficinaUser = $data->OficinaUser;

                $registroResult = $InicioSesion->registrarCuenta();
                
                // Verificar el resultado del registro
                if ($registroResult) {
                    // Registro exitoso
                    http_response_code(200);
                    echo json_encode(array('message' => 'Inicio de sesion exitosamente.', 'data' => $registroResult), JSON_UNESCAPED_UNICODE);
                } else {
                    // Error en el registro
                    http_response_code(400);
                    echo json_encode(array('message' => 'Correo ya existe en el sistema.', 'data' => $registroResult), JSON_UNESCAPED_UNICODE);
                }
                break;
            case 'recuperarCuenta':
                    $InicioSesion->CorreoUser = $data->CorreoUser;
    
                    $result = $InicioSesion->recuperacionCuenta();
    
                    if ($result) {
                        //funcion PHPMailing
                        $nameDestinatario = $result['NombreUser'];
                        $emailDestinatarios = $result['CorreoUser'];
                        $emailClaveUser = $result['ClaveUser'];
                        $asuntoDestinatario = 'Recuperacion de cuenta';
                        $plantilla_Recuperacion_Cuenta = '
                            <!DOCTYPE html>
                            <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">

                            <head>
                                <meta charset="UTF-8">
                                <meta name="viewport" content="width=device-width,initial-scale=1">
                                <meta name="x-apple-disable-message-reformatting">
                                <style>
                                table,
                                td,
                                div,
                                h1,
                                p {
                                    font-family: Arial, sans-serif;
                                }
                                </style>
                            </head>

                            <body style="margin:0;padding:0;">
                                <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
                                <tr>
                                    <td align="center" style="padding:0;">
                                    <table role="presentation" style="width:602px;border-collapse:collapse;border:1px solid #cccccc;border-spacing:0;text-align:left;">
                                        <tr>
                                        <td align="center" style="padding:40px 0 30px 0;background:#022345;">
                                            
                                        </td>
                                        </tr>
                                        <tr>
                                        <td style="padding:36px 30px 42px 30px;">
                                            <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
                                            <tr>
                                                <td style="padding:0 0 36px 0;color:#153643;">
                                                <h1 style="font-size:24px;margin:0 0 20px 0;font-family:Arial,sans-serif;">Sistema  Gimnasio - Recuperación de cuenta</h1>
                                                <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">'.$nameDestinatario.' Adjuntamos su credenciales importante actulizar su contraseña.'.'<br> Correo Electronico: '.$emailDestinatarios.'<br> Clave de cuenta: '.$emailClaveUser.'</p>
                                                </td>
                                            </tr>
                                            </table>
                                        </td>
                                        </tr>
                                    </table>
                                    </td>
                                </tr>
                                </table>
                            </body>

                            </html>
                        ';
                        // Crea una instancia de PHPMailerSender, implementando EmailSenderInterface
                        $emailDestinatarios = [
                            ['email' => $result['CorreoUser'], 'name' => 'name']
                        ];

                        $bodyDestinatario = $plantilla_Recuperacion_Cuenta;
                        $emailSender = new PHPMailerSender();
                        // Crea una instancia de MailController con la instancia de PHPMailerSender
                        $mailController = new MailController($emailSender);
                        // Llama al método no estático enviarEmail en la instancia
                        $resp = $mailController->enviarEmail($emailDestinatarios, $nameDestinatario, $asuntoDestinatario, $bodyDestinatario, 'smtp.gmail.com', 'austintv52@gmail.com', 'Agustin Arenas', 'otfn alwg efgf adex', '465');
                        // Puedes imprimir la respuesta o hacer algo más con ella
                        // echo json_encode($resp);
                        http_response_code(200);
                        echo json_encode(array('message' => 'Recuperacion de cuenta exitosamente.', 'data' => $result), JSON_UNESCAPED_UNICODE);
                    } else {
                        http_response_code(404);
                        echo json_encode(array('message' => 'Error al recuperar la cuenta. Usuario no encontrado.'), JSON_UNESCAPED_UNICODE);
                    }
                    break;
            default:
                echo json_encode(['Message' => 'Acción POST desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes GET
    function handleGetRequest($action, $data, $InicioSesion)
    {
        switch ($action) {
            default:
                http_response_code(404);
                echo json_encode(['Message' => 'Acción GET desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes PATCH
    function handlePatchRequest($action, $data, $InicioSesion)
    {
        switch ($action) {
            case 'modificarCuenta':
                $InicioSesion->InicioSesionID = $data->InicioSesionID;
                $InicioSesion->NickUser = $data->NickUser;
                $InicioSesion->NombreUser = $data->NombreUser;
                $InicioSesion->ApellidoUser = $data->ApellidoUser;
                $InicioSesion->CorreoUser = $data->CorreoUser;
                $InicioSesion->ClaveUser = $data->ClaveUser;
                $InicioSesion->RolUser = $data->RolUser;
                $InicioSesion->OficinaUser = $data->OficinaUser;

                $result = $InicioSesion->modificarCuenta();

                if ($result) {
                    http_response_code(200);
                    echo json_encode(array('message' => 'Se modifico exitosamente la cuenta.', 'data' => $result), JSON_UNESCAPED_UNICODE);
                } else {
                    http_response_code(404);
                    echo json_encode(array('message' => 'Error al modificar la cuenta. Usuario no encontrado.'), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                echo json_encode(['Message' => 'Acción PATCH desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes DELETE
    function handleDeleteRequest($action, $data, $InicioSesion)
    {
        switch ($action) {
            case 'eliminarCuenta':
                $InicioSesion->InicioSesionID = $data->InicioSesionID;

                $result = $InicioSesion->eliminarCuenta();

                if ($result) {
                    http_response_code(200);
                    echo json_encode(array('message' => 'Elimino la cuenta.', 'data' => $result), JSON_UNESCAPED_UNICODE);
                } else {
                    http_response_code(404);
                    echo json_encode(array('message' => 'Error al eliminar la cuenta.'), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                echo json_encode(['Message' => 'Acción DELETE desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
