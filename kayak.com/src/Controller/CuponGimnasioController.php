<?php
    require '../Models/CuponGimnasioModel.php';
    require '../Config/DataBase.php';

    // Realizar la conexión a la base de datos
    $database = new Database();
    $db = $database->conectar();

    //Inicializar el obejto CuponGimnasioModel
    $CuponGimnasioM = new CuponGimnasioModel($db);
    //Obtener el método de la solicitud HTTP
    $requestMethod = $_SERVER['REQUEST_METHOD'];

    // TRY: CONTROLA LOS METODOS [GET-POST-PATCH-DALETE] Y EXCEPTION A ERROR 500
    try{
        if (isset($_GET['action'])) {
            $action = $_GET['action'];
            $data = json_decode(file_get_contents("php://input"));
            // Ejecutar el manejo según el método de la solicitud HTTP
            switch ($requestMethod) {
                case 'GET':
                    handleGetRequest($action, $data, $CuponGimnasioM);
                    break;
                case 'POST':
                    handlePostRequest($action, $data, $CuponGimnasioM);
                    break;
                case 'PATCH':
                    handlePatchRequest($action, $data, $CuponGimnasioM);
                    break;
                case 'DELETE':
                    handleDeleteRequest($action, $data, $CuponGimnasioM);
                    break;
                default:
                    http_response_code(404);
                    echo json_encode([
                        'Message' => 'Solicitud no válida.'
                    ], JSON_UNESCAPED_UNICODE);
                    break;
            }
        } else {
            http_response_code(404);
            echo json_encode([
                'Message' => 'No hay acción. URL: /PlanGimnasioController/'
            ], JSON_UNESCAPED_UNICODE);
        }

    }catch(Exception $e){
        http_response_code(500);
        echo json_encode([
            'Message' => 'Error interno del servidor. Detalles: ' . $e->getMessage() . ' en línea ' . $e->getLine()
        ], JSON_UNESCAPED_UNICODE);
    }

    // Función para manejar las solicitudes POST
    function handlePostRequest($action, $data, $CuponGimnasioM)
    {
        switch ($action) {
            case 'registrarCuponGimnasio':
                $CuponGimnasioM-> CuponID = $data-> CuponID;
                $CuponGimnasioM-> NombreCupon = $data-> NombreCupon;
                $CuponGimnasioM-> TipoDescuento = $data-> TipoDescuento;
                $CuponGimnasioM-> ValorDescuento = $data-> ValorDescuento;
                $CuponGimnasioM-> FechaExpiracion = $data-> FechaExpiracion;
                $CuponGimnasioM-> DescripcionCupon = $data-> DescripcionCupon;

                $ResultadoCuponGimnasioModel = $CuponGimnasioM->registrarCuponGimnasio();
                // Verificar el resultado del registro
                if ($ResultadoCuponGimnasioModel) {
                    // Registro exitoso
                    http_response_code(200);
                    echo json_encode(array('message' => 'Se registro cupon.', 'data' => $ResultadoCuponGimnasioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    // Error en el registro
                    http_response_code(400);
                    echo json_encode(array('message' => 'Error al registrar cupon.', 'data' => $ResultadoCuponGimnasioModel), JSON_UNESCAPED_UNICODE);
                }
                break;
            case 'buscarCuponGimnasio':
                $CuponGimnasioM-> CuponID = $data-> CuponID;
                $ResultadoCuponGimnasioModel = $CuponGimnasioM->buscarCuponGimnasio();
                // Verificar el resultado del registro
                if ($ResultadoCuponGimnasioModel) {
                    // Registro exitoso
                    http_response_code(200);
                    echo json_encode(array('message' => 'Se encontro cupon.', 'data' => $ResultadoCuponGimnasioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    // Error en el registro
                    http_response_code(400);
                    echo json_encode(array('message' => 'No existe cupon.', 'data' => $ResultadoCuponGimnasioModel), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                echo json_encode(['Message' => 'Acción POST desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes GET
    function handleGetRequest($action, $data, $CuponGimnasioM)
    {
        switch ($action) {
            case 'listarCuponGimnasio':
                $ResultadoCuponGimnasioModel = $CuponGimnasioM->listarCuponGimnasio();
                // Verificar el resultado del registro
                if ($ResultadoCuponGimnasioModel) {
                    // Registro exitoso
                    http_response_code(200);
                    echo json_encode(array('message' => 'Lista cupon.', 'data' => $ResultadoCuponGimnasioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    // Error en el registro
                    http_response_code(400);
                    echo json_encode(array('message' => 'Error al listar', 'data' => $ResultadoCuponGimnasioModel), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                http_response_code(404);
                echo json_encode(['Message' => 'Acción GET desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes PATCH
    function handlePatchRequest($action, $data, $CuponGimnasioM)
    {
        switch ($action) {
            case 'modificarCuponGimnasio':
                $CuponGimnasioM-> CuponID = $data-> CuponID;
                $CuponGimnasioM-> NombreCupon = $data-> NombreCupon;
                $CuponGimnasioM-> TipoDescuento = $data-> TipoDescuento;
                $CuponGimnasioM-> ValorDescuento = $data-> ValorDescuento;
                $CuponGimnasioM-> FechaExpiracion = $data-> FechaExpiracion;
                $CuponGimnasioM-> DescripcionCupon = $data-> DescripcionCupon;

                $ResultadoCuponGimnasioModel = $CuponGimnasioM->modificarCuponGimnasio();
                // Verificar el resultado del registro
                if ($ResultadoCuponGimnasioModel) {
                    // Registro exitoso
                    http_response_code(200);
                    echo json_encode(array('message' => 'Se modifico cupon.', 'data' => $ResultadoCuponGimnasioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    // Error en el registro
                    http_response_code(400);
                    echo json_encode(array('message' => 'Error al modificar cupon.', 'data' => $ResultadoCuponGimnasioModel), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                echo json_encode(['Message' => 'Acción PATCH desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes DELETE
    function handleDeleteRequest($action, $data, $CuponGimnasioM)
    {
        switch ($action) {
            case 'eliminarCuponGimnasio':
                $CuponGimnasioM-> CuponID = $data-> CuponID;
                $ResultadoCuponGimnasioModel = $CuponGimnasioM->eliminarCuponGimnasio();
                // Verificar el resultado del registro
                if ($ResultadoCuponGimnasioModel) {
                    // Registro exitoso
                    http_response_code(200);
                    echo json_encode(array('message' => 'Se elimino cupon.', 'data' => $ResultadoCuponGimnasioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    // Error en el registro
                    http_response_code(400);
                    echo json_encode(array('message' => 'Error al cupon.', 'data' => $ResultadoCuponGimnasioModel), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                echo json_encode(['Message' => 'Acción DELETE desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    