<?php
    require '../Models/SocioModel.php';
    require '../Models/PagosGimnasioModel.php';
    require '../Config/DataBase.php';

    // Realizar la conexión a la base de datos
    $database = new Database();
    $db = $database->conectar();

    //Inicializar el obejto SocioModel
    $SocioM = new SocioModel($db);
    
    //Obtener el método de la solicitud HTTP
    $requestMethod = $_SERVER['REQUEST_METHOD'];

    // TRY: CONTROLA LOS METODOS [GET-POST-PATCH-DALETE] Y EXCEPTION A ERROR 500
    try {
        if (isset($_GET['action'])) {
            $action = $_GET['action'];
            $data = json_decode(file_get_contents("php://input"));
            // Ejecutar el manejo según el método de la solicitud HTTP
            switch ($requestMethod) {
                case 'GET':
                    handleGetRequest($action, $data, $SocioM);
                    break;
                case 'POST':
                    handlePostRequest($action, $data, $SocioM);
                    break;
                case 'PATCH':
                    handlePatchRequest($action, $data, $SocioM);
                    break;
                case 'DELETE':
                    handleDeleteRequest($action, $data, $SocioM);
                    break;
                default:
                    http_response_code(404);
                    echo json_encode([
                        'Message' => 'Solicitud no válida.'
                    ], JSON_UNESCAPED_UNICODE);
                    break;
            }
        } else {
            http_response_code(404);
            echo json_encode([
                'Message' => 'No hay acción. URL: /SocioController/'
            ], JSON_UNESCAPED_UNICODE);
        }
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode([
            'Message' => 'Error interno del servidor. Detalles: ' . $e->getMessage() . ' en línea ' . $e->getLine()
        ], JSON_UNESCAPED_UNICODE);
    }

    // Función para manejar las solicitudes POST
    function handlePostRequest($action, $data, $SocioM)
    {
        switch ($action) {
            case 'CrearSocio':
                // Realizar la conexión a la base de datos
                $database = new Database();
                $db = $database->conectar();
                //Inicializar el obejto PagosModel
                $PagosM = new PagosGimnasioModel($db);

                $SocioM->NombreSocio = $data->NombreSocio;
                $SocioM->ApellidoPartenoSocio = $data->ApellidoPartenoSocio;
                $SocioM->ApellidoMaternoSocio = $data->ApellidoMaternoSocio;
                $SocioM->SexoSocio = $data->SexoSocio;
                $SocioM->CorreoElectronicoSocio = $data->CorreoElectronicoSocio;
                $SocioM->TelefonoSocio = $data->TelefonoSocio;
                $SocioM->FechaNacimientoSocio = $data->FechaNacimientoSocio;
                $SocioM->FotoPerfilSocio = $data->FotoPerfilSocio;
                $SocioM->DireccionCalleSocio = $data->DireccionCalleSocio;
                $SocioM->DireccionColoniaSocio = $data->DireccionColoniaSocio;
                $SocioM->DireccionEstadoSocio = $data->DireccionEstadoSocio;
                $SocioM->CodigoPostalSocio = $data->CodigoPostalSocio;
                $SocioM->PlanGimnasioID = $data->PlanGimnasioID;
                $SocioM->InicioPlanGimnasio = $data->InicioPlanGimnasio;
                $SocioM->PeriodoPlan = $data->PeriodoPlan;
                $ResultadoPagoGimnasioModel = $PagosM->obtenerProximoPago($data->InicioPlanGimnasio,1);
                $SocioM->ProximoPagoPlan = $ResultadoPagoGimnasioModel;

                $ResultadoSocioModel = $SocioM->registrarSocio();

                // Verificar el resultado del registro
                if ($ResultadoSocioModel) {
                    // Registro exitoso
                    http_response_code(200);
                    echo json_encode(['message' => 'Registro de socio exitosamente.', 'data' => $ResultadoSocioModel], JSON_UNESCAPED_UNICODE);
                } else {
                    // Error en el registro
                    http_response_code(400);
                    echo json_encode(['message' => 'Socio ya se encuentra Registrado.', 'data' => $ResultadoSocioModel], JSON_UNESCAPED_UNICODE);
                }
                break;
            case 'BuscarSocio':
                $SocioM->SocioID = $data->SocioID;
                $ResultadoSocioModel = $SocioM->buscarSocio();
                if ($ResultadoSocioModel) {
                    // Realizar la conexión a la base de datos
                    $database = new Database();
                    $db = $database->conectar();
                    //Inicializar el obejto PagosModel
                    $PagosM = new PagosGimnasioModel($db);
                    $ResultadoPagoGimnasioModel = $PagosM->validarEstadoPago($ResultadoSocioModel['ProximoPagoPlan'],$ResultadoSocioModel['SocioID']);
                    // Agregar el resultado de Pago al resultado de Socio
                    // $ResultadoSocioModel['ProximoPagoPlan'],$ResultadoSocioModel['SocioID']
                    $ResultadoSocioModel['ResultadoPagoGimnasioModel'] = $ResultadoPagoGimnasioModel;

                    http_response_code(200);
                    echo json_encode(array('message' => $ResultadoPagoGimnasioModel, 'data' => $ResultadoSocioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    http_response_code(404);
                    echo json_encode(array('message' => 'No existe el socio.'), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                echo json_encode(['Message' => 'Acción POST desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes GET
    function handleGetRequest($action, $data, $SocioM)
    {
        switch ($action) {
            case 'ListadoSocios':
                $ResultadoSocioModel = $SocioM->listarSocios();
                if ($ResultadoSocioModel) {
                    http_response_code(200);
                    echo json_encode(array('message' => 'Lista de socios.', 'data' => $ResultadoSocioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    http_response_code(404);
                    echo json_encode(array('message' => 'Error al listar socios.'), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                http_response_code(404);
                echo json_encode(['Message' => 'Acción GET desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes PATCH
    function handlePatchRequest($action, $data, $SocioM)
    {
        switch ($action) {
            case 'ModificarSocio':
                $SocioM->SocioID = $data->SocioID;
                $SocioM->NombreSocio = $data->NombreSocio;
                $SocioM->ApellidoPartenoSocio = $data->ApellidoPartenoSocio;
                $SocioM->ApellidoMaternoSocio = $data->ApellidoMaternoSocio;
                $SocioM->SexoSocio = $data->SexoSocio;
                $SocioM->CorreoElectronicoSocio = $data->CorreoElectronicoSocio;
                $SocioM->TelefonoSocio = $data->TelefonoSocio;
                $SocioM->FechaNacimientoSocio = $data->FechaNacimientoSocio;
                $SocioM->FotoPerfilSocio = $data->FotoPerfilSocio;
                $SocioM->DireccionCalleSocio = $data->DireccionCalleSocio;
                $SocioM->DireccionColoniaSocio = $data->DireccionColoniaSocio;
                $SocioM->DireccionEstadoSocio = $data->DireccionEstadoSocio;
                $SocioM->CodigoPostalSocio = $data->CodigoPostalSocio;
                $SocioM->PlanGimnasioID = $data->PlanGimnasioID;
                $SocioM->InicioPlanGimnasio = $data->InicioPlanGimnasio;
                $SocioM->PeriodoPlan = $data->PeriodoPlan;

                $ResultadoSocioModel = $SocioM->modificarSocio();

                if ($ResultadoSocioModel) {
                    http_response_code(200);
                    echo json_encode(array('message' => 'Se modifico exitosamente el socio.', 'data' => $ResultadoSocioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    http_response_code(404);
                    echo json_encode(array('message' => 'Error al modificar el socio.'), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                echo json_encode(['Message' => 'Acción PATCH desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes DELETE
    function handleDeleteRequest($action, $data, $SocioM)
    {
        switch ($action) {
            case 'EliminarSocio':
                $SocioM->SocioID = $data->SocioID;
                $ResultadoSocioModel = $SocioM->eliminarSocio();
                if ($ResultadoSocioModel) {
                    http_response_code(200);
                    echo json_encode(array('message' => 'Elimino al socio.', 'data' => $ResultadoSocioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    http_response_code(404);
                    echo json_encode(array('message' => 'Error al eliminar al socio.'), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                echo json_encode(['Message' => 'Acción DELETE desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
