<?php
    require '../Models/PlanGimnasioModel.php';
    require '../Config/DataBase.php';

    // Realizar la conexión a la base de datos
    $database = new Database();
    $db = $database->conectar();

    //Inicializar el obejto PlanGimnasioModel
    $PlanGimnasioM = new PlanGimnasioModel($db);
    //Obtener el método de la solicitud HTTP
    $requestMethod = $_SERVER['REQUEST_METHOD'];

    // TRY: CONTROLA LOS METODOS [GET-POST-PATCH-DALETE] Y EXCEPTION A ERROR 500
    try{
        if (isset($_GET['action'])) {
            $action = $_GET['action'];
            $data = json_decode(file_get_contents("php://input"));
            // Ejecutar el manejo según el método de la solicitud HTTP
            switch ($requestMethod) {
                case 'GET':
                    handleGetRequest($action, $data, $PlanGimnasioM);
                    break;
                case 'POST':
                    handlePostRequest($action, $data, $PlanGimnasioM);
                    break;
                case 'PATCH':
                    handlePatchRequest($action, $data, $PlanGimnasioM);
                    break;
                case 'DELETE':
                    handleDeleteRequest($action, $data, $PlanGimnasioM);
                    break;
                default:
                    http_response_code(404);
                    echo json_encode([
                        'Message' => 'Solicitud no válida.'
                    ], JSON_UNESCAPED_UNICODE);
                    break;
            }
        } else {
            http_response_code(404);
            echo json_encode([
                'Message' => 'No hay acción. URL: /PlanGimnasioController/'
            ], JSON_UNESCAPED_UNICODE);
        }

    }catch(Exception $e){
        http_response_code(500);
        echo json_encode([
            'Message' => 'Error interno del servidor. Detalles: ' . $e->getMessage() . ' en línea ' . $e->getLine()
        ], JSON_UNESCAPED_UNICODE);
    }

    // Función para manejar las solicitudes POST
    function handlePostRequest($action, $data, $PlanGimnasioM)
    {
        switch ($action) {
            case 'CrearPlanGimnasio':
                $PlanGimnasioM-> NombrePlanGimnasio = $data-> NombrePlanGimnasio;
                $PlanGimnasioM-> DescripcionPlanGimnasio = $data-> DescripcionPlanGimnasio;
                $PlanGimnasioM-> CostoPlanGimnasio = $data-> CostoPlanGimnasio;

                $ResultadoPlanGimnasioModel = $PlanGimnasioM->registrarPlanGimnasio();

                // Verificar el resultado del registro
                if ($ResultadoPlanGimnasioModel) {
                    // Registro exitoso
                    http_response_code(200);
                    echo json_encode(array('message' => 'Registro de plan exitosamente.', 'data' => $ResultadoPlanGimnasioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    // Error en el registro
                    http_response_code(400);
                    echo json_encode(array('message' => 'Plan ya se encuentra Registrado.', 'data' => $ResultadoPlanGimnasioModel), JSON_UNESCAPED_UNICODE);
                }
                break;
            case 'BuscarPlanGimnasio':
                $PlanGimnasioM->PlanGimnasioID = $data->PlanGimnasioID;
                $ResultadoPlanGimnasioModel = $PlanGimnasioM->buscarPlanGimnasio();
                if ($ResultadoPlanGimnasioModel) {
                    http_response_code(200);
                    echo json_encode(array('message' => 'Se encontro el Plan Gimnasio.', 'data' => $ResultadoPlanGimnasioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    http_response_code(404);
                    echo json_encode(array('message' => 'No existe el Plan Gimnasio.'), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                echo json_encode(['Message' => 'Acción POST desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes GET
    function handleGetRequest($action, $data, $PlanGimnasioM)
    {
        switch ($action) {
            case 'BuscarPlanGimnasio':
                $PlanGimnasioM->PlanGimnasioID = $data->PlanGimnasioID;
                $ResultadoPlanGimnasioModel = $PlanGimnasioM->buscarPlanGimnasio();
                if ($ResultadoPlanGimnasioModel) {
                    http_response_code(200);
                    echo json_encode(array('message' => 'Se encontro el Plan Gimnasio.', 'data' => $ResultadoPlanGimnasioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    http_response_code(404);
                    echo json_encode(array('message' => 'No existe el Plan Gimnasio.'), JSON_UNESCAPED_UNICODE);
                }
                break;
            case 'ListadoPlanGimnasio':
                $ResultadoPlanGimnasioModel = $PlanGimnasioM->listarPlanGimnasio();
                if ($ResultadoPlanGimnasioModel) {
                    http_response_code(200);
                    echo json_encode(array('message' => 'Lista de Planes Gimnasio.', 'data' => $ResultadoPlanGimnasioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    http_response_code(404);
                    echo json_encode(array('message' => 'Error al listar Planes Gimnasio.'), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                http_response_code(404);
                echo json_encode(['Message' => 'Acción GET desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes PATCH
    function handlePatchRequest($action, $data, $PlanGimnasioM)
    {
        switch ($action) {
            case 'ModificarPlanGimnasio':
                $PlanGimnasioM-> PlanGimnasioID = $data-> PlanGimnasioID;
                $PlanGimnasioM-> NombrePlanGimnasio = $data-> NombrePlanGimnasio;
                $PlanGimnasioM-> DescripcionPlanGimnasio = $data-> DescripcionPlanGimnasio;
                $PlanGimnasioM-> CostoPlanGimnasio = $data-> CostoPlanGimnasio;

                $ResultadoSocioModel = $PlanGimnasioM->modificarPlanGimnasio();

                if ($ResultadoSocioModel) {
                    http_response_code(200);
                    echo json_encode(array('message' => 'Se modifico exitosamente el Plan.', 'data' => $ResultadoSocioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    http_response_code(404);
                    echo json_encode(array('message' => 'Error al modificar el Plan. Usuario no encontrado.'), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                echo json_encode(['Message' => 'Acción PATCH desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes DELETE
    function handleDeleteRequest($action, $data, $PlanGimnasioM)
    {
        switch ($action) {
            case 'EliminarPlanGimnasio':
                $PlanGimnasioM->PlanGimnasioID = $data->PlanGimnasioID;
                $ResultadoPlanGimnasioModel = $PlanGimnasioM->eliminarPlanGimnasio();
                if ($ResultadoPlanGimnasioModel) {
                    http_response_code(200);
                    echo json_encode(array('message' => 'Elimino el Plan Gimnasio.', 'data' => $ResultadoPlanGimnasioModel), JSON_UNESCAPED_UNICODE);
                } else {
                    http_response_code(404);
                    echo json_encode(array('message' => 'Error al eliminar el Plan Gimnasio.'), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                echo json_encode(['Message' => 'Acción DELETE desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    