<?php
    require_once './../Controller/mailcontroller.php';
    require './../../vendor/autoload.php';

    if (isset($_POST['subject'], $_POST['message'])) {

        $asuntoEmail = $_POST['subject'];
        // $emailDestinatario = $_POST['email'];
        $nameDestinatario = $_POST['nameDestinatario'];
        $bodyEmail = $_POST['message'];
        $fileData = $_FILES['file'];
        // $emailDestinatarios = [
        //     ['email' => 'power_gugus52@hotmail.com', 'name' => 'Austin TV'],
        //     ['email' => 'teb_005@hotmail.com', 'name' => 'Austin']
        // ];

        $smtpHost = $_POST['smtpHost']; 
        $userEmail = $_POST['userEmail'];  
        $userName = $_POST['userName'];
        $passwordEmail = $_POST['passwordEmail'];  
        $portEmail = $_POST['portEmail'];

        $destinatariosJSON = $_POST['destinatarios'];
        $destinatarios = json_decode($destinatariosJSON, true);
        $emailDestinatarios = $destinatarios;

        $plantilla_Basica = '
            <!DOCTYPE html>
            <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">

            <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width,initial-scale=1">
            <meta name="x-apple-disable-message-reformatting">
            <style>
                table,
                td,
                div,
                h1,
                p {
                font-family: Arial, sans-serif;
                }
            </style>
            </head>

            <body style="margin:0;padding:0;">
            <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
                <tr>
                <td align="center" style="padding:0;">
                    <table role="presentation" style="width:602px;border-collapse:collapse;border:1px solid #cccccc;border-spacing:0;text-align:left;">
                    <tr>
                        <td align="center" style="padding:40px 0 30px 0;background:#022345;">
                        
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:36px 30px 42px 30px;">
                        <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
                            <tr>
                            <td style="padding:0 0 36px 0;color:#153643;">
                                <h1 style="font-size:24px;margin:0 0 20px 0;font-family:Arial,sans-serif;">Sistema  Gimnasio</h1>
                                <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">'.$bodyEmail.'</p>
                                <p style="margin:0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;"><a href="https://grupoozorno.com/" style="color:#51AF5F;text-decoration:underline;">Visita nuestro sitio web.</a></p>
                            </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:30px;background:#51AF5F;">
                        <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;font-size:9px;font-family:Arial,sans-serif;">
                            <tr>
                            <td style="padding:0;width:50%;" align="left">
                                <p style="margin:0;font-size:14px;line-height:16px;font-family:Arial,sans-serif;color:#ffffff;">
                                &reg; Sistema  Gimnasio 2024<br /><a href="https://www.grupoozorno.com/" style="color:#ffffff;text-decoration:underline;">https://www.grupoozorno.com/</a>
                                </p>
                            </td>
                            <td style="padding:0;width:50%;" align="right">
                                <table role="presentation" style="border-collapse:collapse;border:0;border-spacing:0;">
                                <tr>
                                    <td style="padding:0 0 0 10px;width:38px;">
                                    <a href="https://www.facebook.com/seguroz.mx/" style="color:#ffffff;"><img src="https://assets.codepen.io/210284/fb_1.png" alt="Facebook" width="38" style="height:auto;display:block;border:0;" /></a>
                                    </td>
                                </tr>
                                </table>
                            </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    </table>
                </td>
                </tr>
            </table>
            </body>

            </html>
        ';
        $bodyEmail = $plantilla_Basica;
        // $resp = MailController::enviarEmail($emailDestinatario,$nameDestinatario,$asuntoEmail,$bodyEmail,$fileData, $smtpHost,$userEmail,$userName,$passwordEmail,$portEmail);

        // Crea una instancia de PHPMailerSender, implementando EmailSenderInterface
        $emailSender = new PHPMailerSender();

        // Crea una instancia de MailController con la instancia de PHPMailerSender
        $mailController = new MailController($emailSender);

        // Llama al método no estático enviarEmail en la instancia
        $resp = $mailController->enviarEmail($emailDestinatarios, $nameDestinatario, $asuntoEmail, $bodyEmail, $fileData, $smtpHost, $userEmail, $userName, $passwordEmail, $portEmail);

        // Puedes imprimir la respuesta o hacer algo más con ella
        echo json_encode($resp);

    }