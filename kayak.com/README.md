# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

#   Sistema de gimnasio
##   Pull Request QA

Composer Configuracion PHP Unitest:
- composer require --dev phpunit/phpunit ^9
- composer dump-autoload -o
- ./vendor/bin/phpunit tests

#   Lista de comandos Docker para el desarrollador:
>   docker build -t digiceo/sistemagimnasio:version .
>   docker run -d -p 8080:80 --name sistemaweb digiceo/sistemagimnasio:version 
>   docker run -d -p 3000:80 -v $(pwd):/var/www/html/ --name sistemaweb digiceo/sistemagimnasio:version
>   docker exec -it sistemaweb /bin/bash  
