<?php

    use PHPUnit\Framework\TestCase;
    require_once './src/Controller/mailcontroller.php';
    
    class EmailTest extends TestCase
    {
        public function testEnviarEmailSuccess()
        {
            // Mock the EmailSenderInterface
            $emailSenderMock = $this->createMock(EmailSenderInterface::class);
            $emailSenderMock->method('sendEmail')->willReturn(['status' => 200, 'message' => 'Message has been sent']);

            // Create an instance of MailController with the mocked EmailSenderInterface
            $mailController = new MailController($emailSenderMock);

            // Call the enviarEmail method with your specific data
            $result = $mailController->enviarEmail(
                'power_gugus52@hotmail.com', // emailDestinatario
                'Test', // nameDestinatario
                'Test', // asuntoEmail
                'Test', // bodyEmail
                ['tmp_name' => 'path/to/file', 'name' => 'file.txt'], // Sample fileData (you may need to adjust this based on your file input)
                'smtp.gmail.com', // smtpHost
                'austintv52@gmail.com', // userEmail
                'Agustin Arenas', // userName
                'otfn alwg efgf adex', // passwordEmail
                465 // portEmail
            );

            // Assert that the result matches the expected success response
            $this->assertEquals(['status' => 200, 'message' => 'Message has been sent'], $result);
        }

        public function testEnviarEmailFailure()
        {
            // Mock the EmailSenderInterface
            $emailSenderMock = $this->createMock(EmailSenderInterface::class);
            $emailSenderMock->method('sendEmail')->willReturn(['status' => 404, 'message' => 'Error']);

            // Create an instance of MailController with the mocked EmailSenderInterface
            $mailController = new MailController($emailSenderMock);

            // Call the enviarEmail method
            $result = $mailController->enviarEmail(
                'power_gugus52@hotmail.com', // emailDestinatario
                'Test', // nameDestinatario
                'Test', // asuntoEmail
                'Test', // bodyEmail
                ['tmp_name' => 'path/to/file', 'name' => 'file.txt'], // Sample fileData (you may need to adjust this based on your file input)
                'smtp.gmail.com', // smtpHost
                'austintv52@gmail.com', // userEmail
                'Agustin Arenas', // userName
                'otfn alwg efgf adex', // passwordEmail
                465 // portEmail
            );

            // Assert that the result matches the expected failure response
            $this->assertEquals(['status' => 404, 'message' => 'Error'], $result);
        }
    }
