<?php
require_once 'vendor/autoload.php'; // Asegúrate de incluir la ubicación correcta del archivo autoload.php

// Verifica si se enviaron datos desde el formulario
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Credenciales de Twilio
    $sid = 'ACc35669a752af99f36ef8ad36cdbd3424'; // Reemplaza con tu SID de Twilio
    $token = '05ebe5ef47521096c230b5a03e60b9d5'; // Reemplaza con tu token de Twilio
    $twilio = new Twilio\Rest\Client($sid, $token);

    // Número 'From' de Twilio (tu número de WhatsApp)
    $from = 'whatsapp:+14155238886'; // Reemplaza con tu número de WhatsApp de Twilio

    // Obtén el mensaje y el número de WhatsApp del formulario
    $mensaje = $_POST["mensaje"];
    $numero = $_POST["numero"];
    $imagen = 'https://www.google.com/url?sa=i&url=https%3A%2F%2Fes.snhu.edu%2Fnoticias%2Fque-es-gestion-de-tecnologias-de-informacion&psig=AOvVaw38WT4W_-7kL_iEcyuNZ5p6&ust=1704347914200000&source=images&cd=vfe&opi=89978449&ved=0CBEQjRxqFwoTCKCGkvPEwIMDFQAAAAAdAAAAABAD';

    try {
        // Envío del mensaje de WhatsApp
        $message = $twilio->messages->create(
            $numero, 
            array(
            'from' => $from,
            'body' => $mensaje
            //'mediaUrl' => $imagen // Agrega la URL de la imagen al parámetro mediaUrl
            )
        );
        echo "Mensaje enviado. SID: " . $message->sid . ' Número enviado: ' . $numero;
    } catch (Exception $e) {
        echo "Error al enviar el mensaje: " . $e->getMessage();
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Enviar mensaje de WhatsApp</title>
</head>
<body>
    <h2>Enviar mensaje de WhatsApp</h2>
    <form action="" method="post">
        <label for="mensaje">Mensaje:</label><br>
        <textarea id="mensaje" name="mensaje" rows="4" cols="50"></textarea><br><br>
        <label for="numero">Número de WhatsApp:</label><br>
        <input type="text" id="numero" name="numero" placeholder="whatsapp:+5212211276404"><br><br>
        <input type="submit" value="Enviar mensaje">
    </form>
</body>
</html>
