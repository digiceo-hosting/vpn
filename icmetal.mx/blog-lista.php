
                <!doctype html>
                <html class='no-js' lang='en'>
                    <head>
                        <meta charset='UTF-8'>
                        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                        <title>Blog</title>
                        <meta name='viewport' content='width=device-width, initial-scale=1'>
                        <!-- Favicon, robots, description,keywords -->
                        <link rel='shortcut icon' type='image/x-icon' href='./img/engranet/logo.jpg'> 
                        <meta name='robots' content='index,follow'>
                        <meta name='description' content='   '>
                        <meta name='keywords' content='   '>
                        <!-- Favicon, robots, description,keywords -->
                        <!-- Link css, js -->
                        <link rel='stylesheet' href='css/bootstrap.min.css'>
                        <link rel='stylesheet' href='css/animate.min.css'>
                        <link rel='stylesheet' href='css/meanmenu.css'>
                        <link rel='stylesheet' href='css/magnific-popup.css'>
                        <link rel='stylesheet' href='css/owl.carousel.min.css'>
                        <link rel='stylesheet' href='css/font-awesome.min.css'>
                        <link rel='stylesheet' href='css/et-line-icon.css'>
                        <link rel='stylesheet' href='css/reset.css'>
                        <link rel='stylesheet' href='css/ionicons.min.css'>
                        <link rel='stylesheet' href='css/material-design-iconic-font.min.css'>
                        <link rel='stylesheet' href='css/style.css'>
                        <link rel='stylesheet' href='css/responsive.css'>
                        <script src='js/vendor/modernizr-3.11.2.min.js'></script>
                        <!-- Link css, js -->
                           
                    </head>
                    <body>
                        <header class='top'>
                            <div class='header-top'>
                                <div class='container'>
                                    <div class='row'>
                                        <div class='col-md-12 col-sm-12'>
                                            <div class='header-top-left'>
                                                <p> 
                                                    ¿TIENES ALGUNA PREGUNTA?
                                                    <a style='color: aliceblue;' href='tel:+528119070498'>8119070498</a> /
                                                    <a style='color: aliceblue;' href='tel:+528119070498'>8119070498</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class='header-area two header-sticky'>
                                <div class='container'>
                                    <div class='row'>
                                        <div class='col-md-3 col-sm-5 col-6'>
                                            <div class='logo'>
                                                <a href='index.html'><img src='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4gHYSUNDX1BST0ZJTEUAAQEAAAHIAAAAAAQwAABtbnRyUkdCIFhZWiAH4AABAAEAAAAAAABhY3NwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlkZXNjAAAA8AAAACRyWFlaAAABFAAAABRnWFlaAAABKAAAABRiWFlaAAABPAAAABR3dHB0AAABUAAAABRyVFJDAAABZAAAAChnVFJDAAABZAAAAChiVFJDAAABZAAAAChjcHJ0AAABjAAAADxtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAAgAAAAcAHMAUgBHAEJYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9YWVogAAAAAAAA9tYAAQAAAADTLXBhcmEAAAAAAAQAAAACZmYAAPKnAAANWQAAE9AAAApbAAAAAAAAAABtbHVjAAAAAAAAAAEAAAAMZW5VUwAAACAAAAAcAEcAbwBvAGcAbABlACAASQBuAGMALgAgADIAMAAxADb/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAAyAF0DASIAAhEBAxEB/8QAHQAAAQQDAQEAAAAAAAAAAAAAAAEHCAkCBQYEA//EADQQAAEDAwMDAwIEBAcAAAAAAAECAwQFBhEABxIIEyEUMVEJIhUyQXEWIyWSQlJTYoGCkf/EABsBAAMBAAMBAAAAAAAAAAAAAAAFBgQBBwgD/8QAMREAAQMDAwEGBQMFAAAAAAAAAQIDEQAEIQUSMVEGExRBYXEyQoGh4RUikVKxwdHw/9oADAMBAAIRAxEAPwCz3Ro0aKKNaa87xtvb61apet31RunUejx1SZclzyEIHgAAeVKJISlI8qUQB763OoO/VTuyp0zbuyrPiurbh1urSZkwJOO4IrSO2g/I5PFWPlIP6aZaRYfqd83aEwFHPsMn7Csl9c+Et1vRMD8CmzvT6gHUXu7dj1udOloSKZEBJjNRKUKnVXWwcdx3KVttZ+EpwPbmrWvgdbfWRshXojG+FrSqjBkKyYlfogpzzyB+bsSGkJHID5Cx8jU0Ojfaq29rdgrVTRobIqFxUyNWqtNSkdyU++2HAFK9yhCVhCU+wAJ9ySXE3Q20tfd+xqpt9d8BuVT6oyptJUnKoz2D232z/hcQohQI+MHwSNUjus6Tbvm0FmksgwVfOfKZ5+k/UUpRYXrrYfL5CyJj5faKbrpj6paN1Mxbil0iyqrQG6DLZjoXMeQ6iWHUKUOKkAAOJCCVIHLAUk5OdPFSK5RLgjuTKDWINSYZfdiuOw5CHkIebVxcbKkEgLSoYKfcH31T9sFvLbG3112Rb24Fp1KqxrLu1+o06dSKoqKtl6Qppp0uscCmSgKZQoAKQrHJBUpJ46tWsSdZblQqVK2r/hRFDpNUqMO4GqagtPMVzm2paChKQgqPJZcJ+7PDGfOMPaPRU6Y+S0khByPMQMcyTnBzGTGea06VqBu2/wB5BUOev8en14mu40aNGpenFGjRo0UUaNLxVjPE4+caXtuf6av7ToomsdRs69NiazvXs2l+04S5lxWlKVVYUVsZXLZKCiQwgfqsp4rSP1LfH3I1JXgvOOCs/sdIQUnBBBH/AAdarG8csLhFy18STP4+vFfG4YRctKaXwar26P8Ar1smzLDp20+9r0ylqt9Ho6bWkRlvtLjJJ4MvoQCttbf5QoJIKQAeJHnuOoT6jG1tGs2oUXZSrP3FclQjrjsT0xHWIdP5pILxU6EqcWkHKUpTjlgqIAwXi3a6Ltgd5ao9cly2c9T6zJVzkVOjSFQ3ZB/zOgAtuK/3KRyP6k6023nQD04beVZmutWjULhmxlhxhVflmW00sHIUGQlLaiP05JVqoXe9nXnvHONrCyZKMbSffmJ9vbypOm31Rtvw6VJ28bszH+/+mo49AW11AvWfQb0h7f2ZU6Lb0Z5FaqVTbcerDNwBwuMra5Ht9rtKZKCE4GFE/wAwZ1PJti1rht+NcX8LLfY7ya8xHfpRYl+rbBUh3sLCViRkeOQCs41r4uzlmUmsUCqWvTZFtNW/MmT26bQ1ehgzX5LYbcXLYbSEvkAAp5ex/wDNdRSKK3RYXoIq57zZeef5y5Lkhzk44pxQ5uEq4gqISnOEpASMAAaTavqg1K4NwkkdATwJOJ/jiOnlNb7K0Nq0GjHv14/NZ06X+IU+LP8ASSYvqWG3uxJb7bzXJIVwcTk8VjOFDJwQRr0ayDaz7Nq/tOgtuAZLah/1OkxInFb6x0aUpUn8yVD9xjSaKKrp3OqE9PXrX4qZ0lLKS7hsPKCR/RSfbOPfTY9PWwG4XUMzW123fUalCg+lS6J8iSe53gvjx7YPt2znPyNOtfNEn1j6hlRgR2lo/EJfpG3ltq7aVOUjgCSB7Aq8403+2G5+8HRpdl0WhKsOLKlzPTtzGJjbpTlrl23WnGyOSFJcUQfIIIPggjUutKO+Knp2blT9or15ZPXg0Vu30RSPGG0tFICtuQCoLMKxgGPQkelevqY2Mv3p42FYjXFerFTerd5Q3GXKfIkJLaWoMsKCivifJWk+Pj9tcxsL10bn7DWZU7Eq9JXcUZUTv27+JOrCqc84ApJJP3OxlBXMIyPOOKglR103U1v5ePUPsLHl1+z41Lcol5Q2mm4KH19xLsGWVFQXk+Cge3zqS+3HS3tHvhsXspcW4dvOuVKgW/AKlsrLKpsdKCfSSfGVs8iDjwoeQCApQPcWiXemWvZ1gXTe9pS1e4IJI6HnHIweleP+3LGuPdrLo36gm6ARuiNuUpHy4+GDjz9aghsxe1/Xf1O2hcd6XBV5lRrVabmyHZLziQ+HG1lKkoyEhGMBISOISAB4Gm+sluHcBkIubd9VqJYbbLK5bVQliQTnISIyVlPHAP3Yznx+upX700pUT6j9Hap9MUzCjyKK0yliOUstNppyQEpCRxSkAYAHgYxqJlkzKPQDIXdW1RupL7bYYRIlzoXpyM8iDHKSrlkD7s4x499XVm8m5R3zadu5tsgJ2yJKsDdjFdfPtqZV3ajMKUJM546ZqUOxnTjV96trb2tbbbfam1Z8Vuiy3ZjsWqxm44aamAt5dQFKUrvJP25ACPuxlOmh2/2I3F3B34q+wlNv5qLVqM/UGXZz8qUYyzDVxWUhP34V+mR++pofTavSlVyjXxbVC2wiWfBp8uFPIamS5K5TzyXEKKlySSAlLCQAnx9x+dNb00xJbf1Gr5fciPpaVOughamlBJy6ceSMaQDU7ph++bONidyZCZmBzGOPKmRs2XG7ZX9RgxMRJ65pqt/ts7zpG/FhbCqvACqigW9b656JL6Ypfc5I7pH5+OVAnI5eNfOgbcXlsf1hWbtlcl3/AIvJh1imvuPw5T5YWl5PMDDhBOAcHI12nXHZNVv/AK06baFOWuK5XotDprcxbLi2mFOkoC1cRnCScnHnxriqJsncWxnWPZu31Zqaa0/ArFMkLnxY7qWVJdRzA+/J8A4OTpha3IdsUBbglTKlFO3JMfFPlExHrWZ5souVEJMBYEz9o/zTy/SqqFQm3XuIJs+VICaXTikPPrcA/nu+3InGrFdUw9NfUPfPTPUq7U7bsWNWV16MxGeTPbkIDaWlqUCnt4JJKznPwNWZdIW+9zdRW3lTvG7LYhUKVBrDlNbjxC7wW2llpYWe75zlwjx48aku2WlXCbtzUIHdHaJkdAOOeadaDeNqYTbE/vz/AHp7NAJ+fbRo1D1Q0uT86TRo0VyKXJ+dGT8nRo0UUZJ9zoyfbOjRoooyfbOjJ9s6NGiijJ+To9/fRo0UV//Z' alt='eduhome' /></a>
                                            </div>
                                        </div>
                                        <div class='col-md-9 col-sm-7 col-6'>
                                            <div class='content-wrapper text-end'>
                                                <!-- Main Menu Start -->
                                                <div class='main-menu'>
                                                    <nav>
                                                        <ul>
                                                            <li><a class='nav-link' href='index.html'>Inicio</a></li>
                                                            <li><a class='nav-link' href='acerca.html'>Acerca de</a></li>
                                                            <li><a class='nav-link' href='servicios.html'>Servicios</a></li>
                                                            <li><a class='nav-link' href='blog-lista.php'>blog</a></li>
                                                            <li><a class='nav-link' href='contacto.html'>Contacto</a></li>
                                                        </ul>
                                                    </nav>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='col-12'>
                                            <div class='mobile-menu hidden-sm'></div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </header>
                        <!-- Header Area End -->

                        <!-- Banner Area Start -->
                        <div class='banner-area-wrapper'>
                            <div class='banner-area text-center'>
                                <div class='container'>
                                    <div class='row'>
                                        <div class='col-12'>
                                            <div class='banner-content-wrapper'>
                                                <div class='banner-content'>
                                                    <h2></h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Banner Area End -->
                        
                        <!-- Course Start -->
                        <div class='course-area pt-150 pb-150'>
                            <div class='container'>   
                                <div class='row'>
                                            <?php
    require_once './config/ConexionBlog.php';
    $getAllBlog = " SELECT * FROM tb_blog WHERE  webOrigen = 'icmetal.mx' AND blogEstado = 0 ORDER BY idBlog DESC";
    $query_run = mysqli_query($conn, $getAllBlog);
    ?>

    <?php
    if ($query_run) {
        $res = mysqli_fetch_all($query_run, MYSQLI_ASSOC);
        foreach ($res as $row) {
    ?>
            <div class="col-lg-4 col-md-6">
                <div class="single-course mb-70">
                    <a href="<?php echo $row['accionBlog']; ?>">
                        <div class="course-img" style="width: 100%;height: 200px;overflow: hidden;">
                            <?php
                            echo '<img src="data:image/jpeg;base64,' . $row['imagenBlog'] . '" alt="Imagen en base64" width="95%">';
                            ?>
                        </div>
                        <div class="course-content">
                            <h3><?php echo $row['tituloBlog'] ?></h3>
                        </div>
                    </a>
                </div>
            </div>
    <?php
        }
    } else {
        echo "Error en la consulta: " . mysqli_error($conn);
    }
    ?>
                                </div>
                            </div> 
                        </div>    

                        <!-- Subscribe Start -->
                        <div class='subscribe-area pt-60 pb-70'>
                            <div class='container'>
                                <div class='row'>
                                    <div class='col-lg-8 offset-lg-2'>
                                        <div class='subscribe-content section-title text-center'>
                                            <h2></h2>
                                            <p></p>
                                        </div>
                                        <div class='newsletter-form mc_embed_signup'>
                                            <form class='formSucriptor' action='registroweb.php' method='post'>
                                                <div id='mc_embed_signup_scroll' class='mc-form'> 
        
                                                    <input type='email' value='' name='correo' class='email'  placeholder='Introduzca su dirección de correo electrónico' required>
                                                    <input type='text' name='nombre' value='Suscriptor' hidden>
                                                    <input type='text' name='telefono' value='Suscriptor' hidden>
                                                    <input type='text' name='mensaje' value='Suscriptor' hidden>
        
                                                    <button id='mc-embedded-subscribe' class='default-btn' type='submit' name='subscribe'><span></span></button> 
                                                </div>    
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Subscribe End -->
        
                        <!-- Footer Start -->
                        <footer class='footer-area'>
                            <div class='main-footer'>
                                <div class='container'>
                                    <div class='row'>
                                        <div class='col-lg-6 col-md-6 pt-4 pt-lg-0'>
                                            <div class='single-widget pr-60'>
                                                <div class='footer-logo pb-25'>
                                                    <a href='index.html'><img src='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4gHYSUNDX1BST0ZJTEUAAQEAAAHIAAAAAAQwAABtbnRyUkdCIFhZWiAH4AABAAEAAAAAAABhY3NwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlkZXNjAAAA8AAAACRyWFlaAAABFAAAABRnWFlaAAABKAAAABRiWFlaAAABPAAAABR3dHB0AAABUAAAABRyVFJDAAABZAAAAChnVFJDAAABZAAAAChiVFJDAAABZAAAAChjcHJ0AAABjAAAADxtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAAgAAAAcAHMAUgBHAEJYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9YWVogAAAAAAAA9tYAAQAAAADTLXBhcmEAAAAAAAQAAAACZmYAAPKnAAANWQAAE9AAAApbAAAAAAAAAABtbHVjAAAAAAAAAAEAAAAMZW5VUwAAACAAAAAcAEcAbwBvAGcAbABlACAASQBuAGMALgAgADIAMAAxADb/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAAyAF0DASIAAhEBAxEB/8QAHQAAAQQDAQEAAAAAAAAAAAAAAAEHCAkCBQYEA//EADQQAAEDAwMDAwIEBAcAAAAAAAECAwQFBhEABxIIEyEUMVEJIhUyQXEWIyWSQlJTYoGCkf/EABsBAAMBAAMBAAAAAAAAAAAAAAAFBgQBBwgD/8QAMREAAQMDAwEGBQMFAAAAAAAAAQIDEQAEIQUSMVEGExRBYXEyQoGh4RUikVKxwdHw/9oADAMBAAIRAxEAPwCz3Ro0aKKNaa87xtvb61apet31RunUejx1SZclzyEIHgAAeVKJISlI8qUQB763OoO/VTuyp0zbuyrPiurbh1urSZkwJOO4IrSO2g/I5PFWPlIP6aZaRYfqd83aEwFHPsMn7Csl9c+Et1vRMD8CmzvT6gHUXu7dj1udOloSKZEBJjNRKUKnVXWwcdx3KVttZ+EpwPbmrWvgdbfWRshXojG+FrSqjBkKyYlfogpzzyB+bsSGkJHID5Cx8jU0Ojfaq29rdgrVTRobIqFxUyNWqtNSkdyU++2HAFK9yhCVhCU+wAJ9ySXE3Q20tfd+xqpt9d8BuVT6oyptJUnKoz2D232z/hcQohQI+MHwSNUjus6Tbvm0FmksgwVfOfKZ5+k/UUpRYXrrYfL5CyJj5faKbrpj6paN1Mxbil0iyqrQG6DLZjoXMeQ6iWHUKUOKkAAOJCCVIHLAUk5OdPFSK5RLgjuTKDWINSYZfdiuOw5CHkIebVxcbKkEgLSoYKfcH31T9sFvLbG3112Rb24Fp1KqxrLu1+o06dSKoqKtl6Qppp0uscCmSgKZQoAKQrHJBUpJ46tWsSdZblQqVK2r/hRFDpNUqMO4GqagtPMVzm2paChKQgqPJZcJ+7PDGfOMPaPRU6Y+S0khByPMQMcyTnBzGTGea06VqBu2/wB5BUOev8en14mu40aNGpenFGjRo0UUaNLxVjPE4+caXtuf6av7ToomsdRs69NiazvXs2l+04S5lxWlKVVYUVsZXLZKCiQwgfqsp4rSP1LfH3I1JXgvOOCs/sdIQUnBBBH/AAdarG8csLhFy18STP4+vFfG4YRctKaXwar26P8Ar1smzLDp20+9r0ylqt9Ho6bWkRlvtLjJJ4MvoQCttbf5QoJIKQAeJHnuOoT6jG1tGs2oUXZSrP3FclQjrjsT0xHWIdP5pILxU6EqcWkHKUpTjlgqIAwXi3a6Ltgd5ao9cly2c9T6zJVzkVOjSFQ3ZB/zOgAtuK/3KRyP6k6023nQD04beVZmutWjULhmxlhxhVflmW00sHIUGQlLaiP05JVqoXe9nXnvHONrCyZKMbSffmJ9vbypOm31Rtvw6VJ28bszH+/+mo49AW11AvWfQb0h7f2ZU6Lb0Z5FaqVTbcerDNwBwuMra5Ht9rtKZKCE4GFE/wAwZ1PJti1rht+NcX8LLfY7ya8xHfpRYl+rbBUh3sLCViRkeOQCs41r4uzlmUmsUCqWvTZFtNW/MmT26bQ1ehgzX5LYbcXLYbSEvkAAp5ex/wDNdRSKK3RYXoIq57zZeef5y5Lkhzk44pxQ5uEq4gqISnOEpASMAAaTavqg1K4NwkkdATwJOJ/jiOnlNb7K0Nq0GjHv14/NZ06X+IU+LP8ASSYvqWG3uxJb7bzXJIVwcTk8VjOFDJwQRr0ayDaz7Nq/tOgtuAZLah/1OkxInFb6x0aUpUn8yVD9xjSaKKrp3OqE9PXrX4qZ0lLKS7hsPKCR/RSfbOPfTY9PWwG4XUMzW123fUalCg+lS6J8iSe53gvjx7YPt2znPyNOtfNEn1j6hlRgR2lo/EJfpG3ltq7aVOUjgCSB7Aq8403+2G5+8HRpdl0WhKsOLKlzPTtzGJjbpTlrl23WnGyOSFJcUQfIIIPggjUutKO+Knp2blT9or15ZPXg0Vu30RSPGG0tFICtuQCoLMKxgGPQkelevqY2Mv3p42FYjXFerFTerd5Q3GXKfIkJLaWoMsKCivifJWk+Pj9tcxsL10bn7DWZU7Eq9JXcUZUTv27+JOrCqc84ApJJP3OxlBXMIyPOOKglR103U1v5ePUPsLHl1+z41Lcol5Q2mm4KH19xLsGWVFQXk+Cge3zqS+3HS3tHvhsXspcW4dvOuVKgW/AKlsrLKpsdKCfSSfGVs8iDjwoeQCApQPcWiXemWvZ1gXTe9pS1e4IJI6HnHIweleP+3LGuPdrLo36gm6ARuiNuUpHy4+GDjz9aghsxe1/Xf1O2hcd6XBV5lRrVabmyHZLziQ+HG1lKkoyEhGMBISOISAB4Gm+sluHcBkIubd9VqJYbbLK5bVQliQTnISIyVlPHAP3Yznx+upX700pUT6j9Hap9MUzCjyKK0yliOUstNppyQEpCRxSkAYAHgYxqJlkzKPQDIXdW1RupL7bYYRIlzoXpyM8iDHKSrlkD7s4x499XVm8m5R3zadu5tsgJ2yJKsDdjFdfPtqZV3ajMKUJM546ZqUOxnTjV96trb2tbbbfam1Z8Vuiy3ZjsWqxm44aamAt5dQFKUrvJP25ACPuxlOmh2/2I3F3B34q+wlNv5qLVqM/UGXZz8qUYyzDVxWUhP34V+mR++pofTavSlVyjXxbVC2wiWfBp8uFPIamS5K5TzyXEKKlySSAlLCQAnx9x+dNb00xJbf1Gr5fciPpaVOughamlBJy6ceSMaQDU7ph++bONidyZCZmBzGOPKmRs2XG7ZX9RgxMRJ65pqt/ts7zpG/FhbCqvACqigW9b656JL6Ypfc5I7pH5+OVAnI5eNfOgbcXlsf1hWbtlcl3/AIvJh1imvuPw5T5YWl5PMDDhBOAcHI12nXHZNVv/AK06baFOWuK5XotDprcxbLi2mFOkoC1cRnCScnHnxriqJsncWxnWPZu31Zqaa0/ArFMkLnxY7qWVJdRzA+/J8A4OTpha3IdsUBbglTKlFO3JMfFPlExHrWZ5souVEJMBYEz9o/zTy/SqqFQm3XuIJs+VICaXTikPPrcA/nu+3InGrFdUw9NfUPfPTPUq7U7bsWNWV16MxGeTPbkIDaWlqUCnt4JJKznPwNWZdIW+9zdRW3lTvG7LYhUKVBrDlNbjxC7wW2llpYWe75zlwjx48aku2WlXCbtzUIHdHaJkdAOOeadaDeNqYTbE/vz/AHp7NAJ+fbRo1D1Q0uT86TRo0VyKXJ+dGT8nRo0UUZJ9zoyfbOjRoooyfbOjJ9s6NGiijJ+To9/fRo0UV//Z' alt='eduhome'></a>
                                                </div>
                                                <p>Somos una empresa dedicada a la distribución de insumos para la industria de maquinados y metalmecánica</p>
                                                <div class='footer-social'>
                                                    <ul>
                                                        <li><a href='Icmetal Monterrey' target='_blank'><i class='zmdi zmdi-facebook'></i></a></li>
                                                        <li><a href='Icmetal Monterrey'><i class='zmdi zmdi-instagram'></i></a></li>
                                                    </ul>    
                                                </div>
                                            </div>
                                        </div>
                                        <div class='col-lg-6 col-md-6 pt-4 pt-lg-0'>
                                            <div class='single-widget'>
                                                <h3>¿TIENES ALGUNA PREGUNTA?</h3>
                                                <p>OREGON 6362, KENNEDY, MONTERREY, NL</p>
                                                <p>OREGON 6362, KENNEDY, MONTERREY, NL</p>
                                                <p>
                                                    <a style='color:gray;' href='tel:+528119070498' target='_blank'>+52 8119070498</a>
                                                    <br>
                                                    <a style='color:gray;' href='tel:+528119070498' target='_blank'>+52 8119070498</a></p>
                                                <!-- <p>icmetalmty@gmail.com</p> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class='footer-bottom text-center'>
                                <div class='container'>
                                    <div class='row'>
                                        <div class='col-12'>
                                            <p>Copyright © Politicas de privacidad. Todos los derechos reservados por Icmetal</p>
                                        </div> 
                                    </div>
                                </div>    
                            </div>
                        </footer>
                        <!-- Footer End -->
                        
                        <script src='js/vendor/jquery-3.6.0.min.js'></script>
                        <script src='js/vendor/jquery-migrate-3.3.2.min.js'></script>
                        <script src='js/bootstrap.bundle.min.js'></script>
                        <script src='js/jquery.meanmenu.js'></script>
                        <script src='js/jquery.magnific-popup.js'></script>
                        <script src='js/ajax-mail.js'></script>
                        <script src='js/owl.carousel.min.js'></script>
                        <script src='js/jquery.mb.YTPlayer.js'></script>
                        <script src='js/jquery.nicescroll.min.js'></script>
                        <script src='js/plugins.js'></script>
                        <script src='js/main.js'></script>
                           
                    </body>
                </html>
            