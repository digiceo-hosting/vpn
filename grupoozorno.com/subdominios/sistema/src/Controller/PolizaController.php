<?php
require '../Models/PolizaModel.php';
require '../Config/DataBase.php';

// Realizar la conexión a la base de datos
$database = new Database();
$db = $database->conectar();

// Inicializar el objeto PolizaModel
$PolizaM = new PolizaModel($db);

// Obtener el método de la solicitud HTTP
$requestMethod = $_SERVER['REQUEST_METHOD'];

// TRY: CONTROLA LOS METODOS [GET-POST-PATCH-DALETE] Y EXCEPTION A ERROR 500
try {
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
        $data = json_decode(file_get_contents("php://input"));
        // Ejecutar el manejo según el método de la solicitud HTTP
        switch ($requestMethod) {
            case 'GET':
                handleGetRequest($action, $data, $PolizaM);
                break;
            case 'POST':
                handlePostRequest($action, $data, $PolizaM);
                break;
            case 'PATCH':
                handlePatchRequest($action, $data, $PolizaM);
                break;
            case 'DELETE':
                handleDeleteRequest($action, $data, $PolizaM);
                break;
            default:
                http_response_code(404);
                echo json_encode([
                    'Message' => 'Solicitud no válida.'
                ], JSON_UNESCAPED_UNICODE);
                break;
        }
    } else {
        http_response_code(404);
        echo json_encode([
            'Message' => 'No hay acción. URL: /PolizaController/'
        ], JSON_UNESCAPED_UNICODE);
    }
} catch (Exception $e) {
    http_response_code(500);
    echo json_encode([
        'Message' => 'Error interno del servidor. Detalles: ' . $e->getMessage() . ' en línea ' . $e->getLine()
    ], JSON_UNESCAPED_UNICODE);
}

// Función para manejar las solicitudes POST
function handlePostRequest($action, $data, $PolizaM)
{
    switch ($action) {
        case 'registrarPoliza':
            $PolizaM->ClienteID = $data->ClienteID;
            $PolizaM->PolizaMaestra = $data->PolizaMaestra;
            $PolizaM->TipoPoliza = $data->TipoPoliza;
            $PolizaM->TipoSeguro = $data->TipoSeguro;
            $PolizaM->Endoso = $data->Endoso;
            $PolizaM->Anterior = $data->Anterior;
            $PolizaM->Posterior = $data->Posterior;
            $PolizaM->FormaPago = $data->FormaPago;
            $PolizaM->Moneda = $data->Moneda;
            $PolizaM->Ejecutivo = $data->Ejecutivo;
            $PolizaM->Vendedor = $data->Vendedor;
            $PolizaM->Renovacion = $data->Renovacion;
            $PolizaM->InicioFechaPoliza = $data->InicioFechaPoliza;
            $PolizaM->HastaFechaPoliza = $data->HastaFechaPoliza;
            $PolizaM->Estatus = $data->Estatus;
            $PolizaM->MotivoEstatus = $data->MotivoEstatus;
            $PolizaM->FechaCobro = $data->FechaCobro;
            $PolizaM->PrimaNeta = $data->PrimaNeta;
            $PolizaM->PrimaTotal = $data->PrimaTotal;

            $respuesta = $PolizaM->registrarPoliza();

            if ($respuesta) {
                http_response_code(200);
                echo json_encode(array('message' => 'Póliza registrada.', 'data' => $respuesta), JSON_UNESCAPED_UNICODE);
            } else {
                http_response_code(404);
                echo json_encode(array('message' => 'Póliza no creada'), JSON_UNESCAPED_UNICODE);
            }
            break;
        case 'buscarPoliza':
            $PolizaM->PolizaID = $data->PolizaID;
            $respuesta = $PolizaM->buscarPoliza();
            if ($respuesta) {
                http_response_code(200);
                echo json_encode(array('message' => 'Póliza encontrada.', 'data' => $respuesta), JSON_UNESCAPED_UNICODE);
            } else {
                http_response_code(404);
                echo json_encode(array('message' => 'Póliza no encontrada.'), JSON_UNESCAPED_UNICODE);
            }
            break;
        default:
            echo json_encode(['Message' => 'Acción POST desconocida.'], JSON_UNESCAPED_UNICODE);
            break;
    }
}

// Función para manejar las solicitudes GET
function handleGetRequest($action, $data, $PolizaM)
{
    switch ($action) {
        case 'listarPolizas':
            $respuesta = $PolizaM->listarPolizas();
            if ($respuesta) {
                http_response_code(200);
                echo json_encode(array('message' => 'Lista pólizas.', 'data' => $respuesta), JSON_UNESCAPED_UNICODE);
            } else {
                http_response_code(404);
                echo json_encode(array('message' => 'Lista no encontrada.'), JSON_UNESCAPED_UNICODE);
            }
            break;
        default:
            http_response_code(404);
            echo json_encode(['Message' => 'Acción GET desconocida.'], JSON_UNESCAPED_UNICODE);
            break;
    }
}

// Función para manejar las solicitudes PATCH
function handlePatchRequest($action, $data, $PolizaM)
{
    switch ($action) {
        case 'modificarPoliza':
            $PolizaM->PolizaID = $data->PolizaID;
            $PolizaM->ClienteID = $data->ClienteID;
            $PolizaM->PolizaMaestra = $data->PolizaMaestra;
            $PolizaM->TipoPoliza = $data->TipoPoliza;
            $PolizaM->TipoSeguro = $data->TipoSeguro;
            $PolizaM->Endoso = $data->Endoso;
            $PolizaM->Anterior = $data->Anterior;
            $PolizaM->Posterior = $data->Posterior;
            $PolizaM->FormaPago = $data->FormaPago;
            $PolizaM->Moneda = $data->Moneda;
            $PolizaM->Ejecutivo = $data->Ejecutivo;
            $PolizaM->Vendedor = $data->Vendedor;
            $PolizaM->Renovacion = $data->Renovacion;
            $PolizaM->FechaAntiguedad = $data->FechaAntiguedad;
            $PolizaM->InicioFechaPoliza = $data->InicioFechaPoliza;
            $PolizaM->HastaFechaPoliza = $data->HastaFechaPoliza;
            $PolizaM->Estatus = $data->Estatus;
            $PolizaM->MotivoEstatus = $data->MotivoEstatus;
            $PolizaM->FechaEstatus = $data->FechaEstatus;
            $PolizaM->FechaCobro = $data->FechaCobro;
            $PolizaM->PrimaNeta = $data->PrimaNeta;
            $PolizaM->PrimaTotal = $data->PrimaTotal;
            
            $respuesta = $PolizaM->modificarPoliza();

            if ($respuesta) {
                http_response_code(200);
                echo json_encode(array('message' => 'Póliza modifca.', 'data' => $respuesta), JSON_UNESCAPED_UNICODE);
            } else {
                http_response_code(404);
                echo json_encode(array('message' => 'Póliza no modifca.'), JSON_UNESCAPED_UNICODE);
            }
            break;
        default:
            echo json_encode(['Message' => 'Acción PATCH desconocida.'], JSON_UNESCAPED_UNICODE);
            break;
    }
}

// Función para manejar las solicitudes DELETE
function handleDeleteRequest($action, $data, $PolizaM)
{
    switch ($action) {
        case 'eliminarPoliza':
            $PolizaM->PolizaID = $data->PolizaID;
            $respuesta = $PolizaM->eliminarPoliza();
            if ($respuesta) {
                http_response_code(200);
                echo json_encode(array('message' => 'Póliza eliminada.', 'data' => $respuesta), JSON_UNESCAPED_UNICODE);
            } else {
                http_response_code(404);
                echo json_encode(array('message' => 'Póliza no eliminada.'), JSON_UNESCAPED_UNICODE);
            }
            break;
        default:
            echo json_encode(['Message' => 'Acción DELETE desconocida.'], JSON_UNESCAPED_UNICODE);
            break;
    }
}
?>
