<?php
    require '../Models/ClienteModel.php';
    require '../Config/DataBase.php';

    // Realizar la conexión a la base de datos
    $database = new Database();
    $db = $database->conectar();

    //Inicializar el obejto ClienteModel
    $ClienteM = new ClienteModel($db);

    //Obtener el método de la solicitud HTTP
    $requestMethod = $_SERVER['REQUEST_METHOD'];

    // TRY: CONTROLA LOS METODOS [GET-POST-PATCH-DALETE] Y EXCEPTION A ERROR 500
    try{
        if (isset($_GET['action'])) {
            $action = $_GET['action'];
            $data = json_decode(file_get_contents("php://input"));
            // Ejecutar el manejo según el método de la solicitud HTTP
            switch ($requestMethod) {
                case 'GET':
                    handleGetRequest($action, $data, $ClienteM);
                    break;
                case 'POST':
                    handlePostRequest($action, $data, $ClienteM);
                    break;
                case 'PATCH':
                    handlePatchRequest($action, $data, $ClienteM);
                    break;
                case 'DELETE':
                    handleDeleteRequest($action, $data, $ClienteM);
                    break;
                default:
                    http_response_code(404);
                    echo json_encode([
                        'Message' => 'Solicitud no válida.'
                    ], JSON_UNESCAPED_UNICODE);
                    break;
            }
        } else {
            http_response_code(404);
            echo json_encode([
                'Message' => 'No hay acción. URL: /ClienteController/'
            ], JSON_UNESCAPED_UNICODE);
        }

    }catch(Exception $e){
        http_response_code(500);
        echo json_encode([
            'Message' => 'Error interno del servidor. Detalles: ' . $e->getMessage() . ' en línea ' . $e->getLine()
        ], JSON_UNESCAPED_UNICODE);
    }

    // Función para manejar las solicitudes POST
    function handlePostRequest($action, $data, $ClienteM)
    {
        switch ($action) {
            case 'registrarCliente':
                $ClienteM->NombreCliente = $data->NombreCliente;
                $ClienteM->ApellidoPartenoCliente = $data->ApellidoPartenoCliente;
                $ClienteM->ApellidoMaternoCliente = $data->ApellidoMaternoCliente;
                $ClienteM->SexoCliente = $data->SexoCliente;
                $ClienteM->TipoContribuyenteCliente = $data->TipoContribuyenteCliente;
                $ClienteM->RazonSocialCliente = $data->RazonSocialCliente;
                $ClienteM->RFCCliente = $data->RFCCliente;
                $ClienteM->CorreoElctronicoCliente = $data->CorreoElctronicoCliente;
                $ClienteM->TelefonoCliente = $data->TelefonoCliente;
                $ClienteM->FechaNacimientoCliente = $data->FechaNacimientoCliente;
                $ClienteM->GrupoOficionaCliente = $data->GrupoOficionaCliente;
                $ClienteM->EjecutivoCobranza = $data->EjecutivoCobranza;
                $ClienteM->EjecutivoCuenta = $data->EjecutivoCuenta;
                $ClienteM->NombreContactoCobro = $data->NombreContactoCobro;
                $ClienteM->Status = $data->Status;
                $ClienteM->DireccionCalleCliente = $data->DireccionCalleCliente;
                $ClienteM->DireccionColoniaCliente = $data->DireccionColoniaCliente;
                $ClienteM->DireccionEstadoCliente = $data->DireccionEstadoCliente;
                $ClienteM->CodigoPostalCliente = $data->CodigoPostalCliente;

                $RespuestaClienteM = $ClienteM->registrarCliente();

                if ($RespuestaClienteM) {
                    http_response_code(200);
                    echo json_encode(array('message' => 'Cliente registrado.', 'data' => $RespuestaClienteM), JSON_UNESCAPED_UNICODE);
                } else {
                    http_response_code(404);
                    echo json_encode(array('message' => 'Cliente no registrado.'), JSON_UNESCAPED_UNICODE);
                }
                break;
            case 'buscarCliente':
                $ClienteM->ClienteID = $data->ClienteID;
                $RespuestaClienteM = $ClienteM->buscarCliente();

                if ($RespuestaClienteM) {
                    http_response_code(200);
                    echo json_encode(array('message' => 'Cliente encontrado exitosamente.', 'data' => $RespuestaClienteM), JSON_UNESCAPED_UNICODE);
                } else {
                    http_response_code(404);
                    echo json_encode(array('message' => 'Cliente no encontrado.'), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                echo json_encode(['Message' => 'Acción POST desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes GET
    function handleGetRequest($action, $data, $ClienteM)
    {
        switch ($action) {
            case 'listarClientes':
                $RespuestaClienteM = $ClienteM->listarClientes();

                if ($RespuestaClienteM) {
                    http_response_code(200);
                    echo json_encode(array('message' => 'Lista de clientes.', 'data' => $RespuestaClienteM), JSON_UNESCAPED_UNICODE);
                } else {
                    http_response_code(404);
                    echo json_encode(array('message' => 'No se encontro lista.'), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                http_response_code(404);
                echo json_encode(['Message' => 'Acción GET desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes PATCH
    function handlePatchRequest($action, $data, $ClienteM)
    {
        switch ($action) {
            case 'modificarCliente':
                $ClienteM->ClienteID = $data->ClienteID;
                $ClienteM->NombreCliente = $data->NombreCliente;
                $ClienteM->ApellidoPartenoCliente = $data->ApellidoPartenoCliente;
                $ClienteM->ApellidoMaternoCliente = $data->ApellidoMaternoCliente;
                $ClienteM->SexoCliente = $data->SexoCliente;
                $ClienteM->TipoContribuyenteCliente = $data->TipoContribuyenteCliente;
                $ClienteM->RazonSocialCliente = $data->RazonSocialCliente;
                $ClienteM->RFCCliente = $data->RFCCliente;
                $ClienteM->CorreoElctronicoCliente = $data->CorreoElctronicoCliente;
                $ClienteM->TelefonoCliente = $data->TelefonoCliente;
                $ClienteM->FechaNacimientoCliente = $data->FechaNacimientoCliente;
                $ClienteM->GrupoOficionaCliente = $data->GrupoOficionaCliente;
                $ClienteM->EjecutivoCobranza = $data->EjecutivoCobranza;
                $ClienteM->EjecutivoCuenta = $data->EjecutivoCuenta;
                $ClienteM->NombreContactoCobro = $data->NombreContactoCobro;
                $ClienteM->Status = $data->Status;
                $ClienteM->DireccionCalleCliente = $data->DireccionCalleCliente;
                $ClienteM->DireccionColoniaCliente = $data->DireccionColoniaCliente;
                $ClienteM->DireccionEstadoCliente = $data->DireccionEstadoCliente;
                $ClienteM->CodigoPostalCliente = $data->CodigoPostalCliente;

                $RespuestaClienteM = $ClienteM->modificarCliente();

                if ($RespuestaClienteM) {
                    http_response_code(200);
                    echo json_encode(array('message' => 'Cliente modificado.', 'data' => $RespuestaClienteM), JSON_UNESCAPED_UNICODE);
                } else {
                    http_response_code(404);
                    echo json_encode(array('message' => 'Cliente no modificado.'), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                echo json_encode(['Message' => 'Acción PATCH desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes DELETE
    function handleDeleteRequest($action, $data, $ClienteM)
    {
        switch ($action) {
            case 'eliminarCliente':
                $ClienteM->ClienteID = $data->ClienteID;
                $RespuestaClienteM = $ClienteM->eliminarCliente();

                if ($RespuestaClienteM) {
                    http_response_code(200);
                    echo json_encode(array('message' => 'Cliente eliminado.', 'data' => $RespuestaClienteM), JSON_UNESCAPED_UNICODE);
                } else {
                    http_response_code(404);
                    echo json_encode(array('message' => 'Cliente no eliminado.'), JSON_UNESCAPED_UNICODE);
                }
                break;
            default:
                echo json_encode(['Message' => 'Acción DELETE desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    