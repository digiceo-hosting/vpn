<?php
//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// require_once './src/View/PlantillaMail.php';

interface EmailSenderInterface
{
    public function sendEmail($emailDestinatarios, $nameDestinatario, $asuntoEmail, $bodyEmail, $smtpHost, $userEmail, $userName, $passwordEmail, $portEmail);
}

class PHPMailerSender implements EmailSenderInterface
{
    public function sendEmail($emailDestinatarios, $nameDestinatario, $asuntoEmail, $bodyEmail, $smtpHost, $userEmail, $userName, $passwordEmail, $portEmail)
    {
        // Create an instance of PHPMailer
        $mail = new PHPMailer(true);

        try {
            // Configure PHPMailer settings...

            // Server settings
            $mail->isSMTP();
            $mail->Host       = $smtpHost;
            $mail->SMTPAuth   = true;
            $mail->Username   = $userEmail;
            $mail->Password   = $passwordEmail;
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
            $mail->Port       = $portEmail;

            // Recipients
            $mail->setFrom($userEmail, $userName);
            // $mail->addAddress($emailDestinatario, $nameDestinatario);
            // Añade todos los destinatarios
            foreach ($emailDestinatarios as $destinatario) {
                $mail->addAddress($destinatario['email'], $destinatario['name']);
            }

            // Attachments
            // $mail->addAttachment($fileData['tmp_name'], $fileData['name']);

            // Content
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
            $mail->isHTML(true);
            $mail->Subject = $asuntoEmail;
            $mail->Body    = $bodyEmail;
            $mail->AltBody = $bodyEmail;

            // Send email
            $mail->send();

            // Return success response
            return ['status' => 200, 'message' => 'Message has been sent'];
        } catch (Exception $e) {
            // Return error response
            return ['status' => 404, 'message' => "Message could not be sent. Mailer Error: {$mail->ErrorInfo}"];
        }
    }
}

class MailController
{
    private $emailSender;

    public function __construct(EmailSenderInterface $emailSender)
    {
        $this->emailSender = $emailSender;
    }

    public function enviarEmail($emailDestinatarios, $nameDestinatario, $asuntoEmail, $bodyEmail, $smtpHost, $userEmail, $userName, $passwordEmail, $portEmail)
    {
        return $this->emailSender->sendEmail($emailDestinatarios, $nameDestinatario, $asuntoEmail, $bodyEmail, $smtpHost, $userEmail, $userName, $passwordEmail, $portEmail);
    }
}
