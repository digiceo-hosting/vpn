<?php
    require '../Models/ReporteModel.php';
    require '../Config/DataBase.php';

    // Realizar la conexión a la base de datos
    $database = new Database();
    $db = $database->conectar();

    //Inicializar el obejto ClienteModel
    $ReporteM = new ReporteModel($db);

    //Obtener el método de la solicitud HTTP
    $requestMethod = $_SERVER['REQUEST_METHOD'];

    // TRY: CONTROLA LOS METODOS [GET-POST-PATCH-DALETE] Y EXCEPTION A ERROR 500
    try{
        if (isset($_GET['action'])) {
            $action = $_GET['action'];
            $data = json_decode(file_get_contents("php://input"));
            // Ejecutar el manejo según el método de la solicitud HTTP
            switch ($requestMethod) {
                case 'GET':
                    handleGetRequest($action, $data, $ReporteM);
                    break;
                case 'POST':
                    handlePostRequest($action, $data, $ReporteM);
                    break;
                case 'PATCH':
                    handlePatchRequest($action, $data, $ReporteM);
                    break;
                case 'DELETE':
                    handleDeleteRequest($action, $data, $ReporteM);
                    break;
                default:
                    http_response_code(404);
                    echo json_encode([
                        'Message' => 'Solicitud no válida.'
                    ], JSON_UNESCAPED_UNICODE);
                    break;
            }
        } else {
            http_response_code(404);
            echo json_encode([
                'Message' => 'No hay acción. URL: /ReporteController/'
            ], JSON_UNESCAPED_UNICODE);
        }

    }catch(Exception $e){
        http_response_code(500);
        echo json_encode([
            'Message' => 'Error interno del servidor. Detalles: ' . $e->getMessage() . ' en línea ' . $e->getLine()
        ], JSON_UNESCAPED_UNICODE);
    }
    // Función para manejar las solicitudes POST
    function handlePostRequest($action, $data, $ReporteM)
    {
        switch ($action) {
            case 'reportePolizas':
                $ReporteM->estatus_param = isset($data->estatus_param) ? $data->estatus_param : null;
                $ReporteM->inicio_fecha_poliza_param = isset($data->inicio_fecha_poliza_param) ? $data->inicio_fecha_poliza_param : null;
                $ReporteM->renovacion_param = isset($data->renovacion_param) ? $data->renovacion_param : null;
                $ReporteM->tipo_contribuyente_param = isset($data->tipo_contribuyente_param) ? $data->tipo_contribuyente_param : null;
                $ReporteM->rfc_cliente_param = isset($data->rfc_cliente_param) ? $data->rfc_cliente_param : null;
                $ReporteM->tipo_seguro_param = isset($data->tipo_seguro_param) ? $data->tipo_seguro_param : null;
                $ReporteM->forma_pago_param = isset($data->forma_pago_param) ? $data->forma_pago_param : null;

                $RespuestaClienteM = $ReporteM->reportePolizas();

                if ($RespuestaClienteM) {
                    http_response_code(200);
                    echo json_encode(array('message' => 'Cliente registrado.', 'data' => $RespuestaClienteM), JSON_UNESCAPED_UNICODE);
                } else {
                    http_response_code(404);
                    echo json_encode(array('message' => 'Cliente no registrado.'), JSON_UNESCAPED_UNICODE);
                }
                break;
            case 'name':
                echo json_encode(['Message' => 'name'], JSON_UNESCAPED_UNICODE);
                break;
            default:
                echo json_encode(['Message' => 'Acción POST desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes GET
    function handleGetRequest($action, $data, $ReporteM)
    {
        switch ($action) {
            case 'name':
                echo json_encode(['Message' => 'name'], JSON_UNESCAPED_UNICODE);
                break;
            default:
                http_response_code(404);
                echo json_encode(['Message' => 'Acción GET desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes PATCH
    function handlePatchRequest($action, $data, $ReporteM)
    {
        switch ($action) {
            case 'name':
                echo json_encode(['Message' => 'name'], JSON_UNESCAPED_UNICODE);
                break;
            default:
                echo json_encode(['Message' => 'Acción PATCH desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    // Función para manejar las solicitudes DELETE
    function handleDeleteRequest($action, $data, $ReporteM)
    {
        switch ($action) {
            case 'name':
                echo json_encode(['Message' => 'name'], JSON_UNESCAPED_UNICODE);
                break;
            default:
                echo json_encode(['Message' => 'Acción DELETE desconocida.'], JSON_UNESCAPED_UNICODE);
                break;
        }
    }
    