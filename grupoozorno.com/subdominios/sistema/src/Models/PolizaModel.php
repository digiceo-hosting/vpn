<?php
    class PolizaModel{
        private $conn;
        private const TABLE_NAME = "Poliza_Seguro";

        // DATOS DE LA POLIZA
        public $PolizaID; // 001        
        public $ClienteID; // 1-Agustin
        public $PolizaMaestra; // 288375
        public $TipoPoliza; // Poliza - Endoso(Son Modificaciones de poliza(Puede aumentar o disminuir el coso))
        public $TipoSeguro; // Gastos Medicos - Auto - Vida - Viaje
        public $Endoso; // Relacion con tabla endoso ---????
        public $Anterior; // ? Fecha de antiguedad. -->Se agrega manual
        public $Posterior; // ? Fecha de vigente. -->Se agrega manual
        public $FormaPago; // Anual - Semestral - Trimestral - Mensual
        public $Moneda;    // Pesos Mexicanos - Dolares
        public $Ejecutivo; // Lesly salas
        public $Vendedor; // Lesly salas
        public $Renovacion;    // ?->(Fecha_Renovacion:02-02-2025)
        public $InicioFechaPoliza; // 02-02-2024
        public $HastaFechaPoliza; // 02-02-2025
        public $Estatus; // Vigente - Cancelada - Renovada
        public $MotivoEstatus; // Descripcion o motivo de la cancelación
        public $FechaCobro; //Fecha de cobro de la poliza para el departamento de cobranza
        public $PrimaNeta; // Sin impuesto
        public $PrimaTotal; // Con impuesto

        public function __construct($db)
        {
            $this->conn = $db;
        }

        public function registrarPoliza() {
            try {
                $query = "INSERT INTO " . self::TABLE_NAME . " (ClienteID, PolizaMaestra, TipoPoliza, TipoSeguro, Endoso, Anterior, Posterior, FormaPago, Moneda, Ejecutivo, Vendedor, Renovacion, InicioFechaPoliza, HastaFechaPoliza, Estatus, MotivoEstatus, FechaCobro, PrimaNeta, PrimaTotal) VALUES (:ClienteID, :PolizaMaestra, :TipoPoliza, :TipoSeguro, :Endoso, :Anterior, :Posterior, :FormaPago, :Moneda, :Ejecutivo, :Vendedor, :Renovacion, :InicioFechaPoliza, :HastaFechaPoliza, :Estatus, :MotivoEstatus, :FechaCobro, :PrimaNeta, :PrimaTotal)";
                $stmt = $this->conn->prepare($query);
                $this->bindParameters($stmt);
                if ($stmt->execute()) {
                    return $this->conn->lastInsertId();
                } else {
                    throw new Exception("Error al registrar la póliza.");
                }
            } catch (Exception $e) {
                throw new Exception("Error al registrar la póliza: " . $e->getMessage());
            }
        }

        public function modificarPoliza() {
            try {
                $query = "UPDATE " . self::TABLE_NAME . " SET ClienteID = :ClienteID,PolizaMaestra = :PolizaMaestra,TipoPoliza = :TipoPoliza,TipoSeguro = :TipoSeguro,Endoso = :Endoso,Anterior = :Anterior,Posterior = :Posterior,FormaPago = :FormaPago,Moneda = :Moneda,Ejecutivo = :Ejecutivo,Vendedor = :Vendedor,Renovacion = :Renovacion,InicioFechaPoliza = :InicioFechaPoliza,HastaFechaPoliza = :HastaFechaPoliza,Estatus = :Estatus,MotivoEstatus = :MotivoEstatus,FechaCobro = :FechaCobro,PrimaNeta = :PrimaNeta,PrimaTotal = :PrimaTotal WHERE PolizaID = :PolizaID";
                $stmt = $this->conn->prepare($query);
                $this->bindParameters($stmt);
                $this-> PolizaID = htmlspecialchars(strip_tags($this->PolizaID));
                $stmt->bindParam(":PolizaID", $this->PolizaID);
                if ($stmt->execute()) {
                    return true;
                } else {
                    throw new Exception("Error al modificar la póliza.");
                }
            } catch (Exception $e) {
                throw new Exception("Error al modificar la póliza: " . $e->getMessage());
            }
        }

        public function eliminarPoliza() {
            try {
                $query = "DELETE FROM " . self::TABLE_NAME . " WHERE PolizaID = :PolizaID";
                $stmt = $this->conn->prepare($query);
                $this-> PolizaID = htmlspecialchars(strip_tags($this->PolizaID));
                $stmt->bindParam(":PolizaID", $this->PolizaID);
                if ($stmt->execute()) {
                    return true;
                } else {
                    throw new Exception("Error al eliminar la póliza.");
                }
            } catch (Exception $e) {
                throw new Exception("Error al eliminar la póliza: " . $e->getMessage());
            }
        }

        public function buscarPoliza() {
            try {
                $query = "SELECT PolizaID,ClienteID,PolizaMaestra,TipoPoliza,TipoSeguro,Endoso,Anterior,Posterior,FormaPago,Moneda,Ejecutivo,Vendedor,Renovacion,InicioFechaPoliza,HastaFechaPoliza,Estatus,MotivoEstatus,FechaCobro,PrimaNeta,PrimaTotal FROM " . self::TABLE_NAME . " WHERE PolizaID = :PolizaID";
                $stmt = $this->conn->prepare($query);
                $this-> PolizaID = htmlspecialchars(strip_tags($this->PolizaID));
                $stmt->bindParam(":PolizaID", $this->PolizaID);
                $stmt->execute();
                return $stmt->fetch(PDO::FETCH_ASSOC);
            } catch (Exception $e) {
                throw new Exception("Error al buscar la póliza: " . $e->getMessage());
            }
        }

        public function listarPolizas(){
            try {
                $query = "SELECT PolizaID,ClienteID,PolizaMaestra,TipoPoliza,TipoSeguro,Endoso,Anterior,Posterior,FormaPago,Moneda,Ejecutivo,Vendedor,Renovacion,InicioFechaPoliza,HastaFechaPoliza,Estatus,MotivoEstatus,FechaCobro,PrimaNeta,PrimaTotal FROM " . self::TABLE_NAME;
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            } catch (Exception $e) {
                throw new Exception("Error al listar las pólizas: " . $e->getMessage());
            }
        }

        private function bindParameters($stmt) {
            // Limpia y filtra los datos antes de insertarlos en la base de datos
            $this->ClienteID = htmlspecialchars(strip_tags($this->ClienteID));
            $this->PolizaMaestra = htmlspecialchars(strip_tags($this->PolizaMaestra));
            $this->TipoPoliza = htmlspecialchars(strip_tags($this->TipoPoliza));
            $this->TipoSeguro = htmlspecialchars(strip_tags($this->TipoSeguro));
            $this->Endoso = htmlspecialchars(strip_tags($this->Endoso));
            $this->Anterior = htmlspecialchars(strip_tags($this->Anterior));
            $this->Posterior = htmlspecialchars(strip_tags($this->Posterior));
            $this->FormaPago = htmlspecialchars(strip_tags($this->FormaPago));
            $this->Moneda = htmlspecialchars(strip_tags($this->Moneda));
            $this->Ejecutivo = htmlspecialchars(strip_tags($this->Ejecutivo));
            $this->Vendedor = htmlspecialchars(strip_tags($this->Vendedor));
            $this->Renovacion = htmlspecialchars(strip_tags($this->Renovacion));
            $this->InicioFechaPoliza = htmlspecialchars(strip_tags($this->InicioFechaPoliza));
            $this->HastaFechaPoliza = htmlspecialchars(strip_tags($this->HastaFechaPoliza));
            $this->Estatus = htmlspecialchars(strip_tags($this->Estatus));
            $this->MotivoEstatus = htmlspecialchars(strip_tags($this->MotivoEstatus));
            $this->FechaCobro = htmlspecialchars(strip_tags($this->FechaCobro));
            $this->PrimaNeta = htmlspecialchars(strip_tags($this->PrimaNeta));
            $this->PrimaTotal = htmlspecialchars(strip_tags($this->PrimaTotal));
        
            $stmt->bindParam(":ClienteID", $this->ClienteID);
            $stmt->bindParam(":PolizaMaestra", $this->PolizaMaestra);
            $stmt->bindParam(":TipoPoliza", $this->TipoPoliza);
            $stmt->bindParam(":TipoSeguro", $this->TipoSeguro);
            $stmt->bindParam(":Endoso", $this->Endoso);
            $stmt->bindParam(":Anterior", $this->Anterior);
            $stmt->bindParam(":Posterior", $this->Posterior);
            $stmt->bindParam(":FormaPago", $this->FormaPago);
            $stmt->bindParam(":Moneda", $this->Moneda);
            $stmt->bindParam(":Ejecutivo", $this->Ejecutivo);
            $stmt->bindParam(":Vendedor", $this->Vendedor);
            $stmt->bindParam(":Renovacion", $this->Renovacion);
            $stmt->bindParam(":InicioFechaPoliza", $this->InicioFechaPoliza);
            $stmt->bindParam(":HastaFechaPoliza", $this->HastaFechaPoliza);
            $stmt->bindParam(":Estatus", $this->Estatus);
            $stmt->bindParam(":MotivoEstatus", $this->MotivoEstatus);
            $stmt->bindParam(":FechaCobro", $this->FechaCobro);
            $stmt->bindParam(":PrimaNeta", $this->PrimaNeta);
            $stmt->bindParam(":PrimaTotal", $this->PrimaTotal);
        }
        
    }
?>
