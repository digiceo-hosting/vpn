<?php
    class ClienteModel{
        private $conn;
        private const TABLE_NAME = "Cliente_Seguros";
        // private $table_bd = "Clientes_Seguros";

        // DATOS DEL CLIENTE 
        public $ClienteID; // 001
        public $NombreCliente; // Agustin
        public $ApellidoPartenoCliente; // Arenas
        public $ApellidoMaternoCliente; // Martinez
        public $SexoCliente; // Hombre
        public $TipoContribuyenteCliente; // Fisica - Moral
        public $RazonSocialCliente; // Agustin Arenas Martinez
        public $RFCCliente; // AEMA9011036J1
        public $CorreoElctronicoCliente; // austintv52@gmail.com
        public $TelefonoCliente; // 2211276404
        public $FechaNacimientoCliente; // 03-11-1990
        public $GrupoOficionaCliente; // Queretaro
        public $EjecutivoCobranza; // Lesly Salas
        public $EjecutivoCuenta; // Lesly Salas
        public $NombreContactoCobro; // Lesly Salas
        public $Status; // Activo - Inactivo
        public $DireccionCalleCliente; // 113 A OTE 1807
        public $DireccionColoniaCliente; // Los Heroes Puebla
        public $DireccionEstadoCliente; // Puebla, Pue.
        public $CodigoPostalCliente; // 72590

        public function __construct($db)
        {
            $this->conn = $db;
        }

        public function registrarCliente() {
            try {
                $query = "INSERT INTO " . self::TABLE_NAME . " (NombreCliente, ApellidoPartenoCliente, ApellidoMaternoCliente, SexoCliente, TipoContribuyenteCliente, RazonSocialCliente, RFCCliente, CorreoElctronicoCliente, TelefonoCliente, FechaNacimientoCliente, GrupoOficionaCliente, EjecutivoCobranza, EjecutivoCuenta, NombreContactoCobro, Status, DireccionCalleCliente, DireccionColoniaCliente, DireccionEstadoCliente, CodigoPostalCliente) VALUES (:NombreCliente, :ApellidoPartenoCliente, :ApellidoMaternoCliente, :SexoCliente, :TipoContribuyenteCliente, :RazonSocialCliente, :RFCCliente, :CorreoElctronicoCliente, :TelefonoCliente, :FechaNacimientoCliente, :GrupoOficionaCliente, :EjecutivoCobranza, :EjecutivoCuenta, :NombreContactoCobro, :Status, :DireccionCalleCliente, :DireccionColoniaCliente, :DireccionEstadoCliente, :CodigoPostalCliente)";
                $stmt = $this->conn->prepare($query);
                $this->bindParameters($stmt);
                if ($stmt->execute()) {
                    return $this->conn->lastInsertId();
                } else {
                    throw new Exception("Error al registrar el cliente.");
                }
            } catch (Exception $e) {
                throw new Exception("Error al registrar el cliente: " . $e->getMessage());
            }
        }

        public function modificarCliente() {
            try {
                $query = "UPDATE " . self::TABLE_NAME . " SET NombreCliente = :NombreCliente, ApellidoPartenoCliente = :ApellidoPartenoCliente, ApellidoMaternoCliente = :ApellidoMaternoCliente, SexoCliente = :SexoCliente, TipoContribuyenteCliente = :TipoContribuyenteCliente, RazonSocialCliente = :RazonSocialCliente, RFCCliente = :RFCCliente, CorreoElctronicoCliente = :CorreoElctronicoCliente, TelefonoCliente = :TelefonoCliente, FechaNacimientoCliente = :FechaNacimientoCliente, GrupoOficionaCliente = :GrupoOficionaCliente, EjecutivoCobranza = :EjecutivoCobranza, EjecutivoCuenta = :EjecutivoCuenta, NombreContactoCobro = :NombreContactoCobro, Status = :Status, DireccionCalleCliente = :DireccionCalleCliente, DireccionColoniaCliente = :DireccionColoniaCliente, DireccionEstadoCliente = :DireccionEstadoCliente, CodigoPostalCliente = :CodigoPostalCliente WHERE ClienteID = :ClienteID";
                $stmt = $this->conn->prepare($query);
                $this->bindParameters($stmt);
                $this-> ClienteID = htmlspecialchars(strip_tags($this->ClienteID));
                $stmt->bindParam(":ClienteID", $this->ClienteID);
                if ($stmt->execute()) {
                    return true;
                } else {
                    throw new Exception("Error al modificar el cliente.");
                }
            } catch (Exception $e) {
                throw new Exception("Error al modificar el cliente: " . $e->getMessage());
            }
        }        

        public function eliminarCliente() {
            try {
                $query = "DELETE FROM " . self::TABLE_NAME . " WHERE ClienteID = :ClienteID";
                $stmt = $this->conn->prepare($query);
                $this-> ClienteID = htmlspecialchars(strip_tags($this->ClienteID));
                $stmt->bindParam(":ClienteID", $this->ClienteID);
                if ($stmt->execute()) {
                    return true;
                } else {
                    throw new Exception("Error al eliminar el cliente.");
                }
            } catch (Exception $e) {
                throw new Exception("Error al eliminar el cliente: " . $e->getMessage());
            }
        }

        public function buscarCliente() {
            try {
                $query = "SELECT ClienteID, NombreCliente, ApellidoPartenoCliente, ApellidoMaternoCliente, SexoCliente, TipoContribuyenteCliente, RazonSocialCliente, RFCCliente, CorreoElctronicoCliente, TelefonoCliente, FechaNacimientoCliente, GrupoOficionaCliente, EjecutivoCobranza, EjecutivoCuenta, NombreContactoCobro, Status, DireccionCalleCliente, DireccionColoniaCliente, DireccionEstadoCliente, CodigoPostalCliente FROM " . self::TABLE_NAME . " WHERE ClienteID = :ClienteID";
                $stmt = $this->conn->prepare($query);
                $this-> ClienteID = htmlspecialchars(strip_tags($this->ClienteID));
                $stmt->bindParam(":ClienteID", $this->ClienteID);
                $stmt->execute();
                return $stmt->fetch(PDO::FETCH_ASSOC);
            } catch (Exception $e) {
                throw new Exception("Error al buscar el cliente: " . $e->getMessage());
            }
        }

        public function listarClientes(){
            try {
                $query = "SELECT ClienteID, NombreCliente, ApellidoPartenoCliente, ApellidoMaternoCliente, SexoCliente, TipoContribuyenteCliente, RazonSocialCliente, RFCCliente, CorreoElctronicoCliente, TelefonoCliente, FechaNacimientoCliente, GrupoOficionaCliente, EjecutivoCobranza, EjecutivoCuenta, NombreContactoCobro, Status, DireccionCalleCliente, DireccionColoniaCliente, DireccionEstadoCliente, CodigoPostalCliente FROM " . self::TABLE_NAME;
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            } catch (Exception $e) {
                throw new Exception("Error al listar los clientes: " . $e->getMessage());
            }
        }

        private function bindParameters($stmt) {
            // Limpia y filtra los datos antes de insertarlos en la base de datos
            $this-> NombreCliente = htmlspecialchars(strip_tags($this->NombreCliente));
            $this-> ApellidoPartenoCliente = htmlspecialchars(strip_tags($this->ApellidoPartenoCliente));
            $this-> ApellidoMaternoCliente = htmlspecialchars(strip_tags($this->ApellidoMaternoCliente));
            $this-> SexoCliente = htmlspecialchars(strip_tags($this->SexoCliente));
            $this-> TipoContribuyenteCliente = htmlspecialchars(strip_tags($this->TipoContribuyenteCliente));
            $this-> RazonSocialCliente = htmlspecialchars(strip_tags($this->RazonSocialCliente));
            $this-> RFCCliente = htmlspecialchars(strip_tags($this->RFCCliente));
            $this-> CorreoElctronicoCliente = htmlspecialchars(strip_tags($this->CorreoElctronicoCliente));
            $this-> TelefonoCliente = htmlspecialchars(strip_tags($this->TelefonoCliente));
            $this-> FechaNacimientoCliente = htmlspecialchars(strip_tags($this->FechaNacimientoCliente));
            $this-> GrupoOficionaCliente = htmlspecialchars(strip_tags($this->GrupoOficionaCliente));
            $this-> EjecutivoCobranza = htmlspecialchars(strip_tags($this->EjecutivoCobranza));
            $this-> EjecutivoCuenta = htmlspecialchars(strip_tags($this->EjecutivoCuenta));
            $this-> NombreContactoCobro = htmlspecialchars(strip_tags($this->NombreContactoCobro));
            $this-> Status = htmlspecialchars(strip_tags($this->Status));
            $this-> DireccionCalleCliente = htmlspecialchars(strip_tags($this->DireccionCalleCliente));
            $this-> DireccionColoniaCliente = htmlspecialchars(strip_tags($this->DireccionColoniaCliente));
            $this-> DireccionEstadoCliente = htmlspecialchars(strip_tags($this->DireccionEstadoCliente));
            $this-> CodigoPostalCliente = htmlspecialchars(strip_tags($this->CodigoPostalCliente));

            $stmt->bindParam(":NombreCliente", $this->NombreCliente);
            $stmt->bindParam(":ApellidoPartenoCliente", $this->ApellidoPartenoCliente);
            $stmt->bindParam(":ApellidoMaternoCliente", $this->ApellidoMaternoCliente);
            $stmt->bindParam(":SexoCliente", $this->SexoCliente);
            $stmt->bindParam(":TipoContribuyenteCliente", $this->TipoContribuyenteCliente);
            $stmt->bindParam(":RazonSocialCliente", $this->RazonSocialCliente);
            $stmt->bindParam(":RFCCliente", $this->RFCCliente);
            $stmt->bindParam(":CorreoElctronicoCliente", $this->CorreoElctronicoCliente);
            $stmt->bindParam(":TelefonoCliente", $this->TelefonoCliente);
            $stmt->bindParam(":FechaNacimientoCliente", $this->FechaNacimientoCliente);
            $stmt->bindParam(":GrupoOficionaCliente", $this->GrupoOficionaCliente);
            $stmt->bindParam(":EjecutivoCobranza", $this->EjecutivoCobranza);
            $stmt->bindParam(":EjecutivoCuenta", $this->EjecutivoCuenta);
            $stmt->bindParam(":NombreContactoCobro", $this->NombreContactoCobro);
            $stmt->bindParam(":Status", $this->Status);
            $stmt->bindParam(":DireccionCalleCliente", $this->DireccionCalleCliente);
            $stmt->bindParam(":DireccionColoniaCliente", $this->DireccionColoniaCliente);
            $stmt->bindParam(":DireccionEstadoCliente", $this->DireccionEstadoCliente);
            $stmt->bindParam(":CodigoPostalCliente", $this->CodigoPostalCliente);
        }
    }
?>
