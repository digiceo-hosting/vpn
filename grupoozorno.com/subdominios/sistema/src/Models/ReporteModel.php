<?php
    class ReporteModel{
        private $conn;

        public $estatus_param='';
        public $inicio_fecha_poliza_param='';
        public $renovacion_param='';
        public $tipo_contribuyente_param='';
        public $rfc_cliente_param='';
        public $tipo_seguro_param='';
        public $forma_pago_param='';

        public function __construct($db)
        {
            $this->conn = $db;
        }

        public function reportePolizas() {
            $query = "CALL ConsultarPolizas(:estatus_param,:inicio_fecha_poliza_param, :renovacion_param,:tipo_contribuyente_param, :rfc_cliente_param,:tipo_seguro_param,:forma_pago_param)";
            $stmt = $this->conn->prepare($query);
            // Asigna los parámetros
            $this->bindParameters($stmt);
            // Ejecuta la consulta
            $stmt->execute();
            // return $stmt;
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        private function bindParameters($stmt) {
            // Limpia y filtra los datos antes de insertarlos en la base de datos
            $this-> estatus_param = htmlspecialchars(strip_tags($this->estatus_param));
            $this-> inicio_fecha_poliza_param = htmlspecialchars(strip_tags($this->inicio_fecha_poliza_param));
            $this-> renovacion_param = htmlspecialchars(strip_tags($this->renovacion_param));
            $this-> tipo_contribuyente_param = htmlspecialchars(strip_tags($this->tipo_contribuyente_param));
            $this-> rfc_cliente_param = htmlspecialchars(strip_tags($this->rfc_cliente_param));
            $this-> tipo_seguro_param = htmlspecialchars(strip_tags($this->tipo_seguro_param));
            $this-> forma_pago_param = htmlspecialchars(strip_tags($this->forma_pago_param));

            $stmt->bindParam(":estatus_param", $this->estatus_param);
            $stmt->bindParam(":inicio_fecha_poliza_param", $this->inicio_fecha_poliza_param);
            $stmt->bindParam(":renovacion_param", $this->renovacion_param);
            $stmt->bindParam(":tipo_contribuyente_param", $this->tipo_contribuyente_param);
            $stmt->bindParam(":rfc_cliente_param", $this->rfc_cliente_param);
            $stmt->bindParam(":tipo_seguro_param", $this->tipo_seguro_param);
            $stmt->bindParam(":forma_pago_param", $this->forma_pago_param);
            
        }
    }
?>