document.addEventListener("DOMContentLoaded", function () {
    reportePolizas(data={});
    const formReportePolizas = document.getElementById('formReportePolizas');
    if (formReportePolizas) {
        formReportePolizas.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnReportePolizas") {
                e.preventDefault();
                const data = obtenerDatosFormularioCrearModificarCliente();
                reportePolizas(data);
            }
        });
    }
});

function reportePolizas(data) {
    fetch('./../Controller/ReporteController.php?action=reportePolizas', {
        method: 'POST',
        body: Object.keys(data).length ? JSON.stringify(data) : undefined,
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result && result.data && result.data.length > 0) {
            // alert(result.message);
            // Actualizar el formulario con los datos del socio modificado
            actualizarTabla(result.data);
        } else if (result && result.data && result.data.length === 0) {
            alert('No se encontraron datos para mostrar.');
        } else {
            alert('Error al obtener datos del servidor.');
        }
    })
    .catch(error => console.error('Error al llamar a la API.', error));
}
function actualizarTabla(data) {
    const tbListaCliente = document.getElementById('tbListaReporte').getElementsByTagName('tbody')[0];
    tbListaCliente.innerHTML = '';

    data.forEach(reporte => {
        const newRow = tbListaCliente.insertRow();
        newRow.innerHTML = `
            <td>${reporte.ClienteID}</td>
            <td>${reporte.NombreCliente}</td>
            <td>${reporte.ApellidoPartenoCliente}</td>
            <td>${reporte.ApellidoMaternoCliente}</td>
            <td>${reporte.SexoCliente}</td>
            <td>${reporte.TipoContribuyenteCliente}</td>
            <td>${reporte.RazonSocialCliente}</td>
            <td>${reporte.RFCCliente}</td>
            <td>${reporte.CorreoElctronicoCliente}</td>
            <td>${reporte.TelefonoCliente}</td>
            <td>${reporte.FechaNacimientoCliente}</td>
            <td>${reporte.GrupoOficionaCliente}</td>
            <td>${reporte.EjecutivoCobranza}</td>
            <td>${reporte.EjecutivoCuenta}</td>
            <td>${reporte.NombreContactoCobro}</td>
            <td>${reporte.Status}</td>
            <td>${reporte.DireccionCalleCliente}</td>
            <td>${reporte.DireccionColoniaCliente}</td>
            <td>${reporte.DireccionEstadoCliente}</td>
            <td>${reporte.CodigoPostalCliente}</td>
            <td>${reporte.PolizaID}</td>
            <td>${reporte.PolizaMaestra}</td>
            <td>${reporte.TipoPoliza}</td>
            <td>${reporte.TipoSeguro}</td>
            <td>${reporte.Endoso}</td>
            <td>${reporte.Anterior}</td>
            <td>${reporte.Posterior}</td>
            <td>${reporte.FormaPago}</td>
            <td>${reporte.Moneda}</td>
            <td>${reporte.Ejecutivo}</td>
            <td>${reporte.Vendedor}</td>
            <td>${reporte.Renovacion}</td>
            <td>${reporte.InicioFechaPoliza}</td>
            <td>${reporte.HastaFechaPoliza}</td>
            <td>${reporte.Estatus}</td>
            <td>${reporte.MotivoEstatus}</td>
            <td>${reporte.FechaCobro}</td>
            <td>${reporte.PrimaNeta}</td>
            <td>${reporte.PrimaTotal}</td>
        

            <th>
            <button type="submit" class="btn btn-info"><a href="Polizas/CrearPoliza.html?id=${reporte.ClienteID}" style="color: aliceblue;">Poliza</a></button>
            <button type="submit" class="btn btn-success"><a href="EstadoPolizas.html?id=${reporte.ClienteID}" style="color: aliceblue;">Endoso</a></button>
            <button type="submit" class="btn btn-warning"><a href="NotificacionesAutomatizadas.html?id=${reporte.ClienteID}" style="color: aliceblue;">Configuración</a></button>
            </th>
        `;
    });
}

function obtenerDatosFormularioCrearModificarCliente() {
    return {
        estatus_param: document.getElementById("inputestatus_param").value,
        inicio_fecha_poliza_param: document.getElementById("inputinicio_fecha_poliza_param").value,
        renovacion_param: document.getElementById("inputrenovacion_param").value,
        tipo_contribuyente_param: document.getElementById("inputtipo_contribuyente_param").value,
        rfc_cliente_param: document.getElementById("inputrfc_cliente_param").value,
        tipo_seguro_param: document.getElementById("inputtipo_seguro_param").value,
        forma_pago_param: document.getElementById("inputforma_pago_param").value
    };
}