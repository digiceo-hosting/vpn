document.addEventListener("DOMContentLoaded", function() {
    // Verificar si hay un formulario de inicio de sesión en la página actual
    const formInicioSesion = document.getElementById("formInicioSesion");
    if (formInicioSesion) {
        formInicioSesion.addEventListener("submit", function(e) {
            e.preventDefault();
            const email = document.getElementById("inputEmail");
            const clave = document.getElementById("inputclave");
            inicioSesion(email.value, clave.value);
        });
    }

    // Verificar si hay un formulario de creación de cuenta en la página actual
    const formCrearCuenta = document.getElementById("formCrearCuenta");
    if (formCrearCuenta) {
        formCrearCuenta.addEventListener("submit", function(e) {
            e.preventDefault();
            
            const NickUser = document.getElementById("inputNickUser");
            const NombreUser = document.getElementById("inputNombreUser");
            const ApellidoUser = document.getElementById("inputApellidoUser");
            const CorreoUser = document.getElementById("inputCorreoUser");
            const ClaveUser = document.getElementById("inputClaveUser");
            const RolUser = document.getElementById("inputRolUser");
            const OficinaUser = document.getElementById("inputOficinaUser");

            crearCuenta(NickUser.value, NombreUser.value, ApellidoUser.value, CorreoUser.value, ClaveUser.value, RolUser.value, OficinaUser.value);
        });
    }

    // Verificar si hay un formulario de recuperación de cuenta en la página actual
    const formRecuperarCuenta = document.getElementById("formrecuperarCuenta");
    if (formRecuperarCuenta) {
        formRecuperarCuenta.addEventListener("submit", function(e) {
            e.preventDefault();
            
            const CorreoUser = document.getElementById("inputCorreoUserR");

            recuperarCuenta(CorreoUser.value);
        });
    }

    // Verificar si hay un formulario de recuperación de cuenta en la página actual
    const formModificarCuenta = document.getElementById("formModificarCuenta");
    if (formModificarCuenta) {
        formModificarCuenta.addEventListener("submit", function(e) {
            e.preventDefault();

            const InicioSesionID = document.getElementById("inputUInicioSesionID");
            const NickUser = document.getElementById("inputUNickUser");
            const NombreUser = document.getElementById("inputUNombreUser");
            const ApellidoUser = document.getElementById("inputUApellidoUser");
            const CorreoUser = document.getElementById("inputUCorreoUser");
            const ClaveUser = document.getElementById("inputUClaveUser");
            const RolUser = document.getElementById("inputURolUser");
            const OficinaUser = document.getElementById("inputUOficinaUser");

            modificarCuenta(InicioSesionID.value, NickUser.value, NombreUser.value, ApellidoUser.value, CorreoUser.value, ClaveUser.value, RolUser.value, OficinaUser.value);
        });
    }
    // Verificar si hay un formulario de recuperación de cuenta en la página actual
    const formEliminarCuenta = document.getElementById("formEliminarCuenta");
    if (formEliminarCuenta) {
        formEliminarCuenta.addEventListener("submit", function(e) {
            e.preventDefault();

            const InicioSesionID = document.getElementById("inputEInicioSesionID");

            eliminarCuenta(InicioSesionID.value);
        });
    }

    //Verificar si hay información de usuario en el localStorage
    const usuario = localStorage.getItem('usuario');
    if (usuario) {
        try {
            // Parsear el JSON almacenado en localStorage
            const usuarioObj = JSON.parse(usuario);

            // Verificar si usuarioObj es un objeto válido
            if (usuarioObj && typeof usuarioObj === 'object') {
                // Asignar los valores a los campos del formularioinputEInicioSesionID
                document.getElementById("inputEInicioSesionID").value = usuarioObj.InicioSesionID || ' ';
                document.getElementById("inputUInicioSesionID").value = usuarioObj.InicioSesionID || ' ';
                document.getElementById("inputUNickUser").value = usuarioObj.NickUser || ' ';
                document.getElementById("inputUNombreUser").value = usuarioObj.NombreUser || ' ';
                document.getElementById("inputUApellidoUser").value = usuarioObj.ApellidoUser || ' ';
                document.getElementById("inputUCorreoUser").value = usuarioObj.CorreoUser || ' ';
                document.getElementById("inputUClaveUser").value = usuarioObj.ClaveUser || ' ';
                document.getElementById("inputURolUser").value = usuarioObj.RolUser || ' ';
                document.getElementById("inputUOficinaUser").value = usuarioObj.OficinaUser || ' ';
            } else {
                console.error('El objeto almacenado en localStorage no es válido.');
            }
        } catch (error) {
            console.error('Error al parsear JSON:', error);
        }
    }


});


function inicioSesion(email, clave){
    const data = { CorreoUser: email, ClaveUser: clave};

    fetch('./src/Controller/InicioSesionController.php?action=inicioSesion', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        console.log(result.message);
        if (result.message === 'Inicio de sesion exitosamente.') {

            // Guardar el objeto de usuario en localStorage
            localStorage.setItem('usuario', JSON.stringify(result.data));
            // Redireccionar a la página deseada en caso de éxito
            window.location.href = './src/View/InformesDetallados.html';
            
        } else {
            console.error(result.message); // Mostrar un mensaje de error en caso de fallo
        }
    })
    .catch(error => console.error('Error al iniciar sesión', error));
}
function crearCuenta(NickUser, NombreUser, ApellidoUser, CorreoUser, ClaveUser, RolUser, OficinaUser){
    const data = { NickUser:NickUser, NombreUser:NombreUser, ApellidoUser:ApellidoUser, CorreoUser:CorreoUser, ClaveUser:ClaveUser, RolUser:RolUser, OficinaUser:OficinaUser};

    fetch('./../../Controller/InicioSesionController.php?action=crearCuenta', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        console.log(result.status);
        //validar http_response_code(200);
        if (result.message === 'Inicio de sesion exitosamente.') {
            alert('Se registró exitosamente.');
        } else if (result.message === 'Correo ya existe en el sistema.') {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error al crear cuenta', error));
}
function recuperarCuenta(CorreoUser){
    const data = { CorreoUser: CorreoUser };
    fetch('./../../Controller/InicioSesionController.php?action=recuperarCuenta', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        console.log(result.message);
        if (result.message === 'Recuperacion de cuenta exitosamente.') {
            // Redireccionar a la página deseada en caso de éxito
            // window.location.href = '#';
            alert('Se mando un correo a la cuenta de correo.');
        } else {
            console.error(result.message); // Mostrar un mensaje de error en caso de fallo
        }
    })
    .catch(error => console.error('Error al iniciar sesión', error));

}
function modificarCuenta(InicioSesionID, NickUser, NombreUser, ApellidoUser, CorreoUser, ClaveUser, RolUser, OficinaUser){
    const data = { InicioSesionID:InicioSesionID, NickUser:NickUser, NombreUser:NombreUser, ApellidoUser:ApellidoUser, CorreoUser:CorreoUser, ClaveUser:ClaveUser, RolUser:RolUser, OficinaUser:OficinaUser};
    fetch('./../../Controller/InicioSesionController.php?action=modificarCuenta', {
        method: 'PATCH',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        console.log(result.status);
        //validar http_response_code(200);
        if (result.message === 'Se modifico exitosamente la cuenta.') {
            alert('Se modifico exitosamente la cuenta.');
        } else if (result.message === 'Error al modificar la cuenta. Usuario no encontrado.') {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error al crear cuenta', error));
}
function eliminarCuenta(InicioSesionID){
    const data = { InicioSesionID:InicioSesionID};

    fetch('./../../Controller/InicioSesionController.php?action=eliminarCuenta', {
        method: 'DELETE',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        console.log(result.status);
        //validar http_response_code(200);
        if (result.message === 'Elimino la cuenta.') {
            alert('Se Elimino la cuenta.');
        } else if (result.message === 'Error al eliminar la cuenta.') {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error al crear cuenta', error));
}