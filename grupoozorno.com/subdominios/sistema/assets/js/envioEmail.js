/* Boton para enviar email, se llama desde la vista index con el id="btnEnviarEmail" */
$('#btnEnviarEmail').click(function() {
    const formData = new FormData($('#miFormulario')[0]);

    // Convertir la cadena de correos electrónicos en un arreglo
    var emailsString = formData.get("emails");
    var emailsArray = emailsString.split(",");

    // Crear un nuevo arreglo para almacenar los datos de los destinatarios
    var destinatarios = [];
    for (var i = 0; i < emailsArray.length; i++) {
        var emailParts = emailsArray[i].split(":");
        var email = emailParts[0].trim();
        var name = emailParts[1] ? emailParts[1].trim() : "";
        destinatarios.push({ email: email, name: name });
    }
    
    // Agregar el arreglo de destinatarios al formData
    formData.append("destinatarios", JSON.stringify(destinatarios));

    $.ajax({
        url: "./../Functions/procesar.php",
        method: "POST",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",  // Indica que esperas una respuesta en formato JSON
        success: function (respuesta) {
            console.log("respuesta", respuesta);
            if (respuesta.status === 200) {
                alert("Email enviado");
            } else {
                alert("Ocurrió un error: " + respuesta.message);
            }
        },
        error: function(xhr, status, error) {
            console.error("Error en la solicitud:", error);
            // Aquí puedes manejar el error de la solicitud AJAX si es necesario
        }
    });
});