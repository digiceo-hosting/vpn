document.addEventListener("DOMContentLoaded", function () {
    const formCrearCliente = document.getElementById("formCrearCliente");
    if (formCrearCliente) {
        formCrearCliente.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnCrearCliente") {
                e.preventDefault();
                const data = obtenerDatosFormularioCrearModificarCliente();
                CrearCliente(data);
            }
        });
    }
    const formModificarCliente = document.getElementById("formModificarCliente");
    if (formModificarCliente) {
        formModificarCliente.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnModificarCliente") {
                e.preventDefault();
                const data = obtenerDatosFormularioCrearModificarCliente();
                ModificarCliente(data);
            }
        });
    }
    const formBuscarCliente = document.getElementById('formBuscarCliente');
    if (formBuscarCliente) {
        formBuscarCliente.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnBuscarCliente") {
                e.preventDefault();
                const data = {
                    ClienteID: document.getElementById("inputCliente_ID").value
                };
                BuscarCliente(data);
            }
        });
    }
    const formEliminarCliente = document.getElementById('formEliminarCliente');
    if (formEliminarCliente) {
        formEliminarCliente.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnEliminarCliente") {
                e.preventDefault();
                const data = {
                    ClienteID: document.getElementById("inputClienteID").value
                };
                EliminarCliente(data);
            }
        });
    }
    ListadoClientes();
});

function CrearCliente(data) {
    fetch('./../../Controller/ClienteController.php?action=registrarCliente', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Cliente registrado.') {
            alert('Cliente registrado exitosamente.');
        } else {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error al llamar a la API.', error));
}

function ModificarCliente(data) {
    fetch('./../../Controller/ClienteController.php?action=modificarCliente', {
        method: 'PATCH',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Cliente modificado.') {
            alert('Cliente modificado exitosamente.');
        } else {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error al llamar a la API.', error));
}

function BuscarCliente(data) {
    fetch('./../../Controller/ClienteController.php?action=buscarCliente', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result) {
            alert(result.message);
            // Actualizar el formulario con los datos del socio modificado
            actualizarFormulario(result.data);
        } else {
            alert(result.message);
            document.getElementById("inputClienteID").value = "";
        }
    })
    .catch(error => console.error('Error al llamar a la API.', error));
}

function EliminarCliente(data) {
    fetch('./../../Controller/ClienteController.php?action=eliminarCliente', {
        method: 'DELETE',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Cliente eliminado.') {
            alert('Cliente eliminado exitosamente.');
        } else if(result.message === undefined){
            alert('No se puede eliminar cliente ya tiene polizas registradas.');
        }else {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error al llamar a la API.', error));
}

function ListadoClientes() {
    fetch('./../../Controller/ClienteController.php?action=listarClientes', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Lista de clientes.') {
            // alert('Lista de clientes obtenida exitosamente.');
            actualizarTabla(result.data);
        } else {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error al llamar a la API.', error));
}

function actualizarTabla(data) {
    const tbListaCliente = document.getElementById('tbListaClientes').getElementsByTagName('tbody')[0];
    tbListaCliente.innerHTML = '';

    data.forEach(cliente => {
        const newRow = tbListaCliente.insertRow();
        newRow.innerHTML = `
            <td>${cliente.ClienteID}</td>
            <td>${cliente.NombreCliente}</td>
            <td>${cliente.ApellidoPartenoCliente}</td>
            <td>${cliente.ApellidoMaternoCliente}</td>
            <td>${cliente.SexoCliente}</td>
            <td>${cliente.TipoContribuyenteCliente}</td>
            <td>${cliente.RazonSocialCliente}</td>
            <td>${cliente.RFCCliente}</td>
            <td>${cliente.CorreoElctronicoCliente}</td>
            <td>${cliente.TelefonoCliente}</td>
            <td>${cliente.FechaNacimientoCliente}</td>
            <td>${cliente.GrupoOficionaCliente}</td>
            <td>${cliente.EjecutivoCobranza}</td>
            <td>${cliente.EjecutivoCuenta}</td>
            <td>${cliente.NombreContactoCobro}</td>
            <td>${cliente.Status}</td>
            <td>${cliente.DireccionCalleCliente}</td>
            <td>${cliente.DireccionColoniaCliente}</td>
            <td>${cliente.DireccionEstadoCliente}</td>
            <td>${cliente.CodigoPostalCliente}</td>
            <th>
            <button type="submit" class="btn btn-info"><a href="../Polizas/CrearPoliza.html?id=${cliente.ClienteID}" style="color: aliceblue;">Poliza</a></button>
            <button type="submit" class="btn btn-success"><a href="./../EstadoPolizas.html?id=${cliente.ClienteID}" style="color: aliceblue;">Endoso</a></button>
            <button type="submit" class="btn btn-warning"><a href="./../NotificacionesAutomatizadas.html?id=${cliente.ClienteID}" style="color: aliceblue;">Configuración</a></button>
            </th>
        `;
    });
}

function obtenerDatosFormularioCrearModificarCliente() {
    return {
        ClienteID: document.getElementById("inputClienteID").value,
        NombreCliente: document.getElementById("inputNombreCliente").value,
        ApellidoPartenoCliente: document.getElementById("inputApellidoPaternoCliente").value,
        ApellidoMaternoCliente: document.getElementById("inputApellidoMaternoCliente").value,
        SexoCliente: document.getElementById("inputSexoCliente").value,
        TipoContribuyenteCliente: document.getElementById("inputTipoContribuyenteCliente").value,
        RazonSocialCliente: document.getElementById("inputRazonSocialCliente").value,
        RFCCliente: document.getElementById("inputRFCCliente").value,
        CorreoElctronicoCliente: document.getElementById("inputCorreoElectronicoCliente").value,
        TelefonoCliente: document.getElementById("inputTelefonoCliente").value,
        FechaNacimientoCliente: document.getElementById("inputFechaNacimientoCliente").value,
        GrupoOficionaCliente: document.getElementById("inputGrupoOficionaCliente").value,
        EjecutivoCobranza: document.getElementById("inputEjecutivoCobranza").value,
        EjecutivoCuenta: document.getElementById("inputEjecutivoCuenta").value,
        NombreContactoCobro: document.getElementById("inputNombreContactoCobro").value,
        Status: document.getElementById("inputStatus").value,
        DireccionCalleCliente: document.getElementById("inputDireccionCalleCliente").value,
        DireccionColoniaCliente: document.getElementById("inputDireccionColoniaCliente").value,
        DireccionEstadoCliente: document.getElementById("inputDireccionEstadoCliente").value,
        CodigoPostalCliente: document.getElementById("inputCodigoPostalCliente").value
    };
}