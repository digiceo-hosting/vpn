document.addEventListener("DOMContentLoaded", function () {
    const formCrearPoliza = document.getElementById("formCrearPoliza");
    if (formCrearPoliza) {
        formCrearPoliza.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnCrearPoliza") {
                e.preventDefault();
                const data = obtenerDatosFormularioPoliza();
                CrearPoliza(data);
            }
        });
    }

    const formModificarPoliza = document.getElementById("formModificarPoliza");
    if (formModificarPoliza) {
        formModificarPoliza.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnModificarPoliza") {
                e.preventDefault();
                const data = obtenerDatosFormularioPoliza();
                ModificarPoliza(data);
            }
        });
    }

    const formBuscarPoliza = document.getElementById('formBuscarPoliza');
    if (formBuscarPoliza) {
        formBuscarPoliza.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnBuscarPoliza") {
                e.preventDefault();
                const data = {
                    PolizaID: document.getElementById("inputPoliza_ID").value
                };
                BuscarPoliza(data);
            }
        });
    }

    const formEliminarPoliza = document.getElementById('formEliminarPoliza');
    if (formEliminarPoliza) {
        formEliminarPoliza.addEventListener("submit", function (e) {
            e.preventDefault();
            if (e.submitter && e.submitter.id === "btnEliminarPoliza") {
                e.preventDefault();
                const data = {
                    PolizaID: document.getElementById("inputPolizaID").value
                };
                EliminarPoliza(data);
            }
        });
    }

    ListadoPolizas();
});

function CrearPoliza(data) {
    fetch('./../../Controller/PolizaController.php?action=registrarPoliza', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Poliza registrada.') {
            alert('Poliza registrada exitosamente.');
        } else {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error al llamar a la API.', error));
}

function ModificarPoliza(data) {
    fetch('./../../Controller/PolizaController.php?action=modificarPoliza', {
        method: 'PATCH',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Poliza modificada.') {
            alert('Poliza modificada exitosamente.');
        } else {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error al llamar a la API.', error));
}

function BuscarPoliza(data) {
    fetch('./../../Controller/PolizaController.php?action=buscarPoliza', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result) {
            alert('Poliza encontrada exitosamente.');
            actualizarFormulario(result.data);
        } else {
            alert(result.message);
            document.getElementById("inputClienteID").value = "";
        }
    })
    .catch(error => console.error('Error al llamar a la API.', error));
}

function EliminarPoliza(data) {
    fetch('./../../Controller/PolizaController.php?action=eliminarPoliza', {
        method: 'DELETE',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result.message === 'Poliza eliminada.') {
            alert('Poliza eliminada exitosamente.');
        } else {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error al llamar a la API.', error));
}

function ListadoPolizas() {
    fetch('./../../Controller/PolizaController.php?action=listarPolizas', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(result => {
        if (result) {
            actualizarTabla(result.data);
        } else {
            alert(result.message);
        }
    })
    .catch(error => console.error('Error al llamar a la API.', error));
}

function actualizarTabla(data) {
    const tbListaPoliza = document.getElementById('tbListaPoliza').getElementsByTagName('tbody')[0];
    tbListaPoliza.innerHTML = '';

    data.forEach(poliza => {
        const newRow = tbListaPoliza.insertRow();
        newRow.innerHTML = `
            <td>${poliza.PolizaID}</td>
            <td>${poliza.ClienteID}</td>
            <td>${poliza.PolizaMaestra}</td>
            <td>${poliza.TipoPoliza}</td>
            <td>${poliza.TipoSeguro}</td>
            <td>${poliza.Endoso}</td>
            <td>${poliza.Anterior}</td>
            <td>${poliza.Posterior}</td>
            <td>${poliza.FormaPago}</td>
            <td>${poliza.Moneda}</td>
            <td>${poliza.Ejecutivo}</td>
            <td>${poliza.Vendedor}</td>
            <td>${poliza.Renovacion}</td>
            <td>${poliza.InicioFechaPoliza}</td>
            <td>${poliza.HastaFechaPoliza}</td>
            <td>${poliza.Estatus}</td>
            <td>${poliza.MotivoEstatus}</td>
            <td>${poliza.FechaCobro}</td>
            <td>${poliza.PrimaNeta}</td>
            <td>${poliza.PrimaTotal}</td>
        `;
    });
}


// function actualizarFormulario(data) {
//     document.getElementById("inputPolizaID").value = data.PolizaID;
//     document.getElementById("inputClienteID").value = data.ClienteID;
//     document.getElementById("inputPolizaMaestra").value = data.PolizaMaestra;
//     document.getElementById("inputTipoPoliza").value = data.TipoPoliza;
//     document.getElementById("inputTipoSeguro").value = data.TipoSeguro;
//     document.getElementById("inputEndoso").value = data.Endoso;
//     document.getElementById("inputAnterior").value = data.Anterior;
//     document.getElementById("inputPosterior").value = data.Posterior;
//     document.getElementById("inputFormaPago").value = data.FormaPago;
//     document.getElementById("inputMoneda").value = data.Moneda;
//     document.getElementById("inputEjecutivo").value = data.Ejecutivo;
//     document.getElementById("inputVendedor").value = data.Vendedor;
//     document.getElementById("inputRenovacion").value = data.Renovacion;
//     document.getElementById("inputInicioFechaPoliza").value = data.InicioFechaPoliza;
//     document.getElementById("inputHastaFechaPoliza").value = data.HastaFechaPoliza;
//     document.getElementById("inputEstatus").value = data.Estatus;
//     document.getElementById("inputMotivoEstatus").value = data.MotivoEstatus;
//     document.getElementById("inputFechaCobro").value = data.FechaCobro;
//     document.getElementById("inputPrimaNeta").value = data.PrimaNeta;
//     document.getElementById("inputPrimaTotal").value = data.PrimaTotal;
// }

function obtenerDatosFormularioPoliza() {
    return {
        PolizaID: document.getElementById("inputPolizaID").value,
        ClienteID: document.getElementById("inputClienteID").value,
        PolizaMaestra: document.getElementById("inputPolizaMaestra").value,
        TipoPoliza: document.getElementById("inputTipoPoliza").value,
        TipoSeguro: document.getElementById("inputTipoSeguro").value,
        Endoso: document.getElementById("inputEndoso").value,
        Anterior: document.getElementById("inputAnterior").value,
        Posterior: document.getElementById("inputPosterior").value,
        FormaPago: document.getElementById("inputFormaPago").value,
        Moneda: document.getElementById("inputMoneda").value,
        Ejecutivo: document.getElementById("inputEjecutivo").value,
        Vendedor: document.getElementById("inputVendedor").value,
        Renovacion: document.getElementById("inputRenovacion").value,
        InicioFechaPoliza: document.getElementById("inputInicioFechaPoliza").value,
        HastaFechaPoliza: document.getElementById("inputHastaFechaPoliza").value,
        Estatus: document.getElementById("inputEstatus").value,
        MotivoEstatus: document.getElementById("inputMotivoEstatus").value,
        FechaCobro: document.getElementById("inputFechaCobro").value,
        PrimaNeta: document.getElementById("inputPrimaNeta").value,
        PrimaTotal: document.getElementById("inputPrimaTotal").value
    };
}


