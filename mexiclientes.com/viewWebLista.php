<?php
    session_start();
    //require_once './functions/PanelEmpresaKPI.php';
    // Cerrar la conexión
    //$conn->close();
    //validacion doble comprueba por url
    // Verificar si el usuario ha iniciado sesión
    if (!isset($_SESSION["usuario"])) {
        // Redireccionar al usuario a la página de inicio de sesión
        header("Location: ./index.php");
        exit();
    }
?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Empresa | MexiClientes</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="./img/favicon.png">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/meanmenu.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/et-line-icon.css">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/ionicons.min.css">
    <link rel="stylesheet" href="css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <script src="js/vendor/modernizr-3.11.2.min.js"></script>

    <link rel="stylesheet" href="css/dataTables.min.css">
    </link>
</head>

<body>
    <header class="top">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <div class="header-top-left">
                            <!-- <p>HAVE ANY QUESTION ?  +880 5698  598  6587</p> -->
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="header-top-right text-end">
                            <ul>
                                <li><a style="color: white;">Sistema MexiClientes</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-area two header-sticky">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="logo">
                            <a href="panelEmpresa.php">
                                <?php
                                    echo $_SESSION["imgEmpresa"];
                                ?>
                                <!--b>
                                    <?php //echo $_SESSION["usuario"];?>
                                </b-->
                            </a>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="content-wrapper text-end">
                            <div class="main-menu">
                                <nav>
                                    <ul>
                                        <li><a href="./panelEmpresa.php">Inicio</a>
                                        <li><a href="./viewProspectoLista.php">Mis clientes</a>
                                                                                <?php
                                            if($_SESSION["tipoUsuario"] == 'Oro'){
                                                echo '<li><a href="./viewBlogLista.php">Mi blog</a>';
                                            }
                                        ?>
                                        <li><a href="./viewWebLista.php">Mi negocio</a>
                                        <?php
                                            if($_SESSION["tipoUsuario"] == 'Oro'){
                                                echo '<li><a href="./viewCRMLista.php">Mi tienda</a>';
                                            }
                                        ?>
                                        <li><a href="">Cuenta</a>
                                            <ul>
                                                <li>
                                                    <a href="./functions/logout.php">Cerrar Sesión</a>
                                                    <!-- <a href="#">Cambiar clave</a>
                                                    <a href="#">Configuración</a> -->
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="mobile-menu hidden-sm"></div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header Area End -->

    <!-- Login start -->
    <div class="login-area pt-50 pb-150">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-md-offset-3 text-center">
                    <div class="login">
                        <div style="overflow: auto; width:95%;">
                        <div class='reply-area'>
                            <h3>Negocio auto administrable</h3>
                            <p>Modifica el perfil de tu negocio, crea secciones para tu negocio y modifica los componentes de tu negocio digital.</p>
                        </div>
                            
                            <ul>
                                <!-- <li>
                                    <h3><a href="viewWebPerfilCrear.php">Crear Perfil del negocio</a></h3>
                                </li> -->
                                <li>
                                    <h3><a href="viewWebPerfilModificar.php">Perfil del negocio</a></h3>
                                </li><br>
                                <!-- <li>
                                    <h3><a href="viewWebPageWebCrear.php">Crear Sección WEB</a></h3>
                                </li> <br> -->
                                <li>
                                    <?php
                                        if($_SESSION["tipoUsuario"] == 'Oro' || $_SESSION["tipoUsuario"] == 'Plata'){
                                            echo '<a href="viewWebPageWebModificar.php?idNegocio=' . $_SESSION["isUser"] . '&fileNameWeb=index">Publicar Sección Inicio</a>';
                                        } 
                                    ?>
                                </li>
                                <li>
                                    <?php
                                        if($_SESSION["tipoUsuario"] == 'Oro' || $_SESSION["tipoUsuario"] == 'Plata'){
                                            echo '<a href="viewWebPageWebModificar.php?idNegocio=' . $_SESSION["isUser"] . '&fileNameWeb=acerca">Modificar Publicar Acerca de</a>';
                                        } 
                                    ?>
                                </li>
                                <li>
                                    <?php
                                        if($_SESSION["tipoUsuario"] == 'Oro' || $_SESSION["tipoUsuario"] == 'Plata'){
                                            echo '<a href="viewWebPageWebModificar.php?idNegocio=' . $_SESSION["isUser"] . '&fileNameWeb=servicios">Publicar Sección Servicios</a>';
                                        } 
                                    ?>
                                </li>
                                <li>
                                    <?php
                                        if($_SESSION["tipoUsuario"] == 'Oro' ){
                                            echo '<a href="viewWebPageWebModificar.php?idNegocio=' . $_SESSION["isUser"] . '&fileNameWeb=blog-lista">Publicar Sección Blog</a>';
                                        } 
                                    ?>
                                </li>
                                <li>
                                    <?php
                                        if($_SESSION["tipoUsuario"] == 'Oro' || $_SESSION["tipoUsuario"] == 'Plata'){
                                            echo '<a href="viewWebPageWebModificar.php?idNegocio=' . $_SESSION["isUser"] . '&fileNameWeb=contacto">Publicar Sección Contacto</a>';
                                        } 
                                    ?>
                                </li>
                                <li>
                                    <?php
                                        if ($_SESSION["tipoUsuario"] == 'Bronce') {
                                            echo '<a href="viewWebPageWebModificar.php?idNegocio=' . $_SESSION["isUser"] . '&fileNameWeb=landing">Publicar Sección Landing Page</a>';
                                        } 
                                    ?>
                                </li><br><br>
                                <li hidden>
                                    <h3><a href="viewWebComponenteCrear.php">Crear nuevo componente</a></h3>
                                </li>
                                <li>
                                    <h3><a href="viewWebComponenteLista.php">Listado de componentes</a></h3>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Login end -->

    <!-- Footer Start -->
    <footer class="footer-area">
        <div class="footer-bottom text-center">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <p>Copyright © <a href="#">MexiClientes</a> 2023. All Right Reserved By <a href="#" target="_blank">Engranet.</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer End -->

    <script src="js/vendor/jquery-3.6.0.min.js"></script>
    <script src="js/vendor/jquery-migrate-3.3.2.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery.meanmenu.js"></script>
    <script src="js/jquery.magnific-popup.js"></script>
    <script src="js/ajax-mail.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.mb.YTPlayer.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>

    <script src="js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
        });

        $(document).ready(function() {
            $('#myTable2').DataTable();
        });
    </script>
</body>

</html>